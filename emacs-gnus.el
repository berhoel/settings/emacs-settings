;;; emacs-gnus.el --- gnus startup file

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;;; Commentary:

;;; Code:

(setq mail-source-movemail-program "movemail")

(cond
 ((member (downcase (system-name))
          (list "pchoel" "pchoel.xn--hllmanns-n4a.de"
                "ubuntu-laptop"))
  (load-library "my-gnus/split-home"))
 ((member (downcase (system-name))
          (list "kkcli937" "kkcli937.kumkeo.de"))
  (load-library "my-gnus/split-kumkeo"))
 (t
  (error (format "Unknown system %s" (system-name)))))

(provide 'emacs-gnus)



;; Local Variables:
;; mode: emacs-lisp
;; coding: utf-8
;; End:

;;; emacs-gnus.el ends here
