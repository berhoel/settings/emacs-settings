;;; early-init.el --- early init for startup, emacs > 27
;; Copyright © 2020, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; ID: $Id$

;;; Commentary:

;;; Code:

;; Turn off `debug-on-error` after init is complete (set by
;; `--debug-init` command line argument)
(when debug-on-error
  (add-hook 'after-init-hook
            #'(lambda () (setq debug-on-error t)))
  (setq byte-compile-error-on-warn t))

;;; Set Directory for emacs user settings.
(unless (boundp 'user-emacs-directory)
  (defvar user-emacs-directory
    (file-name-as-directory (expand-file-name "~/.emacs.d/"))
    "Directory beneath which additional per-user Emacs-specific
files are placed.
  Various programs in Emacs store information in this directory.
  Note that this should end with a directory separator.
  See also `locate-user-emacs-file'."))

;;; File used for storing customization information.
(setq custom-file
      (concat user-emacs-directory "emacs-custom.el"))

;;; Set directory for local settings.
(setq user-emacs-local-directory
      (if (member (downcase (system-name))
                  (list "pc130799.gl.ad.germanlloyd.org"
                        "hhbw402111"
                        "hhbw402111.gl.ad.germanlloyd.org"))
          (file-name-as-directory (expand-file-name "/data/tmp/berhol/emacs.d"))
        user-emacs-directory))

;;; Store ELPA packages on local instead of NFS directory.
(setq package-user-dir
      (file-name-as-directory
       (concat user-emacs-local-directory
               "elpa-"
               (number-to-string emacs-major-version))))

(custom-set-variables
 '(package-install-upgrade-built-in t))

;; Load customization settings.
(load custom-file)



;; Local Variables:
;; mode: emacs-lisp
;; coding: utf-8
;; End:

;;; early-init.el ends here
