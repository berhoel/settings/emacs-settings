;;; init-my-switches.el --- Define several switch variables used throughout the configuration.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:14 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;; windows only stuff
(defvar my-iswin (member system-type (list 'windows-nt 'ms-dos 'cygwin))
  "Non-nil if running on a Microsoft Windows system.")
;; unix only stuff
(defvar my-isux (not my-iswin)
  "Non-nil if running on a Linux system.")

(defvar my-iswork nil "Non-nil if EMACS is started at work.")
(defvar my-ishome nil "Non-nil if EMACS is started at home.")

(defvar my-c-style nil "C mode coding style to use.")

(cond
 ((member (downcase (system-name))
	  (list "pchoel"
		"ubuntu-laptop"
		"pchoel.xn--hllmanns-n4a.de"
		"localhost"
		"localhost.localdomain"))
  (setq my-iswork nil)
  (setq my-ishome t)
  (setq my-c-style "gnu"))
 ((member (downcase (system-name))
	  (list "kkcli937"))
  (setq my-iswork t)
  (setq my-ishome nil)
  (setq my-c-style "kumkeo"))
 (t
  (error (format "Unknown system %s" (system-name)))))

(setq frame-title-format
      `(multiple-frames "%b"
			("EMACS:" invocation-name "@" ,(system-name))))
(set-cursor-color "blue")

(use-package emacs
  :custom
  (user-full-name "Berthold Höllmann")
  ;; Set email address.
  (user-mail-address
   (cond
    (my-ishome "berhoel@gmail.com")
    (my-iswork "berthold.hoellmann@kumkeo.de"))))

(el-init-provide)



;;; init-my-switches.el ends here
