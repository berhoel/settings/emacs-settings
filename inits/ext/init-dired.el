;;; init-dired.el --- Settings for `dired'.

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-06 16:50:08 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(use-package init-my-switches :load-path "inits")
(use-package init-theme :load-path "inits")

;;; dired.el --- directory-browsing commands
(use-package dired
  :commands (dired
	     dired-other-window
	     dired-other-frame
	     dired-noselect
	     dired-mode
	     dired-hide-details-mode))

;;; dired-aux.el --- less commonly used parts of dired
(use-package dired-aux
  :bind (
	 :map dired-mode-map
	 ("C-s"   . dired-isearch-filenames)
	 ("M-C-s" . dired-isearch-filenames-regexp))
  :custom
  ;; Non-nil to Isearch in file names only. If t, Isearch in Dired
  ;; always matches only file names. If `dwim', Isearch matches file
  ;; names when initial point position is on a file name. Otherwise,
  ;; it searches the whole buffer without restrictions.
  (dired-isearch-filenames 'dwim))

;;; dired-x.el --- extra Dired functionality
(use-package dired-x
  :custom
  (dired-dwim-target t)
  (dired-guess-shell-gnutar "tar")
  (dired-guess-shell-alist-user
   `((,(rx (or (: ".avi" eos)
	       (: ".mpg" eos)
	       (: ".mpeg" eos)
	       (: ".MPG" eos)
	       (: ".mkv" eos)
	       (: ".mov" eos)
	       (: ".mp4" eos)
	       (: ".divx" eos)
	       (: ".f4v" eos)
	       (: ".m4v" eos)
	       (: ".ts" eos)
	       (: ".wmv" eos)
	       (: ".flv" eos))) "mpv" "mplayer" "xine" "vlc")
     (,(rx (or (: ".eps" eos)
               (: ".ps" eos))) "gv" "lpr")
     (,(rx (or (: ".doc" eos)
	       (: ".xls" eos)
	       (: ".ppt" eos)
	       (: ".odt" eos)
	       (: ".ods" eos)
	       (: ".odp" eos)
	       (: ".odg" eos)
	       (: ".odm" eos)
	       (: ".odf" eos)
	       (: ".odb" eos)
	       (: ".sxw" eos)
	       (: ".stw" eos)
	       (: ".rtf" eos)
	       (: ".sdw" eos))) "libreoffice")
     (,(rx "." "dot" eos) "dot" "neato" "twopi" "circo" "fdp" "libreoffice")
     (,(rx "." "gv" eos) "dot" "neato" "twopi" "circo" "fdp")
     (,(rx "." "pdf" eos) "evince")
     (,(rx "." "txt" eos) "cat"))))

;;; diredfl --- Extra font lock rules for a more colourful dired
;; Homepage: https://github.com/purcell/diredfl
;; This is adapted from the extra font lock rules provided by Drew
;; Adams' `dired+' package, but published via a modern means, and
;; with support for older Emacsen removed.
;;
;; Enable in all dired buffers by calling or customising `diredfl-global-mode'.
;;
;; Alternatively:
(use-package diredfl
  :ensure t
  :functions diredfl-global-mode
  ;; :hook ((dired-mode . diredfl-mode))
  :config
  (diredfl-global-mode 1))

;;; Dired integration

;;; gnus-dired.el --- utility functions where gnus and dired meet
;; `gnus-dired-minor-mode' installs key bindings in dired buffers to
;; send a file as an attachment (`C-c C-a'), open a file using the
;; approriate mailcap entry (`C-c C-l'), and print a file using the
;; mailcap entry (`C-c P'). It is enabled with
(use-package gnus-dired
  :if my-isux
  :hook ((dired-mode . turn-on-gnus-dired-mode)))

;;; dired-toggle.el --- provide a simple way to toggle dired buffer
;;;                     for current directory
(use-package dired-toggle
  :ensure t
  :bind
  ([f5] . dired-toggle)
  :preface
  (defun my@dired@toggle@mode@hook ()
    "My hook for dired-toggle."
    (interactive)
    (visual-line-mode 1)
    (dired-hide-details-mode)
    (setq-local visual-line-fringe-indicators '(nil right-curly-arrow))
    (setq-local word-wrap nil))
  :hook (;; You could also custom functions after `dired-toggle-mode'
	 ;; enabled, for example enable `visual-line-mode' for our narrow
	 ;; dired buffer:
	 (dired-toggle-mode . my@dired@toggle@mode@hook)))

;;; dired-k.el --- highlight dired buffer by file size, modified time,
;;;                git status
;; This package provides highlighting dired buffer like k.sh which
;; is zsh script.
(use-package dired-k
  :ensure t
  :bind (
	 :map dired-mode-map
	 ("k" . dired-k)))

;;; dired-dups.el --- Find duplicate files and display them in a dired
;;;                   buffer
;; This library provides the command `dired-find-duplicates' which
;; searches a directory for duplicates of the marked files in the
;; current dired buffer. It requires that the unix find and md5sum
;; commands are on your system.
(use-package dired-dups
  :ensure t
  :commands dired-find-duplicates)

;;; image-dired.el --- use dired to browse and manipulate your images
;; I needed a program to browse, organize and tag my pictures. I got
;; tired of the old gallery program I used as it did not allow
;; multi-file operations easily. Also, it put things out of my
;; control. Image viewing programs I tested did not allow multi-file
;; operations or did not do what I wanted it to.
(use-package image-dired
  :commands image-dired
  :custom
  ;; ### Recommend:
  ;; * Suppress unknown cursor movements:
  (image-dired-track-movement nil))

;;; image-dired+.el --- Image-dired extensions
;; - Non-blocking thumbnail creating
;; - Adjust image to window
(use-package image-dired+
  :ensure t
  :demand t
  :after image-dired
  :functions image-diredx-async-mode image-diredx-adjust-mode
  :config
  (image-diredx-async-mode 1)
  (image-diredx-adjust-mode 1))

;;; dired-launch --- Use dired as a launcher
;; This package provides a launcher for the Emacs dired-mode. In a
;; nutshell, it lets you select a file and then launch an external
;; application with that file.
(use-package dired-launch
  :ensure t
  :hook ((dired-mode . dired-launch-mode)))

;;; dired-recent --- Dired visited paths history
;; A simple history keeping for dired buffers.  All the visited
;; directories get saved for reuse later.  Works great with Ivy and
;; other `completing-read' replacements.
;;
;;  HOW TO USE IT:
;;  C-x C-d (`dired-recent-open')
(use-package dired-recent
  :ensure t
  :functions dired-recent-mode
  :bind (
	 ("C-x C-d" . dired-recent-open)
	 :map dired-recent-mode-map
	 ("C-x C-d" . nil))
  :config
  (dired-recent-mode .1))

;;; treemacs --- A tree style file explorer package
;; Homepage: https://github.com/Alexander-Miller/treemacs
;; A powerful and flexible file tree project explorer.
(use-package treemacs
  :ensure t
  :commands treemacs-follow-mode treemacs-filewatch-mode treemacs-fringe-indicator-mode
  :commands treemacs-git-mode
  :bind (
	 :map global-map
	 ("M-0" . treemacs-select-window)
	 ("C-x t 1" . treemacs-delete-other-windows)
	 ("C-x t t" . treemacs)
	 ("C-x t B" . treemacs-bookmark)
	 ("C-x t C-t" . treemacs-find-file)
	 ("C-x t M-t" . treemacs-find-tag))
  :custom
  (treemacs-collapse-dirs (if (executable-find "python") 3 0))
  (treemacs-deferred-git-apply-delay 0.5)
  (treemacs-display-in-side-window t)
  (treemacs-file-event-delay 5000)
  (treemacs-file-follow-delay 0.2)
  (treemacs-follow-after-init t)
  (treemacs-recenter-distance 0.1)
  (treemacs-git-command-pipe "")
  (treemacs-goto-tag-strategy 'refetch-index)
  (treemacs-indentation 2)
  (treemacs-indentation-string " ")
  (treemacs-is-never-other-window nil)
  (treemacs-max-git-entries 5000)
  (treemacs-no-png-images nil)
  (treemacs-no-delete-other-windows t)
  (treemacs-project-follow-cleanup nil)
  (treemacs-persist-file (expand-file-name ".cache/treemacs-persist" user-emacs-local-directory))
  (treemacs-recenter-after-file-follow nil)
  (treemacs-recenter-after-tag-follow nil)
  (treemacs-show-cursor nil)
  (treemacs-show-hidden-files t)
  (treemacs-silent-filewatch nil)
  (treemacs-silent-refresh nil)
  (treemacs-sorting 'alphabetic-desc)
  (treemacs-space-between-root-nodes nil)
  (treemacs-tag-follow-cleanup t)
  (treemacs-tag-follow-delay 1.5)
  (treemacs-width 35)
  :config
  ;; The default width and height of the icons is 22 pixels. If you are
  ;; using a Hi-DPI display, uncomment this to double the icon size.
  ;;(treemacs-resize-icons 44)
  (treemacs-follow-mode t)
  (treemacs-filewatch-mode t)
  (treemacs-fringe-indicator-mode t)
  (pcase (cons (not (null (executable-find "git")))
               (not (null (executable-find "python3"))))
    (`(t . t)
     (treemacs-git-mode 'deferred))
    (`(t . _)
     (treemacs-git-mode 'simple))))

;;; treemacs-projectile --- Projectile integration for treemacs
(use-package treemacs-projectile
  :after (:all treemacs projectile)
  :ensure t)

;;; all-the-icons-dired --- Shows icons for each file in dired mode
;; To use this package, simply add this to your init.el:
;; (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
(use-package all-the-icons-dired
  :ensure t
  :disabled ;; either this or treemacs-icons-dired
  :hook ((dired-mode . all-the-icons-dired-mode)))

;;; treemacs-icons-dired --- Treemacs icons for dired
;; Treemacs icons for dired. Code is based on all-the-icons-dired.el
(use-package treemacs-icons-dired
  :after (:all treemacs dired)
  :ensure t
  :functions treemacs-icons-dired-mode
  ;; :disabled ;; either this or all-the-icons-dired 2019/01/11
  :config
  (treemacs-icons-dired-mode))

;;; treemacs-all-the-icons.el --- all-the-icons integration for treemacs
;; all-the-icons integration for treemacs.
(use-package treemacs-all-the-icons :ensure t :after treemacs)

;;; treemacs-magit --- Magit integration for treemacs
(use-package treemacs-magit
  :after (all: treemacs magit)
  :ensure)

;;; pack --- Pack and unpack archive files
;; Provides some commands and functions to pack and unpack
;; archives in a simple way.
;;
;; Commands to pack/unpack archive files can be defined by setting
;; `pack-program-alist' variable.
(use-package pack
  :ensure t
  ;; To pack/unpack files from dired buffers, add following to your
  ;; dired confiugration:
  ;;   (define-key dired-mode-map "P" 'pack-dired-dwim))
  ;; This command creates an archive file from marked files, or unpack
  ;; the file when only one file is selected and that has an extension
  ;; for archive.
  :bind (
	 :map dired-mode-map
	 ("M-P" . pack-dired-dwim)))

;;; dired-git-info --- Show git info in dired
;; This Emacs packages provides a minor mode which shows git
;; information inside the dired buffer.
(use-package dired-git-info
  :ensure t
  :after dired
  :bind (
	 :map dired-mode-map
	 (")" . dired-git-info-mode)))

(el-init-provide)



;;; dired-settings.el ends here
