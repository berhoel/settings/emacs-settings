;;; init-ecb.el --- settings for `ecb'.

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:48 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)
(require 'advice)

(use-package init-my-switches :load-path "inits")

;; (setq debug-on-error nil)

;; (semantic-load-enable-excessive-code-helpers)
;; (semantic-load-enable-semantic-debugging-helpers)
(require 'srecode/mode)

(defvar my@ecb@active nil "Current state of ECB.")

;;; ecb-layout.el --- layout for ECB
(use-package ecb-layout
  :after ecb
  :commands (ecb-canonical-edit-windows-list
             ecb-compile-window-live-p
             ecb-get-window-fix-type
             ecb-layout-debug-error
             ecb-maximize-ecb-buffer
             ecb-point-in-compile-window
             ecb-point-in-edit-window-number
             ecb-select-edit-window
             ecb-set-window-size-fixed
             ecb-toggle-compile-window
             ecb-windows-all-hidden)
  :init
  (setq ecb-compile-window-height 12))

;;; ecb-common-browser.el --- common browsing stuff for  Emacs
(use-package ecb-common-browser
  :after ecb
  :commands ecb-advices-debug-error ecb-disable-advices ecb-enable-advices)

;;; ecb-util.el --- utility functions for ECB
(use-package ecb-util
  :after ecb
  :commands ecb-bolp ecb-error ecb-line-beginning-pos ecb-position ecb-window-edges)


;;; ecb --- a code browser for Emacs
(use-package ecb
  :ensure t
  :after (semantic)
  :disabled t
  :if (and my-isux)
  :preface
  (defun my@ecb@before@activate@hook (&rest args)
    "ECB activation hook."

    ;; ;; Enable the Project management system
    ;; (global-ede-mode t)

    ;; Enable semantic. Causes the "Development" main menu
    ;; to be added.
    (semantic-mode t)

    ;; Enable prototype help and smart completion
    ;; (semantic-load-enable-code-helpers)

    ;; Enable template insertion menu
    (global-srecode-minor-mode 1)
    ;; If enabled, semantic inserts lines in the code
    ;; buffer above the tags it is tracking, and highlights
    ;; things

    ;;(setq ecb-bucket-node-display '("[" "]" ecb-bucket-node-face))
    )

  (defun my@ecb@activate@hook (&rest args)
    "ECB activation hook."
    (setq my@ecb@active t)

    ;; Close compile window if open
    (when (ecb-compile-window-live-p)
      (ecb-toggle-compile-window)))

  (defun my@ecb@deactivate@hook (&rest args)
    "ECB deactivation hook."
    (setq my@ecb@active nil))

  :hook ((ecb-before-activate . my@ecb@before@activate@hook)
         (ecb-activate . my@ecb@activate@hook)
         (ecb-deactivate . my@ecb@deactivate@hook)
         (after-init . ecb-activate))
  :init
  (setq ecb-version-check nil)
  (setq ecb-tip-of-the-day nil)

  (setq ecb-sources-exclude-cvsignore '(".*"))

  ;; ECB layout.
  (setq ecb-layout-name "right1")
  (setq ecb-layout-window-sizes '(("right1"
                                   (ecb-directories-buffer-name 0.2 . 0.2)
                                   (ecb-sources-buffer-name 0.2 . 0.4)
                                   (ecb-methods-buffer-name 0.2 . 0.4))))
  (setq ecb-show-sources-in-directories-buffer 'always)

  (when (display-graphic-p)
    (eval-after-load 'ecb-face
      ;; Use a slightly smaller face for the ECB tree-buffers.
      '(set-face-attribute 'ecb-default-general-face
                           nil :height 0.8)))

  :config
  (setq ecb-source-path
        (cond
         ((string= "pchoel.xn--hllmanns-n4a.de" (system-name))
          '(("/home/hoel/work/hh-pythoneers" "hh-pythoneers")
            ("/home/hoel/work/fill_mmusic2" "fill_mmusic2")
            ("/home/hoel/work/PSH.web" "PSH.web")
            ("/home/hoel/work/Höllmanns.de" "Höllmanns.de")
            (user-emacs-directory "emacs.d")
            "/home/hoel/work"
            ("/home/hoel" "home")))
         (t nil))))

(el-init-provide)



;;; init-ecb.el ends here
