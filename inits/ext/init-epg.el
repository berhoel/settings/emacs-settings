;;; init-epg.el --- Setup `epg.el'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:52:59 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; epg.el --- the EasyPG Library
(use-package epg
  :custom
  (setq epg-debug nil)) ;; t ;;  then read the *epg-debug*" buffer

(el-init-provide)



;;; init-epg.el ends here
