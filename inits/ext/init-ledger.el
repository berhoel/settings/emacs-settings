;;; init-ledger.el --- Helper for ledger files.

;; Copyright © 2018, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-12-21 18:40:19 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'quelpa)
  (require 'quelpa-use-package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(use-package init-flycheck :load-path "inits/ext")
(use-package init-progmodes :load-path "inits/progmodes")

;;; ledger-mode --- Helper code for use with the "ledger" command-line tool
(use-package ledger-mode
  :ensure t
  :mode ((rx (or ".ledger" ".rec") eos))
  :bind (
         :map ledger-mode-map
         ([C-f8] . format-all-buffer)))

;;; dklrt --- Ledger Recurring Transactions.
(use-package dklrt
  :ensure t
  ;; binding ‘\C-cr’ to ‘dklrt-AppendRecurringMaybe’ <while in
  ;; ‘ledger-mode’.
  :hook (ledger-mode . dklrt-SetCcKeys))

;;; flycheck-ledger.el --- Flycheck integration for ledger files
(use-package flycheck-ledger
  :ensure t
  :after (:all flycheck ledger))

;;; beancount.el --- A minor mode that can be used to edit beancount input files.
(use-package beancount
  :quelpa ((beancount :fetcher github :repo "beancount/beancount-mode") :upgrade t)
  :mode ((rx ".beancount" eos) . beancount-mode)
  :hook ((beancount-mode . (lambda () (setq-local electric-indent-chars nil)))
	 (beancount-mode . outline-minor-mode)
	 (beancount-mode . flymake-bean-check-enable)))

(el-init-provide)



;;; init-ledger.el ends here
