;;; init-svn.el --- Settings for SVN access.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:46 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'quelpa)
  (require 'quelpa-use-package)
  (require 'use-package))

(require 'el-init)

;;; vc-svn.el --- non-resident support for Subversion version-control
(use-package vc-svn)

;;; dsvn.el --- Subversion interface
(use-package dsvn
  :ensure t
  :commands svn-status svn-update)

;;; psvn.el --- Subversion interface for emacs
(use-package psvn
  :disabled
  :quelpa ((psvn
	    :fetcher url
	    :url "http://www.xsteve.at/prg/emacs/psvn.el")
	   :upgrade t)
  :commands svn-examine svn-status svn-status-update-modeline
  :init
  (setq svn-status-svn-executable "/usr/bin/svn"))

;;; diff-hl.el --- Highlight uncommitted changes
(use-package diff-hl
  :ensure t
  :after (hl-line)
  :commands global-diff-hl-mode
  :hook ((prog-mode vc-dir-mode) . turn-on-diff-hl-mode)
  );; :init (global-diff-hl-mode)

;;; helm-ls-svn.el --- helm extension to list svn files
(use-package helm-ls-svn
  :after (:all svn helm)
  :ensure t)

(el-init-provide)



;;; init-svn.el ends here
