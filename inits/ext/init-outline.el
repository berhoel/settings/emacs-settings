;;; init-outline.el --- Settings for package `outline'.

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:49 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

;;; outline.el --- outline mode commands for Emacs
(use-package outline
  :commands outline-minor-mode
  :defines outline-minor-mode-map
  :hook (((c-mode-common emacs-lisp-mode LaTeX-mode) . outline-minor-mode)))

;;; outline-magic.el --- outline mode extensions for Emacs
(use-package outline-magic
  :ensure t
  :after outline
  :bind (
         :map outline-minor-mode-map
         ([f9] . outline-cycle)))

(el-init-provide)



;;; init-outline.el ends here
