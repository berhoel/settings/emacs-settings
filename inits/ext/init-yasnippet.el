;;; init-yasnippet.el --- Initializing of `yasnippet'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:40:02 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; yasnippet-snippets.el --- Collection of yasnippet snippets
(use-package yasnippet-snippets
  :ensure t
  :after yasnippet)

;;; yasnippet.el --- Yet another snippet extension for Emacs.
;; Yas minor mode is enabled in all buffers where `yas-minor-mode-on'
;; would do it.
;; See `yas-minor-mode' for more information on Yas minor mode.
(use-package yasnippet
  :ensure t
  :functions yas-hippie-try-expand yas-global-mode yas-recompile-all
  :custom
  (yas-prompt-functions '(yas-dropdown-prompt
                          yas-ido-prompt
                          yas-completing-prompt))
  ;; Expanding with hippie-expand
  ;; To integrate with hippie-expand, just put yas-hippie-try-expand
  ;; in hippie-expand-try-functions-list. This probably makes more
  ;; sense when placed at the top of the list, but it can be put
  ;; anywhere you prefer.
  (hippie-expand-try-functions-list
   (append hippie-expand-try-functions-list '(yas-hippie-try-expand)))
  :config
  (setq yas-verbosity 0)
  (yas-recompile-all)
  (yas-global-mode))

(use-package license-snippets
  :ensure t
  :after yasnippet
  :functions license-snippets-init
  :config
  ;; Make sure to call `license-snippets-init' after yasnippet loaded.
  (license-snippets-init))

(use-package gitignore-snippets
  :ensure t
  :after (:all gitignore-mode yasnippet)
  :functions gitignore-snippets-init
  :config
  ;; Make sure to call `gitignore-snippets-init' after yasnippet loaded.
  (gitignore-snippets-init))

;;; go-snippets.el --- Yasnippets for go
(use-package go-snippets
  :ensure t
  :after (:all go-mode yasnippet))

(el-init-provide)



;;; init-yasnippet.el ends here
