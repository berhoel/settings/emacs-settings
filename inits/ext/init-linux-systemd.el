;;; init-linux-systemd.el --- Settings for `systremd' mode.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:53:38 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; systemd.el --- Major mode for editing systemd units
(use-package systemd
  :ensure t
  :mode ((rx (or ".automount"
                 ".busname"
                 ".mount"
                 ".service"
                 ".slice"
                 ".socket"
                 ".target"
                 ".timer"
                 ".link"
                 ".netdev"
                 ".network"
                 (: ".override.conf" (* any))) eos) . systemd-mode))

(el-init-provide)



;;; init-linux-systemd.el ends here
