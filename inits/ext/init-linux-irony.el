;;; init-linux-irony.el --- Settings for `irony'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:52:55 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; irony.el --- C/C++ minor mode powered by libclang
(use-package irony
  :ensure t
  :disabled t
  :commands irony-mode
  :hook ((irony-mode . irony-cdb-autosetup-compile-options)
         (c-mode-common-hook . irony-mode)
         ;; Activate irony-mode on arudino-mode
         (arduino-mode . irony-mode)))

(el-init-provide)



;;; init-linux-irony.el ends here
