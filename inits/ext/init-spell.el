;;; init-spell.el --- initialization for spell modes

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-12-17 23:17:13 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(use-package init-my-switches :load-path "inits")
(when my-iswin
  (use-package init-windows-base :load-path "inits"))

;; ;;; ispell.el --- interface to International Ispell Versions 3.1 and 3.2
;; (use-package ispell
;;   :demand t
;;   :bind ("M-$" . ispell-word)
;;   :custom
;;   (ispell-dictionary "english")
;;   (ispell-program-name (when (executable-find "hunspell") (executable-find "hunspell")))
;;   (ispell-check-comments t))

;; ;;; flyspell.el --- on-the-fly spell checker
;; (use-package flyspell
;;   :if (not my-iswin)
;;   :hook (((org-mode TeX-mode LaTeX-mode message-mode) . turn-on-flyspell)
;;          (prog-mode . flyspell-prog-mode))
;;   :custom
;;   (flyspell-sort-corrections t)
;;   (flyspell-use-meta-tab nil))

;;; helm-flyspell.el -- Helm extension for correcting words with
;;;                     flyspell
(use-package helm-flyspell
  :ensure t
  :after (:all helm flyspell)
  ;; To use, just put your cursor on or after the misspelled word and
  ;; run helm-flyspell-correct. You can of course bind it to a key as
  ;; well by adding this to your `~/.emacs` file:
  :bind (
         :map flyspell-mode-map
         ("C-;" . helm-flyspell-correct)))
;; When invoked, it will show the list of corrections suggested by
;; Flyspell and options to save the word in your personal dictionary
;; or accept it in the buffer or the session. If a pattern is typed,
;; it will be used to filter the corrections. It can also be
;; directly saved to the dictionary, even if it is different from
;; the initial word. The new typed word will also replace the word
;; typed in the buffer.

;;; guess-language --- Robust automatic language detection
(use-package guess-language
  :ensure t
  :hook ((flyspell-mode text-mode) . guess-language-mode)
  :custom
  (guess-language-langcodes
   '((en . ("en_US" "English" "🇺🇸" "US English"))
     (de . ("de_DE" "German" "🇩🇪" "German")))))

;;; ace-flyspell.el --- Jump to and correct spelling errors using
;;;                     `ace-jump-mode' and flyspell
(use-package ace-flyspell
  :ensure t
  :after flyspell
  :functions ace-flyspell-setup
  ;; If you call =M-x ace-flyspell-setup= , then this setup binds
  ;; the command =ace-flyspell-dwim= to =C-.=, which is originally
  ;; bound to =flyspell-auto-correct-word= if you enable the
  ;; =flyspell-mode=. Of course, you can choose to change the key
  ;; binding.
  :config
  (ace-flyspell-setup))

;;; languagetool.el ---  LanguageTool integration for grammar check
(use-package languagetool
  :ensure t
  :preface
  (setq @my@languagetool@basedir
        (expand-file-name "/snap/languagetool/current/usr/bin/"))
  :bind (("C-c l c" . languagetool-check)
         ("C-c l d" . languagetool-clear-buffer)
         ("C-c l p" . languagetool-correct-at-point)
         ("C-c l b" . languagetool-correct-buffer)
         ("C-c l l" . languagetool-set-language))
  :commands (languagetool-clear-suggestions
             languagetool-server-mode
             languagetool-server-start
             languagetool-server-stop)
  :custom
    ;; Absolute path to LanguageTool command line jar file.
  (languagetool-console-command
   (concat @my@languagetool@basedir "/languagetool-commandline.jar"))
  (languagetool-server-command
   (concat @my@languagetool@basedir "/languagetool-server.jar"))
  (languagetool-server-url "http://localhost")
  (languagetool-server-port 8081)
  ;; List of string passed to java command as arguments.
  ;;
  ;; Described at http://wiki.languagetool.org/command-line-options,
  ;; recomends to use:
  (languagetool-java-arguments '("-Dfile.encoding=UTF-8"))
  :config
  (languagetool-server-mode))

;;; jinx.el --- Enchanted Just-in-time Spell Checker -*- lexical-binding: t -*-
;; Jinx is a fast just-in-time spell-checker for Emacs.  Jinx
;; highlights misspelled words in the text of the visible portion of
;; the buffer.  For efficiency, Jinx highlights misspellings lazily,
;; recognizes window boundaries and text folding, if any.  For
;; example, when unfolding or scrolling, only the newly visible part
;; of the text is checked, if it has not been checked before.  Each
;; misspelling can then be corrected from a list of dictionary words
;; presented as completion candidates in a list.
(use-package jinx
  :ensure t
  :hook (emacs-startup . global-jinx-mode)
  :bind ([remap ispell-word] . jinx-correct))

(el-init-provide)



;;; init-spell.el ends here
