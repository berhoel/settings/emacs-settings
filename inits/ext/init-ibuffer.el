;;; init-ibuffer.el --- Settings for `ibuffer'.

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:52 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

;;; ibuffer --- operate on buffers like dired
(use-package ibuffer
  :bind ("C-x C-b" . ibuffer)
  :commands ibuffer-unmark-all
  :custom
  ;; To include vc status info in the ibuffer list, add either
  ;; vc-status-mini or vc-status to `ibuffer-formats':
  (ibuffer-formats
   '((mark modified read-only vc-status-mini locked
	   " " (name 18 18 :left :elide)
	   " " (size 9 -1 :right)
	   " " (mode 16 16 :left :elide)
	   " " (vc-status 16 16 :left)
	   " " filename-and-process)
     (mark " " (name 16 -1)
	   " " filename))))

;;; ibuffer-vc --- Group ibuffer's list by VC project, or show VC
;;;status
(use-package ibuffer-vc
  :ensure t
  :commands ibuffer-vc-set-filter-groups-by-vc-root
  :preface
  ;; To group buffers by vc parent dir:
  (defun my@ibuffer@setup@for@vc ()
    "Hook function to setup ibuffer for vc."
    (ibuffer-vc-set-filter-groups-by-vc-root)
    (unless (eq ibuffer-sorting-mode 'alphabetic)
      (ibuffer-do-sort-by-alphabetic)))
  :hook (ibuffer . my@ibuffer@setup@for@vc))

;;; ibuf-ext --- extensions for ibuffer
(use-package ibuf-ext
  :commands ibuffer-do-sort-by-alphabetic)

;;; ibuffer-projectile --- Group ibuffer's list by projectile root
(use-package ibuffer-projectile
  :ensure t
  :preface
  (defun my@ibuffer@hook@fun (&rest args)
    (ibuffer-projectile-set-filter-groups)
    (unless (eq ibuffer-sorting-mode 'alphabetic)
      (ibuffer-do-sort-by-alphabetic)))
  :after (projectile )
  :commands ibuffer-projectile-set-filter-groups
  :hook ((ibuffer . my@ibuffer@hook@fun)))

(el-init-provide)



;;; init-ibuffer.el ends here
