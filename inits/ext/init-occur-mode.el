;;; init-occur-mode.el --- Settings for occur-mode

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:53:38 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; occur-context-resize.el --- dynamically resize context around
;;;                             matches in occur-mode
(use-package occur-context-resize
  :ensure t
  ;; Makes +,-, and 0 resize the context displayed around occur
  ;; matches in `occur-mode'.
  ;;
  ;; `occur-context-resize-default' will revert to whatever size
  ;; context is specified in
  ;; `list-matching-lines-default-context-lines'.
  :hook (occur-mode . occur-context-resize-mode))

(el-init-provide)



;;; init-occur-mode.el ends here
