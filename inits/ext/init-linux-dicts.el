;;; init-linux-dicts.el --- Settings for using dictionaries..

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:51 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(use-package init-my-switches :load-path "inits")

;;; helm-wordnet --- Helm interface to local wordnet dictionary
(use-package helm-wordnet
  :ensure t
  :after (helm)
  :commands helm-wordnet-suggest
  :defines helm-wordnet-wordnet-location
  :custom
  (helm-wordnet-wordnet-location
   (car
    (cl-delete-if-not
     'file-exists-p
     '("/usr/share/wordnet-3.0/dict"
       "/usr/share/wordnet")))))

;;; helm-dictionary --- Helm source for looking up dictionaries
(use-package helm-dictionary
  :ensure t
  :after (helm)
  :commands helm-dictionary)

;;; dictionary --- Client for rfc2229 dictionary servers
(use-package dictionary
  :ensure t
  :commands (dictionary-lookup-definition
	     dictionary
	     dictionary-mouse-popup-matching-words
	     dictionary-popup-matching-words
	     dictionary-tooltip-mode
	     global-dictionary-tooltip-mode)
  :defines dictionary-tooltip-dictionary
  :bind (("C-c C-d" . dictionary-search)
	 ("C-c C-m" . dictionary-match-words))
  :custom
  (dictionary-tooltip-dictionary "eng-deu"))

;;; dictcc --- Look up translations on dict.cc
(use-package dictcc :ensure t)

;;; synosaurus --- An extensible thesaurus supporting lookup and
;;;substitution.
;; An extensible thesaurus supporting lookup and substitution.
;;
;; You can choose between multiple backends. Current backends include
;; wordnet and openthesaurus, but it's easy to add your own.
(use-package synosaurus
  :ensure t
  :commands synosaurus-lookup synosaurus-choose-and-replace)

;;; synonymous --- A thesaurus at your fingertips
(use-package synonymous :ensure t)

(el-init-provide)



;;; init-wordnet.el ends here
