;;; init-auto-insert.el --- Settings for `auto-insert'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:56 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))
(require 'el-init)

(eval-when-compile
  (require 'use-package))

(use-package init-my-switches :load-path "inits")
(use-package init-files :load-path "inits/ext")
(use-package init-yasnippet :load-path "inits/ext")

;;; autoinsert.el --- automatic mode-dependent insertion of text into
;;;                   new files
(use-package autoinsert
  :demand t
  :commands auto-insert-mode
  :functions yas--get-template-by-uuid
  :functions yas-expand-snippet
  :defines auto-insert-query
  :defines auto-insert-alist
  :custom
  (auto-insert-directory (concat user-emacs-directory "autoinsert/"))
  (auto-insert 'other)
  :config
  (auto-insert-mode))

(defun my-organization ()
  "Return the organization name either from environment
variable `ORGANIZATION` or defult value."
  (or (getenv "ORGANIZATION")
      "kumkeo GmbH"))

(defun my-fname ()
  "Return the filename of the current buffer without path and
extension."
  (file-name-sans-extension
   (file-name-nondirectory
    (buffer-file-name))))

(defun my-copyright ()
  "Insert gl copyright notice."
  (concat
   "Copyright (C) " (format-time-string "%Y") " by " (my-organization)))

(defun my-copyright-utf ()
  "Insert gl copyright notice."
  (concat
   "Copyright © " (format-time-string "%Y") " by " (my-organization)))

(defun my-header-define ()
  "Generate a standard define for header files."
  (concat
   "_"
   (upcase
    (file-name-nondirectory
     (file-name-sans-extension
      (buffer-file-name))))
   "_H_"))
;; (upcase
;;  (concat
;;   "_"
;;   (user-login-name)
;;   (format-time-string "%Y%m%d")
;;   "_"
;;   (my-fname))))

(define-skeleton my-blurb-head
  "Insert standard blurb for skeletons."
  ""
  "Task      " (skeleton-read "Enter Task: ") "\n"
  "\n"
  "Author    " user-full-name " <" user-mail-address ">\n"
  "\n")

(defun my-yas-expand-by-uuid (mode uuid)
  "Expand snippet template in MODE by its UUID."
  (let ((yas-indent-line 'fixed)
        (yas-wrap-around-region 'nil))
    (yas-expand-snippet
     (yas--template-content
      (yas--get-template-by-uuid mode uuid)))))

(el-init-provide)



;;; init-auto-insert.el ends here
