;;; init-fontlock.el --- Startup settings fpr font lock.

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-09-13 21:19:43 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'quelpa)
  (require 'use-package))

(require 'quelpa-use-package)

(require 'el-init)

(require 'bind-key)

(use-package init-my-switches :load-path "inits")

;; ** New variable 'xft-ignore-color-fonts'.
;; Default t means don't try to load color fonts when using Xft, as they
;; often cause crashes.  Set it to nil if you really need those fonts.
(use-package emacs
  :config
  (setq xft-ignore-color-fonts nil))

;;; font-lock.el --- Electric font lock mode
(use-package font-lock
  :if (display-graphic-p)
  :hook (after-init . global-font-lock-mode)
  :custom
  ;; (font-lock-verbose t)
  (font-lock-maximum-decoration t))

;;; font-lock+.el --- Enhancements to standard library `font-lock.el'.
(use-package font-lock+
  :quelpa ((font-lock+
	    :fetcher github
	    :repo "emacsmirror/font-lock-plus")
	   :upgrade t)
  :after font-lock)

;;; highlight-indentation.el --- Minor modes for highlighting indentation
(use-package highlight-indentation
  :ensure t
  :commands highlight-indentation-current-column-mode highlight-indentation-mode
  :hook (;; displays guidelines indentation (space indentation only).
         (prog-mode . highlight-indentation-mode)
         ;; displays guidelines for the current-point indentation (space
         ;; indentation only).
         (prog-mode . highlight-indentation-current-column-mode)))

;; default-text-scale -- Easily adjust the font size in all frames
;; This package provides commands for increasing or decreasing the
;; default font size in all GUI Emacs frames -- it is like an
;; Emacs-wide version of `text-scale-mode'.
(use-package default-text-scale
  :ensure t
  :bind (("M-C-+" . default-text-scale-increase)
         ("M-C--" . default-text-scale-decrease)))

;;; fontawesome.el --- fontawesome utility
(use-package fontawesome
  :ensure t
  :after (helm)
  :commands helm-fontawesome fontawesome)

;;; rainbow-identifiers.el --- Highlight identifiers according to
;;;                            their names
;; Minor mode providing highlighting of identifiers based on their
;; names. Each identifier gets a color based on a hash of its name.
(use-package rainbow-identifiers
  :ensure t
  :hook (after-init . rainbow-identifiers-mode))

;;; emojify --- Display emojis in Emacs
(use-package emojify
  :ensure t
  :hook ((after-init . global-emojify-mode))
  :custom
  (emojify-display-style 'unicode)
  (emojify-point-entered-behaviour 'uncover))

;;; Add logos to emojify
;; This package adds logo icons for various programming languages and
;; tools to emojify.el
;; All icons are the property of their respective owners and may be
;; trademarked and/or restricted in the way they may be used. See
;; COPYRIGHT.MD for more details.
(use-package emojify-logos
  :ensure t
  :after emojify)

;;; emoji-cheat-sheet-plus --- emoji-cheat-sheet for emacs
;; * emoji buffer has its own major-mode,
;; * automatic display of emoji code in the minibuffer while browsing
;;   the emoji buffer,
;; * new minor mode: emoji-cheat-sheet-plus-display-mode replacing
;;   emoji codes in buffer by the its image,
;; new function: emoji-cheat-sheet-plus-insert to insert an emoji at
;; point using an helm front-end. It is the possible to insert several
;; emoji thanks to helm persistent action or its multiple selection
;; feature.
(use-package emoji-cheat-sheet-plus
  :ensure t
  :after (helm)
  ;; enabled emoji in org buffer
  :hook (org-mode . emoji-cheat-sheet-plus-display-mode)
  ;; insert emoji with helm
  :bind ("C-c C-e" . emoji-cheat-sheet-plus-insert))

;;; highlight-blocks --- Highlight the blocks point is in
;; Highlight the nested blocks the point is currently in.
(use-package highlight-blocks
  :ensure t
  :hook (after-init . highlight-blocks-mode))

;;; all-the-icons.el --- A library for inserting Developer icons
(use-package all-the-icons :ensure t)

(use-package all-the-icons-nerd-fonts
  :ensure t
  :after all-the-icons
  :demand t
  :config
  (all-the-icons-nerd-fonts-prefer))

;;; proportional.el --- use a proportional font everywhere
(use-package proportional
  :ensure t
  :commands proportional-mode
  :defines proportional-font proportional-monospace-font
  :custom
  (proportional-font "Source Sans Pro-12")
  (proportional-monospace-font "Source Code Pro-12"))
;; (proportional-mode)

;;; ligature.el --- Display typographical ligatures in major modes
;; This package converts graphemes (characters) present in major modes
;; of your choice to the stylistic ligatures present in your frame's
;; font.
(use-package ligature
  :quelpa ((ligature
	    :repo "mickeynp/ligature.el"
	    :fetcher github)
	   :upgrade t)
  :functions ligature-set-ligatures global-ligature-mode
  :config
  ;; Enable the "www" ligature in every possible major mode
  (ligature-set-ligatures 't '("www" "ff" "fi" "ffi"))
  ;; Enable traditional ligature support in eww-mode, if the
  ;; `variable-pitch' face supports it
  (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
  ;; Enable all JetBrains Mono ligatures in programming modes
  (ligature-set-ligatures
   'prog-mode
   '("--" "---" "==" "===" "!=" "!==" "=!=" "=:=" "=/=" "<=" ">=" "&&" "&&&" "&=" "++"
     "+++" "***" ";;" "!!" "??" "???" "?:" "?." "?=" "<:" ":<" ":>" ">:" "<:<" "<>"
     "<<<" ">>>" "<<" ">>" "||" "-|" "_|_" "|-" "||-" "|=" "||=" "##" "###" "####" "#{"
     "#[" "]#" "#(" "#?" "#_" "#_(" "#:" "#!" "#=" "^=" "<$>" "<$" "$>" "<+>" "<+" "+>"
     "<*>" "<*" "*>" "</" "</>" "/>" "<!--" "<#--" "-->" "->" "->>" "<<-" "<-" "<=<"
     "=<<" "<<=" "<==" "<=>" "<==>" "==>" "=>" "=>>" ">=>" ">>=" ">>-" ">-" "-<" "-<<"
     ">->" "<-<" "<-|" "<=|" "|=>" "|->" "<->" "<~~" "<~" "<~>" "~~" "~~>" "~>" "~-"
     "-~" "~@" "[||]" "|]" "[|" "|}" "{|" "[<" ">]" "|>" "<|" "||>" "<||" "|||>" "<|||"
     "<|>" "..." ".." ".=" "..<" ".?" "::" ":::" ":=" "::=" ":?" ":?>" "//" "///" "/*"
     "*/" "/=" "//=" "/==" "@_" "__"))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  (global-ligature-mode t))

;;; unicode-fonts --- Configure Unicode fonts
;; Emacs maintains font mappings on a per-glyph basis, meaning
;; that multiple fonts are used at the same time (transparently) to
;; display any character for which you have a font.  Furthermore,
;; Emacs does this out of the box.
;;
;; However, font mappings via fontsets are a bit difficult to
;; configure.  In addition, the default setup does not always pick
;; the most legible fonts.  As the manual warns, the choice of font
;; actually displayed for a non-ASCII character is "somewhat random".
;;
;; The Unicode standard provides a way to organize font mappings: it
;; divides character ranges into logical groups called "blocks".  This
;; library configures Emacs in a Unicode-friendly way by providing
;; mappings from
;;
;;     each Unicode block  ---to--->   a font with good coverage
;;
;; and makes the settings available via the customization interface.
(use-package unicode-fonts
  :ensure t
  :functions unicode-fonts-setup
  :config
  (unicode-fonts-setup))


(el-init-provide)



;;; init-fontlock.el ends here
