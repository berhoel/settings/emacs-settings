;;; keys.el --- Setting up key configuration

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:51 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(use-package init-my-switches :load-path "inits")
(use-package init-path :load-path "inits/ext")

;;; rect.el --- rectangle functions for GNU Emacs
(use-package rect
  :disabled
  :bind (([f6] . kill-rectangle)
         ([f7] . yank-rectangle)))

;;; register.el --- register commands for Emacs
(use-package register
  :disabled
  :bind ([f8] . copy-rectangle-to-register))

;;; compile.el --- run compiler as inferior of Emacs, parse error messages
(use-package compile
  :bind ([f10] . compile)
  :custom
  (compilation-scroll-output #'first-error)
  (compile-command "make "))

;;; simple.el --- basic editing commands for Emacs
(use-package simple
  :demand t
  :commands column-number-mode
  :bind (([?\S- ] . just-one-space) ; Alt-Space get caught by  KDE
         ([f11] . next-error)
         ([f12] . repeat-complex-command)
         ([M-delete] . kill-word)
         :map esc-map
         ("G" . goto-line))
  :hook (tex-mode . turn-on-auto-fill)
  :custom
  (delete-active-region nil)
  (kill-whole-line t)
  (mail-user-agent 'gnus-user-agent)
  (next-line-add-newlines t)
  (save-interprogram-paste-before-kill t)
  (next-error-message-highlight 'keep)
  :config
  (column-number-mode))

;;; etags.el --- etags facility for Emacs
(use-package etags
  :bind (
         :map esc-map
         ("#" . tags-query-replace)))

;;; helm-etags-plus --- Another Etags helm.el interface
;; This package use `helm' as an interface to find tag with Etags.
;;
;; It supports multiple tag files, and it can recursively searches
;; each parent directory for a file named 'TAGS'. so you needn't add
;; this special file to `tags-table-list'
(use-package helm-etags-plus
  :ensure t
  :after (helm)
  :bind (("M-." . helm-etags-plus-select)
         ;; `M-.' default use symbol under point as tagname
         ;; `C-uM-.' use pattern you typed as tagname
         ("M-*" . helm-etags-plus-history)
         ;; list all visited tags
         ("M-," . helm-etags-plus-history-go-back)
         ;; go back directly
         ("M-/" . helm-etags-plus-history-go-forward)))
;; go forward directly

;;; hippie-exp.el --- expand text trying various ways to find its expansion
(use-package hippie-exp
  :bind ("M-/" . hippie-expand))

;;; todo-mode.el --- facilities for making and maintaining todo lists
;; I would also recommend executing the following commands
;; so as to extend the bindings in your global keymap:
(use-package todo-mode
  :bind (("C-c t" . todo-show)          ;; switch to TODO buffer
         ("C-c i" . todo-insert-item))) ;; insert new item

;; Unter X11 die Taste "Del" zum Löschen vorwärts verfügbar machen
;; (geht nur mit GNU-Emacs Version >= 19.*) :
(use-package emacs
  :bind (
	 :map function-key-map
	 ([delete] . delete-char)))

;;; home-end.el --- Alternative Home and End commands.
;; Some useful bindings for Home and End keys:
;; Hit the key once to go to the beginning/end of a line,
;; hit it twice in a row to go to the beginning/end of the window,
;; three times in a row goes to the beiginning/end of the buffer.
;; N.B. there is no timeout involved.
(use-package home-end
  :ensure t
  :defines home-end-marker
  :bind (([end] . home-end-end)
         ([home] . home-end-home)))

;;; mouse.el --- window system-independent mouse support
(use-package mouse
  :custom
  (mouse-yank-at-point t)
  :config
  (context-menu-mode))

;;; scroll-bar.el --- window system-independent scroll bar support
(use-package scroll-bar
  :demand t
  :commands set-scroll-bar-mode
  :config
  (set-scroll-bar-mode 'right))

;;; mwheel.el --- Wheel mouse support
;; To enable wheel mouse support for emacs, simply put this at the top
;; of your .emacs file:
(use-package mwheel
  :demand t
  :commands mouse-wheel-mode
  :custom
  (mouse-wheel-tilt-scroll t)
  :config
  (mouse-wheel-mode))

;; > I want something similar to vi's % command, where it goes to
;; > the matching paren of bracket...
;; I hope this is what you want:
(use-package emacs
  :preface
  (defun goto-match-paren (arg)
    "Go to the matching parenthesis if on parenthesis otherwise insert %."
    (interactive "p")
    (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
          ((looking-at "\\s\)") (forward-char 1) (backward-list 1))
          (t (self-insert-command (or arg 1)))))
  :bind ("%" . goto-match-paren)
  :config
  ;; Indentation can insert tabs if this is non-nil.
  (indent-tabs-mode -1))

;;; which-key --- This is a rewrite of guide-key-mode for emacs.
;; The intention is to provide the following features:
;;
;; 1. A different polling mechanism to make it lighter on resources
;;    than guide-key
;; 2. An improved display of keys with more keys being shown by
;;    default and a nicer presentation
;; 3. Customization options that allow for the rewriting of command
;;    names on the fly through easily modifiable alists
;; 4. Good default configurations that work well with most themes
;; 5. A well configured back-end for displaying keys (removing the
;;    popwin dependency) that can be easily customized by writing new
;;    display functions
(use-package which-key
  :ensure t
  :functions which-key-mode
  :config
  (which-key-mode))

;;; beginend.el --- Redefine M-< and M-> for some modes
;; Redefine M-< and M-> for some modes
;;
;; - in dired mode, M-< (resp. M->) go to first (resp. last) file line
;;
;; - in message mode,
;;    - M-< go to first line of message body (after headings)
;;    - M-> go to last line before message signature
(use-package beginend
  :ensure t
  :functions beginend-setup-all
  :config
  (beginend-setup-all))

(el-init-provide)



;;; keys.el ends here
