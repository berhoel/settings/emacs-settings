;;; init-semantic.el --- Settings for Semantic package

;; Copyright © 2011, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:47 hoel>

;;; Commentary:

;; see <http://alexott.net/en/writings/emacs-devenv/EmacsCedet.html>

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(use-package init-my-switches :load-path "inits")

;;; cedet.el --- Setup CEDET environment
;; This file depends on the major components of CEDET, so that you can
;; load them all by doing (require 'cedet).  This is mostly for
;; compatibility with the upstream, stand-alone CEDET distribution.
(use-package cedet
  :after semantic)

;;; semantic/db-ebrowse.el --- Semanticdb backend using ebrowse.
(use-package semantic/db-ebrowse
  :commands semanticdb-load-ebrowse-caches)

;;; semantic/db-global.el --- Semantic database extensions for GLOBAL
(use-package semantic/db-global
  :after semantic/db-file
  :demand t
  :functions semanticdb-enable-gnu-global-databases
  :config
  (when my-isux
    (semanticdb-enable-gnu-global-databases 'c-mode t)
    (semanticdb-enable-gnu-global-databases 'c++-mode t)))

(use-package semantic/db-file
  :after semantic
  :custom
  (semanticdb-default-save-directory
   (concat user-emacs-local-directory "semanticdb")))

;;; idle.el --- Schedule parsing tasks in idle time
(use-package semantic/idle
  :commands global-semantic-idle-scheduler-mode global-semantic-idle-summary-mode
  :defines semantic-idle-work-update-headers-flag)

;;; semantic/db-mode.el --- Semanticdb Minor Mode
(use-package semantic/db-mode
  :commands global-semanticdb-minor-mode)

;;; semantic/mru-bookmark.el --- Automatic bookmark tracking
(use-package semantic/mru-bookmark
  :commands global-semantic-mru-bookmark-mode)

;;; semantic/decorate/mode.el --- Minor mode for decorating tags
(use-package semantic/decorate/mode
  :commands global-semantic-decoration-mode)

;; ;;; semantic/dep.el --- Methods for tracking dependencies (include files)
(use-package semantic/dep
  :after semantic
  :demand t
  :commands semantic-add-system-include
  :init
  ;; You can also explicitly specify additional paths for look up of
  ;; include files (and these paths also could vary for specific
  ;; modes). To add some path to list of system include paths, you can
  ;; use the semantic-add-system-include command, that accepts two
  ;; parameters — string with path to include files, and symbol,
  ;; representing name of major mode, for which this path will used.
  ;; For example:
  (when (file-exists-p "/usr/include/boost")
    (semantic-add-system-include "/usr/include/boost" 'c++-mode))
  (when (file-exists-p "/usr/local/gltools/include/boost_1_46_0/boost")
    (semantic-add-system-include
     "/usr/local/gltools/include/boost_1_46_0/boost" 'c++-mode)))

;;; cedet-m3.el --- A CEDET mode for binding mouse-3 convenience menu.
;; CEDET M3 is a generic minor mode for CEDET, the collection that
;; puts useful information into a context menu.  This context menu
;; is designed to be bound to mouse-3, and maximize a user's efficiency
;; by figuring out what the "best thing to do" might be, and collecting
;; those concepts together in the menu.
(use-package cedet/cedet-m3
  :disabled ; not in emacs bundled version
  :after semantic
  :commands global-cedet-m3-minor-mode)

;;; semantic.el --- Semantic buffer evaluator.
(use-package semantic
  :commands semantic-mode
  :preface
  (defun my@semantic@hook (&rest args)
    (imenu-add-to-menubar "TAGS"))
  :custom
  (semantic-default-submodes
   ;; Documentation:
   ;; List of auxiliary Semantic minor modes enabled by
   ;; `semantic-mode'. The possible elements of this list include
   ;; the following:
   '(;; Maintain tag database.
     global-semanticdb-minor-mode
     ;; Reparse buffer when idle.
     global-semantic-idle-scheduler-mode
     ;; Show summary of tag at point.
     ;; global-semantic-idle-summary-mode
     ;; Show completions when idle.
     global-semantic-idle-completions-mode
     ;; Additional tag decorations.
     ;; global-semantic-decoration-mode
     ;; Highlight the current tag.
     ;; global-semantic-highlight-func-mode
     ;; Show current fun in header line.
     global-semantic-stickyfunc-mode
     ;; Provide `switch-to-buffer'-like keybinding for tag names.
     ;; global-semantic-mru-bookmark-mode ; seems to cause problems
     ;; A mouse 3 context menu.
     ;; global-cedet-m3-minor-mode ; not in emacs bundled version
     ;; Highlight references of the symbol under point.
     global-semantic-idle-local-symbol-highlight-mode))
  ;; The following modes are more targeted at people who want to see
  ;; some internal information of the semantic parser in action:
  ;; ;; Visualize incremental parser by highlighting not-yet
  ;; ;; parsed changes.
  ;; global-semantic-highlight-edits-mode
  ;; ;; Highlight unmatched lexical syntax tokens.
  ;; ;; global-semantic-show-unmatched-syntax-mode
  ;; ;; Display the parser cache state.
  ;; global-semantic-show-parser-state-mode)
  :config
  ;; The Semantic package can be integrated with the imenu package.
  ;; This enables the display of a menu with a list of functions,
  ;; variables, and other tags. To enable this feature you can either
  ;; use semantic-load-enable-code-helpers, or, you need to add
  ;; following code into your initialization file:
  (when my-isux
    (add-hook 'semantic-init-hook #'my@semantic@hook))
  (semantic-mode))

;;; eieio.el --- Enhanced Implementation of Emacs Interpreted
;;;              Objects or maybe Eric's Implementation of Emacs
;;;              Interpreted Objects
(use-package eieio
  :if my-isux
  :after semantic)

;;; srecode.el --- Semantic buffer evaluator.
(use-package srecode
  :if my-isux
  :after semantic)

;;   (defun semantic-load-enable-minimum-features ()
;;     "Enable the minimum number of semantic features for basic usage.
;; This includes:
;;  `semantic-idle-scheduler-mode' - Keeps a buffer's parse tree up to date.
;;  `semanticdb-minor-mode' - Stores tags when a buffer is not in memory.
;;  `semanticdb-load-ebrowse-caches' - Loads any ebrowse dbs created earlier."
;;     (interactive)
;;     (global-semantic-idle-scheduler-mode 1)
;;     (global-semanticdb-minor-mode 1)

;;     ;; @todo - Enable this
;;     ;; (semanticdb-cleanup-cache-files t)

;;     ;; Don't do the loads from semantic-load twice.
;;     (when (null semantic-load-system-cache-loaded)

;;       ;; This loads any created system databases which get linked into
;;       ;; any searches performed.
;;       (setq semantic-load-system-cache-loaded t)

;;       ;; This loads any created ebrowse databases which get linked
;;       ;; into any searches performed.
;;       (when (and (not (featurep 'xemacs))
;;                  (boundp 'semanticdb-default-system-save-directory)
;;                  (stringp semanticdb-default-system-save-directory)
;;                  (file-exists-p semanticdb-default-system-save-directory))
;;         (semanticdb-load-ebrowse-caches))
;;       )
;;     )
;;   (defun semantic-load-enable-code-helpers ()
;;     "Enable some semantic features that provide basic coding assistance.
;; This includes `semantic-load-enable-minimum-features' plus:
;;   `imenu' - Lists Semantic generated tags in the menubar.
;;   `semantic-idle-summary-mode' - Show a summary for the tag indicated by
;;                                  code under point.  (intellisense)
;;   `senator-minor-mode' - Semantic Navigator, and global menu for all
;;                          features Semantic.
;;   `semantic-mru-bookmark-mode' - Provides a `switch-to-buffer' like
;;                        keybinding for tag names.

;; This also sets `semantic-idle-work-update-headers-flag' to t to
;; pre-build your database of header files in idle time for features
;; such as idle summary mode."
;;     (interactive)

;;     (semantic-load-enable-minimum-features)

;;     ;; This enables parsing of header files.
;;     (setq semantic-idle-work-update-headers-flag t)

;;     ;; (when (and (eq window-system 'x)
;;     ;;       (locate-library "imenu"))
;;     ;;   (add-hook 'semantic-init-hook '(lambda (&rest args)
;;     ;;                              (condition-case nil
;;     ;;					(imenu-add-to-menubar
;;     ;;                                   semantic-load-imenu-string)
;;     ;;                                (error nil)))))

;;     (global-semantic-idle-summary-mode 1)

;;     (global-semantic-mru-bookmark-mode 1)
;;     )

;; if you want to enable support for gnu global

;;; semantic/senator.el --- SEmantic NAvigaTOR
(use-package semantic/senator
  :if my-isux
  :after semantic)

;;; stickyfunc-enhance.el --- An enhancement to stock `semantic-stickyfunc-mode'
(use-package stickyfunc-enhance
  :ensure t
  :after semantic)

;; To use additional features for names completion, and displaying
;; of information for tags & classes, you also need to load the
;; semantic-ia package. This could be performed with following
;; command:
(use-package semantic/ia
  :if my-isux
  :after semantic)

;; If you use GCC for programming in C & C++, then Semantic can
;; automatically find path, where system include files are located.
;; To do this, you need to load semantic-gcc package with following
;; command:
(use-package semantic/bovine/gcc
  :if my-isux
  :after semantic)

;;; Srefactor --- A refactoring tool based on Semantic parser framework
(use-package srefactor
  :ensure t
  :after semantic
  :commands srefactor-refactor-at-point)

;; To enable code completion using Semantic, add the following code:
(use-package srecode/mode
  :after semantic
  :demand t
  :commands global-srecode-minor-mode
  :config
  (global-srecode-minor-mode 1)) ; Enable template insertion menu


;; SRecode
(el-init-provide)



;;; init-semantic.el ends here
