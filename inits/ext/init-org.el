;;; init-org.el --- settings for org.el

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-19 21:23:38 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(use-package init-my-switches :load-path "inits")
(use-package init-path :load-path "inits/ext")

;;; org.el --- Outline-based notes management and organizer
;; Activates `org-mode'
;; http://orgmode.org/guide/Activation.html#Activation
(use-package org
  :mode ("\\.org\\'" . org-mode)
  :bind (
         :map org-mode-map
         ("C-c l" . org-store-link)
         ("C-c a" . org-agenda)
         ("C-c b" . org-iswitchb))
  :commands org-display-inline-images org-babel-do-load-languages
  :custom
  (org-log-done t)
  :config
  (setq org-latex-default-packages-alist
	'(;;("AUTO" "inputenc"  t)
	  ;;("T1"   "fontenc"   t)
	  (""     "fixltx2e"  nil)
	  (""     "graphicx"  t)
	  (""     "longtable" nil)
	  (""     "float"     nil)
	  (""     "wrapfig"   nil)
	  (""     "rotating"  nil)
	  ("normalem" "ulem"  t)
	  (""     "amsmath"   t)
	  (""     "textcomp"  t)
	  (""     "marvosym"  t)
	  (""     "wasysym"   t)
	  (""     "amssymb"   t)
	  (""     "hyperref"  nil)
	  (""     "booktabs"  t)
	  "\\tolerance=1000"))
  (defvar org-eukleides-path "/usr/bin/eukleides")
  (org-babel-do-load-languages
   'org-babel-load-languages
   ;; load all language marked with (lang . t).
   '((C . t)
     (R . t) ; has to be remompiled
     (asymptote . t)
     (awk . nil)
     (calc . nil)
     (clojure . nil)
     (comint . nil)
     (css . t)
     (ditaa . nil)
     (dot . t)
     (emacs-lisp . t)
     (eukleides . nil)
     (fortran . t)
     (gnuplot . t)
     (haskell . nil)
     (io . nil)
     (java . nil)
     (js . t)
     (latex . t)
     (ledger . nil)
     (lisp . nil)
     (matlab . t)
     (maxima . t)
     (mscgen . nil)
     (ocaml . nil)
     (octave . t)
     (org . t)
     (perl . nil)
     (picolisp . nil)
     (plantuml)
     (python . t)
     (ref . nil)
     (ruby . nil)
     (sass . nil)
     (scala . nil) ;; to be taken from scala-mode if required
     (scheme . nil)
     (screen . nil)
     (shell . t)
     (shen . nil)
     (sql . t)
     (sqlite . t)))
  (when (require 'lilypond-mode nil 'noerror)
    (org-babel-do-load-languages
     'org-babel-load-languages
     (append org-babel-load-languages '((lilypond . t))))))

;;; org-contrib.el --- Unmaintained add-ons for Org-mode
(use-package org-contrib :ensure t)

;;; ox-latex.el --- LaTeX Back-End for Org Export Engine
(use-package ox-latex
  :after org
  :custom
  (org-latex-listings t)
  ;; Commands to process a LaTeX file to a PDF file.
  ;;
  ;; This is a list of strings, each of them will be given to the
  ;; shell as a command.  %f in the command will be replaced by the
  ;; relative file name, %F by the absolute file name, %b by the file
  ;; base name (i.e. without directory and extension parts), %o by the
  ;; base directory of the file, %O by the absolute file name of the
  ;; output file, %latex is the LaTeX compiler (see
  ;; ‘org-latex-compiler’), and %bib is the BibTeX-like compiler (see
  ;; ‘org-latex-bib-compiler’).
  ;;
  ;; The reason why this is a list is that it usually takes several
  ;; runs of ‘pdflatex’, maybe mixed with a call to ‘bibtex’.  Org
  ;; does not have a clever mechanism to detect which of these
  ;; commands have to be run to get to a stable result, and it also
  ;; does not do any error checking.
  ;;
  ;; Consider a smart LaTeX compiler such as ‘texi2dvi’ or ‘latexmk’,
  ;; which calls the "correct" combinations of auxiliary programs.
  ;;
  ;; Alternatively, this may be a Lisp function that does the
  ;; processing, so you could use this to apply the machinery of
  ;; AUCTeX or the Emacs LaTeX mode.  This function should accept the
  ;; file name as its single argument.
  (org-latex-pdf-process '("latexmk -gg -latex=xelatex -bibtex-cond -f %f"))
  :config
  (if my-iswork
      (add-to-list 'org-latex-classes
		   '("scrartcl"
		     "\\documentclass[paper=a4,%
twoside,%
10pt,%
headings=small,footinclude=false,%
headinclude,%
headlines,%
BCOR12mm,%
DIV14,%
colhead]{scrartcl}"
		     ("\\section{%s}" . "\\section*{%s}")
		     ("\\subsection{%s}" . "\\subsection*{%s}")
		     ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
		     ("\\paragraph{%s}" . "\\paragraph*{%s}")
		     ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))
  (if my-ishome
      (add-to-list 'org-latex-classes
		   '("scrartcl"
		     "\\documentclass[paper=a4,%
twoside,%
10pt,%
headings=small,footinclude=false,%
headinclude,%
headlines,%
BCOR12mm,%
DIV14,%
colhead]{scrartcl}
% [PACKAGES]
\\usepackage{fontspec}
% [EXTRA]
\\setmainfont[Path=/home/hoel/fonts/Decennie/,%
             BoldItalicFont=Decennie-JYBoldItalic,%
             BoldFont=Decennie-JYBold,%
             %Decennie-JYExpertItalic.otf,%
             %Decennie-JYExpertRoman.otf,%
             ItalicFont=Decennie-JYItalic,%
             %Decennie-JYOSFBoldItalic.otf,%
             %Decennie-JYOSFBold.otf,%
             %Decennie-JYOSFRoman.otf,%
             %Decennie-JYSCOSFRoman.otf,%
             %Decennie-JYTitlingRoman.otf,%
             ]{Decennie-JYRoman.otf}"
		     ("\\section{%s}" . "\\section*{%s}")
		     ("\\subsection{%s}" . "\\subsection*{%s}")
		     ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
		     ("\\paragraph{%s}" . "\\paragraph*{%s}")
		     ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))))

;;; org-bullets.el --- Show bullets in org-mode as UTF-8 characters
;; Show org-mode bullets as UTF-8 characters.
;; https://github.com/sabof/org-bullets
(use-package org-bullets
  :ensure t
  :hook (org-mode . org-bullets-mode))

;;; org-autolist.el --- Improved list management in org-mode
;; org-autolist makes org-mode lists behave more like lists in
;; non-programming editors such as Google Docs, MS Word, and OS X
;; Notes.
(use-package org-autolist
  :ensure t
  :hook (org-mode . org-autolist-mode))

;;; toc-org.el --- add table of contents to org-mode files (formerly,
;;;                org-toc)
;; toc-org helps you to have an up-to-date table of contents in org
;; files without exporting (useful primarily for readme files on
;; GitHub).
(use-package toc-org
  :ensure t
  :hook (org-mode . toc-org-enable))
;; And every time you'll be saving an org file, the first headline
;; with a :TOC: tag will be updated with the current table of
;; contents.

;;; org-cliplink.el --- insert org-mode links from the clipboard
;; Bind `org-cliplink` function to something. For example, put this
;; line in your init file:
(use-package org-cliplink
  :ensure t
  :after org
  :bind (
         :map org-mode-map
         ("C-x p i" . org-cliplink)))
;; Then copy any http/https URL to the clipboard, switch to the
;; Emacs window and hit `C-x p i`.

;;; org-projectile.el --- Repository todo management for org-mode
;; org-projectile provides functions for the creation of org-mode
;; TODOs that are associated with projectile projects.
;; Before using org-projectile, you must specify the file you would
;; like to use for storing project TODOs, as well a keybinding for
;; taking a project note with org-capture and a global keybinding
;; for org-projectile:project-todo-completing-read. It is
;; recommended that you start with the following configuration:
(use-package org-projectile
  :ensure t
  :after (:all projectile org-agenda)
  :functions org-projectile-project-todo-entry
  :bind (("C-c n p" . org-project-capture-project-todo-completing-read))
  :commands org-projectile-project-todo-entry
  :custom
  (org-projectile-projects-file "~/org/projects.org")
  :config
  (nconc org-agenda-files ( org-project-capture-project-todo-completing-read)))

;;; org-capture.el --- Fast note taking in Org-mode
(use-package org-capture
  :defines org-capture-templates
  :bind (("C-c c" . org-capture))
  :custom
  (org-capture-templates (append org-capture-templates (org-projectile-project-todo-entry))))

;;; org-projectile-helm.el --- helm functions for org-projectile
;; This package aims to provide an easy interface to creating per
;; project org-mode TODO headings.
(use-package org-projectile-helm
  :ensure t
  :after (:all helm projectile)
  :bind (("C-c n p" . org-projectile-helm-template-or-project)))

;;; org-repo-todo.el --- Simple repository todo management with
;;;                      org-mode
;; This is a simple package for capturing and visiting todo items for
;; the repository you are currently within. Under the hood it uses
;; `org-capture' to provide a popup window for inputting `org-mode'
;; checkboxed todo items (http://orgmode.org/manual/Checkboxes.html)
;; or regular ** TODO items that get saved to a TODO.org file in the
;; root of the repository.
(use-package org-repo-todo
  :ensure t
  :bind (
         :map ctl-x-map
         ("t ;" . ort/capture-todo)
         ;; ("C-'" . 'ort/capture-check item)
         ("t '" . ort/goto-todos)))

;;; org-agenda.el --- Dynamic task and appointment lists for Org
;; This file contains the code for creating and using the Agenda for
;; Org-mode.
(use-package org-agenda
  :bind ("C-c a" . org-agenda)
  :custom
  (org-agenda-files '("~/.agenda_files")))

;;; org-doing.el --- Keep track of what you're doing
;; Inspired by doing, a set of functions for keeping track of what
;; you're doing right now.
;;
;; How it works
;;
;; Creates a doing.org file in your home directory (or wherever
;; org-doing-file points to). The file has two sections: now and
;; later. The now section lists things you are currently working on.
;; The later section lists things you want to work on later.
(use-package org-doing
  :ensure t
  :commands org-doing org-doing-log)

;;; ob-core.el --- working with code blocks in org-mode
(use-package ob-core
  :hook (;; display/update images in the buffer after I evaluate
         (org-babel-after-execute . org-display-inline-images))
  :init
  ;; don't prompt me to confirm everytime I want to evaluate a block
  (setq org-confirm-babel-evaluate nil))

;;; auto-org-md --- export a markdown file automatically when you save
;;;                 an org-file
;; During editing an org file, M-x auto-org-md-mode toggles this
;; function between on/off.
(use-package auto-org-md
  :ensure t
  :disabled
  :hook (org-mode . auto-org-md-mode))

;;; org-attach-screenshot --- screenshots integrated with org
;;;                           attachment dirs
;; allows taking screenshots from within an emacs org buffer by using
;; the org-attach-screenshot command. The link to the file will be
;; placed at (point) and org inline images will be turned on to
;; display it.
;;
;; Screenshots are placed into the org entry's attachment directory.
;; If no attachment directory has been defined, the user will be
;; offered choices for creating one or using a directory of an entry
;; higher up in the hierarchy.
;;
;; The emacs frame from which the command is issued will hide away
;; during the screenshot taking, except if a prefix argument has been
;; given (so to allow taking images of the emacs session itself).
(use-package org-attach-screenshot
  :ensure t
  :if my-isux
  :after org
  :commands org-attach-screenshot)

;;; org-capture.el --- Fast note taking in Org
(use-package org-capture
  :commands org-capture org-capture-string
  :defines org-capture-templates
  :init
  (setq org-capture-templates
        '(("t" "Todo" entry (file+headline "~/org/gtd.org" "Tasks")
           "* TODO %?\n  %i\n  %a")
          ("j" "Journal" entry (file+olp+datetree "~/org/journal.org")
           "* %?\nEntered on %U\n  %i\n  %a"))))

;;; org-chef --- Cookbook and recipe management with org-mode.
;; Homepage: https://github.com/Chobbes/org-chef
;; org-chef is a package for managing recipes in org-mode.  One of the
;; main features is that it can automatically extract recipes from
;; websites like allrecipes.com
(use-package org-chef
  :ensure t
  :commands org-chef-get-recipe-from-url org-chef-insert-recipe
  :config
  (add-to-list 'org-capture-templates
               '("c" "Cookbook" entry (file "~/org/cookbook.org")
                 "%(org-chef-get-recipe-from-url)"
                 :empty-lines 1)))

;;; org-variable-pitch --- Minor mode for variable pitch text in org mode.
;; Homepage: https://www.gkayaalp.com/emacs.html#ovp
;; Variable-pitch support for org-mode.  This minor mode enables
;; ‘variable-pitch-mode’ in the current Org-mode buffer, and sets some
;; particular faces up so that they are are rendered in fixed-width
;; font.  Also, indentation, list bullets and checkboxes are displayed
;; in monospace, in order to keep the shape of the outline.
(use-package org-variable-pitch
  :ensure t
  :hook (org-mode . org-variable-pitch-minor-mode))

;;; ob-sql-mode --- SQL code blocks evaluated by sql-mode
(use-package ob-sql-mode
  :ensure t
  :preface
  (defun my@org@confirm@babel@evaluate@fun (lang body)
    (not (string= lang "sql-mode")))
  :after org
  ;; To guard against security risks, Org defaults to prompting for
  ;; confirmation every time you evaluate a code block (see Code
  ;; evaluation and security issues for details). To disable this for
  ;; ob-sql-mode blocks you can enter and evaluate the following.
  :custom
  (org-confirm-babel-evaluate #'my@org@confirm@babel@evaluate@fun))

;;; org-journal.el --- a simple org-mode based journaling mode
;; Functions to maintain a simple personal diary / journal in Emacs.
;; Feel free to use, modify and improve the code! - mtvoid, bastibe
(use-package org-journal
  :ensure t
  :bind ("C-c C-j" . org-journal-new-entry)
  :hook ((calendar-today-visible calendar-today-invisible) . org-journal-mark-entries)
  :custom
  (org-journal-dir (concat (getenv "HOME") "/journal")))
;; You also need to specify the directory where your journal files
;; will be saved. You can do this by setting the variable journal-dir
;; (remember to add a trailing slash). org-journal-dir is also a
;; customizable variable. The default value for journal-dir is
;; ~/Documents/journal/.

(el-init-provide)



;;; init-org.el ends here
