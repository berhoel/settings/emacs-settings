;;; init-git.el --- Settings for git support.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2024-01-11 10:38:18 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package)
  (require 'ert))

(require 'el-init)

(require 'bind-key)

;;; ssh-agency --- use ssh-agent on win32 from Emacs
;; This package provides functions to startup ssh-agent, prompt for
;; passphrases from the Windows console, and set the needed
;; environment variables in Emacs, so that pushes and pulls from magit
;; will not require entering any passphrase.
(use-package ssh-agency :ensure t)

;;; magit.el --- A Git porcelain inside Emacs
;; control Git from Emacs
(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)))

;;; magit-diff.el --- Inspect Git diffs
(use-package magit-diff
  :ensure magit
  :custom
  (magit-diff-refine-hunk 'all))

;; magit-annex       ;; Use git annex within magit
;; magit-filenotify  ;; Refresh status buffer when git tree changes
;; magit-find-file   ;; completing-read over all files in Git
;; magit-gerrit      ;; Magit plugin for Gerrit Code Review

;;; forge --- Access Git forges from Magit.
;; Other versions: 0.1.0 (melpa-stable).
;;
;; Work with Git forges, such as Github and Gitlab, from the comfort
;; of Magit and the rest of Emacs.
(use-package forge
  :ensure t
  :after magit)

;; magit-popup       ;; No description available.
;; magit-simple-keys ;; simple keybindings for Magit
;; magit-stgit       ;; StGit plug-in for Magit
;; magit-svn         ;; git-svn plug-in for Magit
;; magit-topgit      ;; topgit plug-in for Magit
;; magit-tramp       ;; git method for TRAMP

;;; git-lens.el --- Show new, deleted or modified files in branch
(use-package git-lens
  :ensure t
  :commands git-lens)

;;; transient.el --- Transient commands          -*- lexical-binding: t; -*-
(use-package transient
  :ensure t
  :after magit
  :functions transient-suffix-put
  :config
  (transient-suffix-put 'magit-dispatch "e" :description "vdiff (dwim)")
  (transient-suffix-put 'magit-dispatch "e" :command 'vdiff-magit-dwim)
  (transient-suffix-put 'magit-dispatch "E" :description "vdiff")
  (transient-suffix-put 'magit-dispatch "E" :command 'vdiff-magit))

;;; vdiff-magit -- The purpose of this Emacs package is to integrate
;;;                vdiff into magit, replacing ediff.
;; Loading vdiff-magit will pull in functions that will allow vdiff to
;; be used with magit. In order to use these functions you need to
;; call the vdiff functions instead of the magit-ediff ones. Here is a
;; very basic setup which replaces the basic ediff key bindings in
;; magit.
(use-package vdiff-magit
  :ensure t
  :after magit
  :defines vdiff-magit-use-ediff-for-merges vdiff-magit-dwim-show-on-hunks vdiff-magit-show-stash-with-index vdiff-magit-stage-is-2way
  :bind (
         :map magit-mode-map
         ("e" . vdiff-magit-dwim)
         ("E" . vdiff-magit))
  :custom
  ;; This flag will default to using ediff for merges. vdiff-magit does not yet
  ;; support 3-way merges. Please see the docstring of this variable for more
  ;; information
  (vdiff-magit-use-ediff-for-merges nil)
  ;; Whether vdiff-magit-dwim runs show variants on hunks. If non-nil,
  ;; vdiff-magit-show-staged or vdiff-magit-show-unstaged are called
  ;; based on what section the hunk is in. Otherwise, vdiff-magit-dwim
  ;; runs vdiff-magit-stage when point is on an uncommitted hunk.
  (vdiff-magit-dwim-show-on-hunks nil)
  ;; Whether vdiff-magit-show-stash shows the state of the index.
  (vdiff-magit-show-stash-with-index t)
  ;; Only use two buffers (working file and index) for vdiff-magit-stage
  (vdiff-magit-stage-is-2way nil))


;; bitbucket dependency is not available
;; ;;; git-commit-insert-issue.el --- Get issues list when typing "Fixes #"
;; ;; This library provides a minor mode and an interactive function to
;; ;; fetch issues of your project when you type "Fixes #" in a commit
;; ;; message. Github, Gitlab and Bitbucket.
;; (use-package git-commit-insert-issue
;;   :ensure t
;;   :hook (git-commit-mode . git-commit-insert-issue-mode))

;;; git-command --- Yet another Git interface
;; Homepage: https://github.com/10sr/git-command-el
;; This package provides a way to invoke Git shell command using minibuffer.
;; There is no major-mode nor minor-mode, you just have to remember usual Git
;; subcommands and options.
;;
;; This package provides only one user command: type
;;
;;     M-x RET git-command
;;
;; to input Git shell command to minibuffer that you want to invoke.
;; Before running git command `$GIT_EDITOR` and `$GIT_PAGER` are set nicely so
;; that you can seamlessly edit files or get pager outputs with Emacs you are
;; currently working on.
;;
;; Optionally, you can give prefix argument to create a new buffer for that git
;; invocation.
;;
;;
;; Completion
;; ----------
;;
;; It is highly recommended to Install `pcmpl-git` with this package to enable
;; completion when entering git command interactively.
(use-package git-command
  :ensure t
  :if my-isux)

;;; git-blamed --- Minor mode for incremental blame for Git
;; Here is an Emacs implementation of incremental git-blame. When you
;; turn it on while viewing a file, the editor buffer will be updated
;; by setting the background of individual lines to a color that
;; reflects which commit it comes from. And when you move around the
;; buffer, a one-line summary will be shown in the echo area.
(use-package git-blamed
  :ensure t
  :commands git-blamed-mode) ; Minor mode for incremental blame for Git.

;;; magit-todos.el --- Show source file TODOs in Magit
;; This package displays keyword entries from source code comments and
;; Org files in the Magit status buffer. Activating an item jumps to
;; it in its file. By default, it uses keywords from `hl-todo', minus
;; a few (like "NOTE").
(use-package magit-todos
  :ensure t
  :after (org)
  :after magit)

;;; gitattributes-mode --- Major mode for editing .gitattributes files
;; A major mode for editing .gitattributes files. See the
;; gitattributes(5) manpage for more information. `eldoc-mode' is
;; supported for known attributes.
(use-package gitattributes-mode
  :ensure git-modes
  :defer)

;;; gitconfig-mode --- Major mode for editing .gitconfig files
(use-package gitconfig-mode
  :ensure git-modes
  :defer)

;;; gitignore-mode --- Major mode for editing .gitignore files
(use-package gitignore-mode
  :ensure git-modes
  :defer)

;;; blamer.el --- Show git blame info about current line
(use-package blamer
  :ensure t
  :after magit
  :disabled t
  :bind (("s-i" . blamer-show-commit-info))
  :defer 20
  :custom
  (blamer-idle-time 0.3)
  (blamer-min-offset 50)
  :custom-face
  (blamer-face ((t (:inherit magit-blame-highlight))))
  :config
  (global-blamer-mode 1))

;;; my-repo-pins.el --- Keep your git repositories organized
;;
;; Open source developers often have to jump between projects, either
;; to read code, or to craft patches. My Repo Pins reduces the
;; friction so that it becomes trivial to do so.

;; The idea of the plugin is based on this idea; if the repository
;; URLs can be translated to a filesystem location, the local disk can
;; be used like a cache. My Repo Pins lazily clones the repo to the
;; filesystem location if needed, and then jumps into the project in
;; one single command. You don't have to remember where you put the
;; project on the local filesystem because it's always using the same
;; location. Something like this:
;;
;; ~/code-root
;; ├── codeberg.org
;; │   └── Freeyourgadget
;; │       └── Gadgetbridge
;; └── github.com
;;     ├── BaseAdresseNationale
;;     │   └── fantoir
;;     ├── mpv-player
;;     │   └── mpv
;;     └── NinjaTrappeur
;;         ├── cinny
;;         └── my-repo-pins.el
(use-package my-repo-pins
  :custom
  (my-repo-pins-code-root "~/work/code-root"))

;;; magit-stats.el --- Generates GIT Repo Statistics Report -*- lexical-binding: t; -*-
;; magit-stats generates reports containing statistics of your GIT Repositories.
;;
;; It uses the ~magit-stats~ npm package CLI Tool for NodeJS.
;;
;; It requires your system to run ~npx~ and have NodeJS
;; (node@latest) installed.  Please first install it if not yet present
;; in your system (see: https://nodejs.org/en/ and
;; https://www.npmjs.com/package/npx)
(use-package magit-stats
  :ensure t
  :commands magit-stats)

(el-init-provide)



;;; init-git.el ends here
