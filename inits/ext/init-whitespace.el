;;; init-whitespace.el --- Settings for `whitespace'.

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:46 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(use-package init-fontlock :load-path "inits/ext")

;;; unicode-whitespace.el --- teach whitespace-mode about fancy
;;;                           characters
;; Unicode-whitespace makes the built-in `whitespace-mode' Unicode-aware
;; in two different ways:
;;
;;     1. Recognizing Unicode whitespace characters in your buffer,
;;        such as "No-Break Space" or "Hair Space".
;;
;;     2. Displaying Unicode characters such as "Paragraph Sign"
;;        (pilcrow) in place of whitespace.
;;
;; This library also makes some minor adjustments to the default
;; settings of `whitespace-mode', and exposes character-by-character
;; display substitution mappings in customize.
(use-package unicode-whitespace
  :ensure t
  :functions unicode-whitespace-setup
  :config
  (unicode-whitespace-setup 'subdued-faces)) ; 'subdued-faces is optional

;;; whitespace.el --- minor mode to visualize TAB, (HARD) SPACE,
;;;                   NEWLINE
(use-package whitespace
  :commands global-whitespace-mode
  ;; How to Use and Setup Emacs's whitespace-mode

  ;; Xah Lee, 2009-08-14, …, 2011-11-03
  ;; Tweet

  ;; This page tells you how to setup emacs's whitespace-mode (bundled
  ;; with emacs 23), and how to use it.

  ;; whitespace-mode renders {spaces, tabs, newlines} characters with
  ;; a visible glyph. This feature is useful for working with
  ;; “comma/tab separated values” (CSV, TSV) that's commonly used
  ;; format for importing/exporting address books or spreadsheets.
  ;; It's also important in whitespace-significant langs such as
  ;; Python.

  ;; To use it, call whitespace-mode. (【Alt+x】) The command will
  ;; toggle it on and off, for current file. Call
  ;; global-whitespace-mode to toggle it globally for current emacs
  ;; session.

  ;; There is also whitespace-newline-mode and
  ;; global-whitespace-newline-mode. They only show newline chars.
  :custom
  ;; make whitespace-mode use just basic coloring
  ;; (setq whitespace-style (quote (spaces tabs newline space-mark
  ;;                                    tab-mark newline-mark)))
  ;; make whitespace-mode use “¶” for newline and “▷” for tab.
  ;; together with the rest of its defaults
  (whitespace-style (append whitespace-style '(big-indent lines-tail empty)))
  ;;         ;; WARNING: the mapping below has a problem.
  ;;         ;; When a TAB occupies exactly one column, it will display
  ;;         ;; the character ▷ at that column followed by a TAB which
  ;;         ;; goes to the next TAB column.
  ;;         ;; If this is a problem for you, please, comment the line
  ;;         ;; below.
  (whitespace-display-mappings
   (append whitespace-display-mappings
	   '((tab-mark 9 [9655 9] [187 9] [92 9]) ; tab, ▷
	     (newline-mark 10 [9166 10] [182 10] [36 10]) ; newlne, ¶
	     (space-mark 160 [164] [95]) 
	     (space-mark 2208 [2212] [95]) 
	     (space-mark 2336 [2340] [95]) 
	     (space-mark 3616 [3620] [95]) 
	     (space-mark 3872 [3876] [95])))) 
  :config
  (global-whitespace-mode))

;;; whitespace-cleanup-mode.el --- Intelligently call
;;;                                whitespace-cleanup on save
(use-package whitespace-cleanup-mode
  :ensure t
  :commands whitespace-cleanup-mode
  :hook (((python-mode
           org-mode
           rst-mode
           emacs-lisp-mode
           tex-mode
           latex-mode
           prog-mode
           c-mode-common) . whitespace-cleanup-mode)))

(el-init-provide)



;;; init-whitespace.el ends here
