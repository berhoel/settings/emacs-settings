;;; init-cerbere.el --- Settings for cebere.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:53:00 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; cerbere --- Unit testing in Emacs for several programming
;;;             languages
(use-package cerbere
  :ensure t
  :commands cerbere-mode
  :hook ((python-mode web-mode go-mode) . cerbere-mode))

(el-init-provide)



;;; init-cerbere.el ends here
