;;; init-timeclock.el --- Starting `timeclock'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 19:55:31 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

;;; time.el --- display time, load and mail indicator in mode line of Emacs
(use-package time
  :demand t
  :commands display-time-mode
  :custom
  (display-time-24hr-format t)
  :config
  (display-time-mode))

;;; timeclock.el --- mode for keeping track of how much you work
(use-package timeclock
  :demand t
  :commands timeclock-mode-line-display
  :bind (
         :map ctl-x-map
         ;; You'll probably want to bind the timeclock commands to
         ;; some handy keystrokes. At the moment, C-x t is unused:
         ("t i" . timeclock-in)
         ("t o" . timeclock-out)
         ("t c" . timeclock-change)
         ("t r" . timeclock-reread-log)
         ("t u" . timeclock-update-mode-line)
         ("t w" . timeclock-when-to-leave-string))
  :custom
  (setq timeclock-workday (* 7.8 60 60))
  :config
  (timeclock-mode-line-display))

(el-init-provide)



;;; init-timeclock.el ends here
