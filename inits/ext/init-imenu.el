;;; init-imenu.el --- Settings for `imenu'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:52 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

;;; imenu.el --- framework for mode-specific buffer indexes
(use-package imenu
  :bind ([M-S-mouse-3] . imenu))

;;; imenus.el --- Imenu for multiple buffers
(use-package imenus
  :ensure t
  :commands imenus imenus-mode-buffers imenus-files
  :hook (emacs-lisp-mode . imenu-add-menubar-index))

(el-init-provide)



;;; init-imenu.el ends here
