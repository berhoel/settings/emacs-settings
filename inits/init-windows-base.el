;;; init-windows.el --- emacs settings for Windows setup.

;; Copyright © 2013, 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:10 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(use-package init-my-switches
  :load-path "inits")

(use-package init-svn
  :load-path "inits/ext")

;;; system-specific-settings.el --- Apply settings only on certain
;;;                                 systems
(use-package system-specific-settings
  :ensure t
  :commands system-specific-settings-do-match)

(defun my@add@to@execpath@when@dir@exists (dir)
  "Add DIR to 'exec-path' if DIR exists."
  (when (file-accessible-directory-p dir)
    (add-to-list 'exec-path dir)))

(let ((my@win@dirs '("C:\\msys64\\usr\\bin"
                     "C:\\msys64\\mingw64\\bin"
                     "D:\\msys64\\usr\\bin"
                     "D:\\msys64\\mingw64\\bin"
                     "C:\\msys32\\usr\\bin"
                     "C:\\msys32\\mingw32\\bin"
                     "D:\\msys32\\usr\\bin"
                     "D:\\msys32\\mingw32\\bin"
                     ;;  "/cygdrive/c/cygwin64/bin/"
                     ;; "c:/cygwin64/bin/"
                     ;; "c:/cygwin/bin/"
                     ;; "/cygdrive/d/cygwin64/bin/"
                     ;; "d:/cygwin64/bin/"
                     ;; "d:/cygwin/bin/"
                     )))
  (dolist (dir my@win@dirs)
    (when (file-exists-p dir)
      (setenv "PATH" (concat (getenv "PATH") ";" dir))))

  (dolist (dir my@win@dirs)
    (when (file-exists-p dir)
      (add-to-list 'exec-path dir)))

;;; term.el --- general command interpreter in a window stuff
  (use-package term
    :config
    (dolist (dir my@win@dirs)
      (when (file-exists-p (concat dir "bash.exe"))
	(setq explicit-shell-file-name (concat dir "bash.exe"))
	(cl-return))))

  (dolist (dir my@win@dirs)
    (my@add@to@execpath@when@dir@exists dir)))

(my@add@to@execpath@when@dir@exists "C:\\Python27")
(my@add@to@execpath@when@dir@exists "C:\\Python27\\Scripts")
(my@add@to@execpath@when@dir@exists "D:\\Python27")
(my@add@to@execpath@when@dir@exists "D:\\Python27\\Scripts")
(my@add@to@execpath@when@dir@exists "C:\\Python27amd64")
(my@add@to@execpath@when@dir@exists
 "C:\\Program Files (x86)\\Microsoft Visual Studio\\Shared\\Python36_64")
(my@add@to@execpath@when@dir@exists
 (concat (getenv "APPDATA") "\\python\\Python36\\Scripts"))

(when (system-specific-settings-do-match system-type '('ms-dos 'windows-nt))
  (when explicit-shell-file-name
    (setq shell-file-name explicit-shell-file-name)))
(add-to-list 'exec-suffixes ".py")

;;   ;; Subprocesses invoked via the shell.
;;   ;; (setq shell-file-name "/bin/bash")
(when (boundp 'svn-status-svn-executable)
  (setq svn-status-svn-executable
	(cond
	 ((file-exists-p "c:/Program Files/TortoiseSVN/bin/svn.exe") "c:/Program Files/TortoiseSVN/bin/svn")
	 ((file-exists-p "c:/Programme/TortoiseSVN/bin/svn.exe") "c:/Programme/TortoiseSVN/bin/svn")
	 ((file-exists-p "/cygdrive/c/Program Files/TortoiseSVN/bin/svn.exe") "/cygdrive/c/Program Files/TortoiseSVN/bin/svn")
	 ((file-exists-p "/cygdrive/c/Programme/TortoiseSVN/bin/svn.exe") "/cygdrive/c/Programme/TortoiseSVN/bin/svn"))))

(when (file-accessible-directory-p
       (concat (getenv "APPDATA") "\\python\\Python36"))
  (unless (getenv "PYTHONUSERBASE")
    (setenv "PYTHONUSERBASE"
            (concat (getenv "APPDATA") "\\python"))))

(el-init-provide)



;;; init-windows.el ends here
