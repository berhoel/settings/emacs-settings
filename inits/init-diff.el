;;; init-diff.el --- Settings for `diff'.

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:12 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; diff-mode.el --- a mode for viewing/editing context diffs
(use-package diff-mode
  :mode (rx (or ".diff" ".diffs" ".patch" ".rej") eos))

;;; vdiff.el --- A tool like vimdiff for Emacs
;; To start a vdiff session, the main entry points are
;;
;; | Command                | Description                                                       |
;; |------------------------+-------------------------------------------------------------------|
;; | =vdiff-buffers=        | Diff two open buffers                                             |
;; | =vdiff-files=          | Diff two files                                                    |
;; | =vdiff-buffers3=       | Diff three open buffers                                           |
;; | =vdiff-files3=         | Diff three files                                                  |
;; | =vdiff-current-file=   | Like =ediff-current-file= (Diff buffer with disk version of file) |
;; | =vdiff-merge-conflict= | Use vdiff to resolve merge conflicts in current file              |
(use-package vdiff
  :ensure t
  :config
  (define-key vdiff-mode-map (kbd "C-c") vdiff-mode-prefix-map))
;; which will bind most of the commands under the =C-c= prefix when
;; vdiff-mode is active. Of course you can pick whatever prefix you
;; prefer. With the =C-c= prefix the commands would be

;; *** Basics

;; | Key     | Command                 | Description                        |
;; |---------+-------------------------+------------------------------------|
;; | =C-c g= | =vdiff-switch-buffer=   | Switch buffers at matching line    |
;; | =C-c n= | =vdiff-next-hunk=       | Move to next hunk in buffer        |
;; | =C-c p= | =vdiff-previous-hunk=   | Move to previous hunk in buffer    |
;; | =C-c h= | =vdiff-hydra/body=      | Enter vdiff-hydra                  |

;; *** Viewing and Transmitting Changes Between Buffers

;; | Key     | Command                            | Description                         |
;; |---------+------------------------------------+-------------------------------------|
;; | =C-c r= | =vdiff-receive-changes=            | Receive change from other buffer    |
;; | =C-c R= | =vdiff-receive-changes-and-step=   | Same as =C-c r= then =C-c n=        |
;; | =C-c s= | =vdiff-send-changes=               | Send this change(s) to other buffer |
;; | =C-c S= | =vdiff-send-changes-and-step=      | Same as =C-c s= then =C-c n=        |
;; | =C-c f= | =vdiff-refine-this-hunk=           | Highlight changed words in hunk     |
;; | =C-c x= | =vdiff-remove-refinements-in-hunk= | Remove refinement highlighting      |
;; | (none)  | =vdiff-refine-this-hunk-symbol=    | Refine based on symbols             |
;; | (none)  | =vdiff-refine-this-hunk-word=      | Refine based on words               |
;; | =C-c F= | =vdiff-refine-all-hunks=           | Highlight changed words             |
;; | (none)  | =vdiff-refine-all-hunks-symbol=    | Refine all based on symbols         |
;; | (none)  | =vdiff-refine-all-hunks-word=      | Refine all based on words           |

;; *** Folds

;; | Key     | Command                            | Description                         |
;; |---------+------------------------------------+-------------------------------------|
;; | =C-c N= | =vdiff-next-fold=                  | Move to next fold in buffer         |
;; | =C-c P= | =vdiff-previous-fold=              | Move to previous fold in buffer     |
;; | =C-c c= | =vdiff-close-fold=                 | Close fold at point or in region    |
;; | =C-c C= | =vdiff-close-all-folds=            | Close all folds in buffer           |
;; | =C-c t= | =vdiff-close-other-folds=          | Close all other folds in buffer     |
;; | =C-c o= | =vdiff-open-fold=                  | Open fold at point or in region     |
;; | =C-c O= | =vdiff-open-all-folds=             | Open all folds in buffer            |

;; *** Ignoring case and whitespace

;; | Key       | Command                   | Description             |
;; |-----------+---------------------------+-------------------------|
;; | =C-c i c= | =vdiff-toggle-case=       | Toggle ignoring of case |
;; | =C-c i w= | =vdiff-toggle-whitespace= | Toggle ignoring of case |

;; *** Saving, Updating and Exiting

;; | Key     | Command                 | Description                  |
;; |---------+-------------------------+------------------------------|
;; | =C-c w= | =vdiff-save-buffers=    | Save both buffers            |
;; | =C-c u= | =vdiff-refresh=         | Force diff refresh           |
;; | (none)  | =vdiff-restore-windows= | Restore window configuration |
;; | =C-c q= | =vdiff-quit=            | Quit vdiff                   |

(use-package abridge-diff
  :after magit
  :ensure t
  :init
  (abridge-diff-mode 1))

(el-init-provide)



;;; init-diff.el ends here
