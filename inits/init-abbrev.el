;;; init-abbrev.el --- Setup for `abbrev-mode'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:13 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(eval-when-compile
  (require 'use-package))

;;; abbrev.el --- abbrev mode commands for Emacs
(use-package abbrev
  :commands abbrev-mode
  :hook ((rst-mode c-mode-common f90-mode) . abbrev-mode)
  :custom
  (abbrev-file-name (concat user-emacs-directory "abbrev_defs")))

(el-init-provide)



;;; init-abbrev.el ends here
