;;; init-erc.el --- Settings for `erc'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:10 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; erc.el --- An Emacs Internet Relay Chat client
(use-package erc
  :commands erc)

;;; erc-log.el --- Logging facilities for ERC.
(use-package erc-log
  :after erc)

;;; erc-track.el --- Track modified channel buffers
(use-package erc-track
  :after erc
  :demand t
  :commands erc-track-mode
  :custom
  (erc-track-showcount t)
  :config
  (erc-track-mode 1))

;; Add score support to tracked channel buffers; Homepage:
;; http://julien.danjou.info/erc-track-score.html
(use-package erc-track-score
  :ensure t
  :after erc
  :demand t
  :commands erc-track-score-mode
  :config
  (erc-track-score-mode 1))

;;; erc-match.el --- Highlight messages matching certain regexps
(use-package erc-match
  :after erc)

;;; erc-view-log.el --- Major mode for viewing ERC logs
;; Homepage:
;; http://github.com/Niluge-KiWi/erc-view-log/raw/master/erc-view-log.el
(use-package erc-view-log
  :ensure t
  :after erc
  :config
  (add-to-list
   'auto-mode-alist
   `(,(format "%s/.*\\.log"
              (regexp-quote (expand-file-name erc-log-channels-directory)))
     . erc-view-log-mode)))

;;; autorevert.el --- revert buffers when files on disk change
(use-package autorevert
  :hook (erc-view-log-mode . turn-on-auto-revert-tail-mode))

(el-init-provide)



;;; init-erc.el ends here
