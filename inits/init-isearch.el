;;; init-isearch.el --- Settings for isearch mode.

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:39:53 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

;; Non-nil if searches and matches should ignore case.
(setq case-fold-search t)

;;; isearch.el --- incremental search minor mode
(use-package isearch
  :bind (("C-s" . isearch-forward-regexp)
         ("C-r" . isearch-backward-regexp)
         ("C-M-s" . isearch-forward)
         ("C-M-r" . isearch-backward)))

;;; isearch-project --- Incremental search through the whole project.
(use-package isearch-project
  :ensure t
  :bind (("C-S-s" . isearch-project-forward)))

;;; evil-search-highlight-persist --- Persistent highlights after
;;;                                   search
;; This extension will make isearch and evil-ex-search-incremental to
;; highlight the search term (taken as a regexp) in all the buffer and
;; persistently until you make another search or clear the highlights
;; with the evil-search-highlight-persist-remove-all command (default
;; binding to C-x SPC). This is how Vim search works by default when
;; you enable hlsearch.
(use-package evil :ensure t :defer)
(use-package evil-search-highlight-persist
  :ensure t
  :functions global-evil-search-highlight-persist
  :config
  (global-evil-search-highlight-persist))

;;; avy.el --- set-based completion
(use-package avy
  :ensure t
  ;; Jump to things in Emacs tree-style
  :bind (("þ" . avy-goto-char)
         ("C-'" . avy-goto-char-2)
         ("M-g f" . avy-goto-line)
         ("M-g w" . avy-goto-word-1)
         ("M-g e" . avy-goto-word-0)))

(el-init-provide)



;;; init-isearch.el ends here
