;;; init-linux-misc.el --- Misc. settings for linux OSs.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2024-05-10 16:27:58 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'cl-seq)

(require 'el-init)

;;; printing.el --- printing utilities
(use-package printing
  :commands pr-update-menus
  :init
  (pr-update-menus t))

;;; server.el --- Lisp code for GNU Emacs running as server process
;; start emacs as server when not in "gnus" mode
(use-package server
  :functions server-start
  :preface
  (defun my@server@hook@fun (&rest args)
    "Ignore ARGS."
    (unless server-kill-buffer-running
      (delete-frame)))
  :hook
  ((server-done . my@server@hook@fun))
  :custom
  (server-auth-dir (concat user-emacs-directory "server") "Directory for server authentication files.")
  :config
  (server-start))

;;; ps-print.el --- print text from the buffer as PostScript
;; print text from the buffer as PostScript
(use-package ps-print
  :custom
  (ps-lpr-command lpr-command "Name of program for printing a PostScript file.")
  (ps-lpr-switches lpr-switches "List of extra switches to pass to ‘ps-lpr-command’.")
  (ps-paper-type 'a4 "Specify the size of paper to format for.")
  (ps-printer-name nil "The name of a local printer for printing PostScript files."))

;;; ps-mule.el --- provide multi-byte character facility to ps-print
;; *Specifies the multi-byte buffer handling.
;;
;; Valid values are:
;;
;;   nil                  This is the value to use the default settings which
;;                        is by default for printing buffer with only ASCII
;;                        and Latin characters.  The default setting can be
;;                        changed by setting the variable
;;                        `ps-mule-font-info-database-default' differently.
;;                        The initial value of this variable is
;;                        `ps-mule-font-info-database-latin' (see
;;                        documentation).
;;
;;   `non-latin-printer'  This is the value to use when you have a Japanese
;;                        or Korean PostScript printer and want to print
;;                        buffer with ASCII, Latin-1, Japanese (JISX0208 and
;;                        JISX0201-Kana) and Korean characters.  At present,
;;                        it was not tested the Korean characters printing.
;;                        If you have a korean PostScript printer, please,
;;                        test it.
;;
;;   `bdf-font'           This is the value to use when you want to
;;                        print buffer with BDF fonts. BDF fonts
;;                        include both latin and non-latin fonts.
;;                        BDF (Bitmap Distribution Format) is a
;;                        format used for distributing X's font
;;                        source file. BDF fonts are included in
;;                        `intlfonts-1.2' which is a collection of
;;                        X11 fonts for all characters supported by
;;                        Emacs. In order to use this value, be sure
;;                        to have installed `intlfonts-1.2' and set
;;                        the variable `bdf-directory-list'
;;                        appropriately (see ps-bdf.el for
;;                        documentation of this variable).
;;
;;   `bdf-font-except-latin' This is like `bdf-font' except that it is used
;;                        PostScript default fonts to print ASCII and Latin-1
;;                        characters.  This is convenient when you want or
;;                        need to use both latin and non-latin characters on
;;                        the same buffer.  See `ps-font-family',
;;                        `ps-header-font-family' and `ps-font-info-database'.
;;
;; Any other value is treated as nil.
;; provide multi-byte character facility to ps-print
(use-package ps-mule
  :after ps-print
  :custom
  (ps-multibyte-buffer 'bdf-font-except-latin "Specifies the multi-byte buffer handling."))

;;; ps-bdf.el --- BDF font file handler for ps-print
;; BDF font file handler for ps-print
(use-package ps-bdf :after ps-print)

;;; notes-first.el --- Setup notes-mode before first use
(use-package notes-first
  :ensure notes-mode
  :if (not (file-exists-p (expand-file-name "~/.notesrc.el")))
  :commands notes-first-run-notes-init
  :config
  (setenv "NOTES_BIN_DIR"
          (car
           (sort
            (cl-remove-if-not
             #'file-accessible-directory-p
             (file-expand-wildcards
              (concat package-user-dir "notes-mode-*"))) #'string<))))
(unless (file-exists-p (expand-file-name "~/.notesrc.el"))
  (notes-first-run-notes-init))

;;; notes-mode.el --- Indexing system for on-line note-taking
(use-package notes-mode :ensure t)

;;; passmm.el --- A minor mode for pass (Password Store).
;; This is a minor mode that uses `dired' to display all password
;; files from the password store.
(use-package passmm
  :ensure t
  :commands passmm-list-passwords)

;;; pulseaudio-control --- Use pactl to manage PulseAudio volumes
;; The default keybindings in the `pulseaudio-control` keymap are:
;;
;; * +: Increase the volume of the currently-selected sink by
;;      `pulseaudio-control-volume-step` (`pulseaudio-control-increase-volume`).
;; * -: Decrease the volume of the currently-selected sink by
;;      `pulseaudio-control-volume-step` (`pulseaudio-control-decrease-volume`).
;; * v: Directly specify the volume of the currently-selected sink
;;      (`pulseaudio-control-set-volume`).  The value can be:
;;   * a percentage, e.g. '10%';
;;   * in decibels, e.g. '2dB';
;;   * a linear factor, e.g. '0.9' or '1.1'.
;; * m: Toggle muting of the currently-selected sink
;;      (`pulseaudio-control-toggle-current-sink-mute`).
;; * x: Toggle muting of a sink, specified by index
;;      (`pulseaudio-control-toggle-sink-mute-by-index`).
;; * e: Toggle muting of a sink, specified by name
;;      (`pulseaudio-control-toggle-sink-mute-by-name`).
;; * i: Select a sink to be the current sink, specified by index
;;      (`pulseaudio-control-select-sink-by-index`).
;; * n: Select a sink to be the current sink, specified by name
;;      (`pulseaudio-control-select-sink-by-name`).
(use-package pulseaudio-control
  :ensure t
  :functions pulseaudio-control-default-keybindings
  :config
  (pulseaudio-control-default-keybindings))

;;; chezmoi.el --- A package for interacting with chezmoi
;; Chezmoi is a dotfile management system that uses a source-target state
;; architecture.  This package provides convenience functions for maintaining
;; synchronization between the source and target states when making changes to
;; your dotfiles through Emacs.  It provides alternatives to `find-file' and
;; `save-buffer' for source state files which maintain synchronization to the
;; target state.  It also provides diff/ediff tools for resolving when dotfiles
;; get out of sync.  Dired and magit integration is also provided.
(use-package chezmoi
  :bind (("C-c C f" . chezmoi-find)
         ("C-c C s" . chezmoi-write)))

(el-init-provide)



;;; init-linux-misc.el ends here
