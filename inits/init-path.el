;;; init-path.el --- set loadpath on startup

;; Copyright © 2010, 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:21 hoel>

;; Keywords: convenience, files, local

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package))

(require 'el-init)

(add-to-list 'load-path (expand-file-name
			 (concat user-emacs-directory "site-lisp")) t)

(el-init-provide)



;;; init-path.el ends here
