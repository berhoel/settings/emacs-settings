;;; init-misc.el --- starting up misc modules. -*- lexical-binding: nil; -*-

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2024-03-27 11:54:43 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'quelpa)

(require 'bind-key)

(use-package init-my-switches :load-path "inits")

;;; saveplace.el --- automatically save place in files
;; Automatically save place in files, so that visiting them later
;; (even during a different Emacs session) automatically moves point
;; to the saved position, when the file is first found. Uses the value
;; of buffer-local variable save-place to determine whether to save
;; position or not.
(use-package saveplace
  :demand t
  :requires saveplace-pdf-view
  :config
  (save-place-mode 1))

;;; pdf-view-pagemark.el --- Add indicator in pdfview mode to show the page remaining
;; Because Emacs cannot show continuous pages at the same time, it is
;; difficult to keep track of the remaining text in current page, this
;; minor mode add a posframe indicator to tell you the line from
;; scroll up.
(use-package pdf-view-pagemark
  :demand t
  :after pdf-tools
  :hook (pdf-view-mode . pdf-view-pagemark-mode))

;;; select.el --- lisp portion of standard selection support
(use-package select
  :custom
  (select-enable-primary t))

;;; savehist.el --- Save minibuffer history
(use-package savehist
  :config
  (savehist-mode))

;;; Misc routines used by other modes

;;; page-break-lines --- Display ugly ^L page breaks as tidy
;;;                      horizontal lines
;; This library provides a global mode which displays form feed
;; characters as horizontal rules.
;;
;; Use `page-break-lines-mode' to enable the mode in specific buffers,
;; or customize `page-break-lines-modes' and enable the mode globally
;; with `global-page-break-lines-mode'.
(use-package page-break-lines
  :ensure t
  :functions global-page-break-lines-mode
  ;; :custom
  ;; (page-break-lines-modes (append page-break-lines-modes '(prog-mode)))
  :config
  (global-page-break-lines-mode))
;; Issues and limitations:
;;
;; If `page-break-lines-char' is displayed at a different width to
;; regular characters, the rule may be either too short or too long:
;; rules may then wrap if `truncate-lines' is nil. On some systems,
;; Emacs may erroneously choose a different font for the page break
;; symbol, which choice can be overridden using code such as:
;;
;; (set-fontset-font "fontset-default"
;;                  (cons page-break-lines-char page-break-lines-char)
;;                  (face-attribute 'default :family) text-scale-decrease)
;;
;; Use `describe-char' on a page break char to determine whether this
;; is the case.
;;
;; Additionally, the use of `text-scale-increase' or
;; `text-scale-decrease' will cause the rule width to be incorrect,
;; because the reported window width (in characters) will continue to
;; be the width in the frame's default font, not the scaled font used
;; to display the rule.

;;; smartparens.el --- Automatic insertion, wrapping and paredit-like
;;;                    navigation with user defined pairs.
(use-package smartparens
  :ensure t
  :commands smartparens-global-mode
  :hook (((markdown-mode prog-mode) . turn-on-smartparens-strict-mode))
  :custom
  (sp-base-key-bindings 'sp)
  :config
  (smartparens-global-mode 1))

;;; anzu --- Show number of matches in mode-line while searching
;; `anzu.el' is an Emacs port of `anzu.vim'.
;;
;; `anzu.el' provides a minor mode which displays 'current match/total
;; matches' in the mode-line in various search modes. This makes it
;; easy to understand how many matches there are in the current buffer
;; for your search query.
(use-package anzu
  :ensure t
  :functions global-anzu-mode
  :config
  (global-anzu-mode))

;;; ucs-utils --- Utilities for Unicode characters
;; This library provides utilities for manipulating Unicode
;; characters, with integrated ability to return fallback characters
;; when Unicode display is not possible.
;;
;; Some ambiguities in Emacs' built-in Unicode data are resolved, and
;; character support is updated to Unicode 7.0.
(use-package ucs-utils :ensure t)

;;; tablist --- Extended tabulated-list-mode ; required by pdf
;; This package adds marks and filters to tabulated-list-mode. It
;; also kind of puts a dired face on tabulated list buffers.
;;
;; It can be used by deriving from tablist-mode and some features by
;; using tablist-minor-mode inside a tabulated-list-mode buffer.
;; ! Required by `pdf-tools' !
(use-package tablist :ensure t :after pdf-tools)

;;; pdf-tools --- Support library for PDF documents.
;; PDF Tools is, among other things, a replacement of DocView for PDF
;; files. The key difference is, that pages are not prerendered by
;; e.g. ghostscript and stored in the file-system, but rather created
;; on-demand and stored in memory.
(use-package pdf-occur
  :ensure pdf-tools
  :commands pdf-occur-global-minor-mode)
(use-package pdf-tools
  :ensure t
  :unless my-iswin
  :functions pdf-tools-install
  :config
  (pdf-tools-install))

;;; saveplace-pdf-view.el --- Save place in pdf-view buffers
;; saveplace-pdf-view adds support for pdf-view buffers (see
;; pdf-tools) in the built-in mode save-place. This will store the
;; place (e.g. the current page and zoom) of PDF buffers under
;; pdf-view-mode, and revisiting the PDF files later will restore the
;; saved place.
(use-package saveplace-pdf-view :ensure t :defer t)

;;; open-junk-file --- Open a junk (memo) file to try-and-error
;; M-x `open-junk-file' opens a new file whose filename is derived
;; from current time. You can write short program in it. It helps to
;; try-and-error programs.
(use-package open-junk-file
  :ensure t
  :commands open-junk-file)

;;; fic-mode.el --- Show FIXME/TODO/BUG(...) in special face only in comments and strings
(use-package fic-mode
  :ensure
  :hook (prog-mode . fic-mode))

;;; tramp.el --- Transparent Remote Access, Multiple Protocol
;; This package provides remote file editing, similar to ange-ftp.
;; The difference is that ange-ftp uses FTP to transfer files between
;; the local and the remote host, whereas tramp.el uses a combination
;; of rsh and rcp or other work-alike programs, such as ssh/scp.
(use-package tramp
  :custom
  (vc-ignore-dir-regexp
   (format "\\(%s\\)\\|\\(%s\\)"
           vc-ignore-dir-regexp
           tramp-file-name-regexp))
  :config
  (defalias 'exit-tramp 'tramp-cleanup-all-buffers))

;;; auto-sudoedit.el --- Auto sudo edit by tramp
(use-package auto-sudoedit
  :if my-ishome
  :ensure t
  :config
  (auto-sudoedit-mode 1))

;;; helm-tramp.el --- Tramp helm interface for ssh, docker, vagrant
;; You can also use tramp with helm interface as root
(use-package helm-tramp
  :after (helm)
  :ensure t
  :bind ("C-c C-t" . helm-tramp))

;;; uniquify.el --- unique buffer names dependent on file name
;; uniquify.el causes Emacs to make buffer names unique not by adding
;; <2>, <3>, etc. to the end of the buffer name, but by adding
;; information about the directory in which the file appears.
;; For instance, buffers visiting /u/mernst/tmp/Makefile and
;; /usr/projects/zaphod/Makefile would be named Makefile|tmp and
;; Makefile|zaphod (instead of Makefile and Makefile<2>).
;;
;;                                      -Michael Ernst
;;                                       mernst@cs.washington.edu
(use-package uniquify
  :custom
  (uniquify-buffer-name-style 'post-forward))

;;; recentf.el --- setup a menu of recently opened files
;; When Recentf mode is enabled, a "Open Recent" submenu is displayed
;; in the "File" menu, containing a list of files that were operated
;; on recently.
(use-package recentf
  :defines recentf-save-file
  :custom
  ;; Cleanup the recent files list and synchronize it every 240
  ;; seconds.
  (recentf-save-file (concat user-emacs-local-directory "recentf")))

;;; sync-recentf --- Synchronize the recent files list between Emacs
;;;                  instances
;; This package helps synchronizing the recent files list between
;; emacs instances. Without it, each emacs instance manages its own
;; recent files list. The last one to close persistently saves its
;; list into recentf-save-file ; all files recently opened by other
;; instances are overwritten.
;;
;; With sync-recentf, all running emacs instances periodically
;; synchronize their local recent files list with recentf-save-file.
;; This ensures that all instances share the same list, which is
;; persistently saved across sessions.
(use-package sync-recentf
  :ensure t
  :config
  ;; Activate recentf
  (recentf-mode))

;;; persistent-scratch --- Preserve the scratch buffer across Emacs
;;;                        sessions
;; `persistent-scratch' is an Emacs package that preserves the state
;; of scratch buffers accross Emacs sessions by saving the state to
;; and restoring it from a file.
(use-package persistent-scratch
  :ensure t
  :functions persistent-scratch-setup-default
  :config
  (persistent-scratch-setup-default))

;;; with-editor --- Use the Emacsclient as $EDITOR
;; Use the Emacsclient as `$EDITOR' of child processes, making sure
;; they know how to call home. For remote processes a substitute is
;; provided, which communicates with Emacs on stdout instead of using
;; a socket as the Emacsclient does.
(use-package with-editor
  :ensure t
  :config
  (define-key (current-global-map)
              [remap async-shell-command] 'with-editor-async-shell-command)
  (define-key (current-global-map)
              [remap shell-command] 'with-editor-shell-command))

;;; redtick --- Smallest pomodoro timer (1 char)
;; This package provides a little pomodoro timer in the mode-line.
;;
;; After importing, it shows a little red tick (✓) in the mode-line.
;; When you click on it, it starts a pomodoro timer.
;;
;; It only shows the timer in the selected window (a moving timer
;; replicated in each window is a little bit distracting!).
(use-package redtick
  :ensure t
  :functions redtick
  :config
  (redtick))

;;; devdocs  --- Launch DevDocs search
;; DevDocs <http://devdocs.io/> is API Documentation Browser.  This
;;package allowing you to easily search the DevDocs Documentation.
(use-package devdocs :ensure t :defer t)

;;; elf-mode --- Show symbols in binaries
;; Use `elf-setup-default' to make `elf-mode' get called
;; automatically.
(use-package elf-mode
  :ensure t
  :magic ("ELF" . elf-mode))

;;; time-stamp.el --- Maintain last change time stamps in files edited by Emacs
;; Update the time stamp string(s) in the buffer. A template in a file
;; can be automatically updated with a new time stamp every time you
;; save the file. Add this line to your init file:
;;     (add-hook 'before-save-hook 'time-stamp)
;; or customize ‘before-save-hook’ through Custom. Normally the
;; template must appear in the first 8 lines of a file and look like
;; one of the following:
;;       Time-stamp: <>
;;       Time-stamp: " "
;; The time stamp is written between the brackets or quotes:
;;       Time-stamp: <2001-02-18 10:20:51 gildea>
;; The time stamp is updated only if the variable ‘time-stamp-active’
;; is non-nil. The format of the time stamp is set by the variable
;; ‘time-stamp-pattern’ or ‘time-stamp-format’. The variables
;; ‘time-stamp-pattern’, ‘time-stamp-line-limit’, ‘time-stamp-start’,
;; ‘time-stamp-end’, ‘time-stamp-count’, and
;; ‘time-stamp-inserts-lines’ control finding the template.
(use-package time-stamp
  :hook (before-save . time-stamp))

;;; diff-hl.el --- Highlight uncommitted changes using VC
;; `diff-hl-mode' highlights uncommitted changes on the side of the
;; window (using the fringe, by default), allows you to jump between
;; the hunks and revert them selectively.
(use-package diff-hl
  :ensure t
  :functions global-diff-hl-mode
  :config
  (global-diff-hl-mode))

;;; mule.el --- basic commands for multilingual environment
(use-package mule
  :commands set-terminal-coding-system
  :custom
  (default-input-method "german")
  :config
  (set-terminal-coding-system 'utf-8)
  (set-language-environment 'utf-8)
  (set-default-coding-systems 'utf-8)
  (prefer-coding-system 'utf-8))

(use-package emacs
  :init
  (setq locale-coding-system 'utf-8))

;;; ffap.el --- find file (or url) at point
(use-package ffap
  :custom
  (ffap-file-finder 'find-file)
  (ffap-require-prefix t)
  :config
  (ffap-bindings)) ;; do default key bindings

;;; filladapt.el --- Adaptive fill
;; These functions enhance the default behavior of Emacs' Auto Fill
;; mode and the commands fill-paragraph, lisp-fill-paragraph,
;; fill-region-as-paragraph and fill-region.

;; The chief improvement is that the beginning of a line to be filled
;; is examined and, based on information gathered, an appropriate
;; value for fill-prefix is constructed. Also the boundaries of the
;; current paragraph are located. This occurs only if the fill prefix
;; is not already non-nil.
(use-package filladapt
  :ensure t
  :commands filladapt-mode
  :defines filladapt-mode
  :hook ((find-file . filladapt-mode))
  :init
  (setq-default filladapt-mode t))

;;; shell.el --- specialized comint.el for running the shell
(use-package shell
  :defer t
  :custom
  (shell-pushd-tohome t))

;;; speedbar --- quick access to files and tags in a frame
(use-package speedbar
  :functions speedbar-add-supported-extension
  :custom
  (speedbar-tag-split-minimum-length 40)
  :config
  (dolist
      (ext
       '(".bnf" ".BNF" ".ansys" ".F\\\\(90\\\\|77\\\\|OR\\\\)?" ".com\\\\(mon\\\\)?" ".COM\\\\(MON\\\\)?" ".y" ".mp" ".pyf" ".mac" ".dat"))
    (speedbar-add-supported-extension ext)))

;;; uuidgen.el --- Provides various UUID generating functions
(use-package uuidgen
  :ensure t
  :commands
  ;; Generate time based UUID string, aka UUIDv1.
  uuidgen-1
  ;; Generate UUID string form random numbers, aka UUIDv4.
  uuidgen-4
  ;; Generate name based UUID string using MD5 hash
  ;; algorithm, aka UUIDv3. NS should be a generated UUID
  ;; or predefined namespaces, uuidgen-ns-dns',
  ;; uuidgen-ns-url', uuidgen-ns-oid', uuidgen-ns-x500'.
  ;; NAME is the node name string.
  uuidgen-3 ;; (ns name)
  ;; Generate name based UUID string using SHA-1 hash
  ;; algorithm, aka UUIDv5. NS should be a generated UUID
  ;; or predefined namespaces, uuidgen-ns-dns',
  ;; uuidgen-ns-url', uuidgen-ns-oid', uuidgen-ns-x500'.
  ;; NAME is the node name string.
  uuidgen-5 ;; (ns name)
  ;; Return the string representation of a UUID as a URN.
  uuidgen-urn ;; (uuid)
  ;; Return UUID string in CID format that is suitable for
  ;; COM definition. If UUID is nil will generate UUID-4
  ;; automatically. You customize
  ;; `uuidgen-cid-format-string' to change the default
  ;; format.
  uuidgen-cid ;; (&optional uuid)
  ;; Insert UUID string in CID format that is suitable for
  ;; COM definition. If UUID is nil will generate UUIDv4
  ;; automatically. You customize
  ;; `uuidgen-cid-format-string' to change the default
  ;; format.
  insert-uuid-cid ;; (uuid)
  ;; Insert UUIDv4 at point. If TIME-BASED is non-nil,
  ;; insert UUIDv1 instead.
  uuidgen ;; (time-based)
  )

;;; direnv.el --- Support for direnv
;; This package provides direnv integration for Emacs.
;; See the README for more information.
(use-package direnv
  :ensure t
  :functions direnv-mode
  :config
  (direnv-mode))

;;; subword.el --- Handling capitalized subwords in a nomenclature
(use-package subword
  :hook
  ((c-mode-common python-mode) . subword-mode))

;;; undo-tree.el --- Treat undo history as a tree  -*- lexical-binding: t; -*-
(use-package undo-tree
  :ensure t
  :functions global-undo-tree-mode
  :init
  (global-undo-tree-mode))

;;; json-mode.el --- Major mode for editing JSON files.
(use-package json-mode
  :ensure t
  :bind (
         :map json-mode-map
         ([C-f8] . prettier-prettify)))

;;; conf-mode.el --- Simple major mode for editing conf/ini/properties files
(use-package conf-mode
  :bind (
         :map conf-toml-mode-hook
         ([C-f8] . format-all-buffer)))

;;; yaml-mode.el --- Major mode for editing YAML files
(use-package yaml-mode
  :ensure t
  :bind (
         :map yaml-mode-map
         ([C-f8] . prettier-prettify)))

;;; prettier.el --- Code formatting with Prettier
(use-package prettier :ensure t)

;;; adoc-mode.el --- a major-mode for editing AsciiDoc files in Emacs
(use-package adoc-mode
  :ensure t
  :preface
  (defun my@adoc@mode@hook@fun (&rest args)
    "Ignore ARGS."
    (buffer-face-mode t))
  :commands adoc-mode
  :hook ((adoc-mode . my@adoc@mode@hook@fun)))

;;; casual.el --- Transient UI for Calc              -*- lexical-binding: t; -*-
;; Casual is an opinionated Transient-based porcelain for Emacs Calc.
(use-package casual
  :ensure t
  :after calc
  :bind (
         :map calc-mode-map
         ("C-c C-c" . casual-main-menu)))

;; Silence flymake warning.
(remove-hook 'flymake-diagnostic-functions 'flymake-proc-legacy-flymake)

(el-init-provide)



;;; init-misc.el ends here
