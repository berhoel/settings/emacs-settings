;;; init-theme.el --- settings for `color-theme`

;; Copyright © 2013, 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:31 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(use-package init-my-switches :load-path "inits")

(use-package select-themes
  :ensure t
  :commands select-themes)

;;; window.el --- GNU Emacs window commands aside from those written in C
(use-package window
  :no-require t
  :bind (([C-f1] . delete-other-windows)
	 ([C-f2] . delete-window)
	 ([C-f3] . other-window)
	 ([C-f4] . split-window-below)
	 ([C-f5] . split-window-right))
  :config
  (setq fit-window-to-buffer-horizontally 1))

;;; org-beautify-theme.el --- A sub-theme to make org-mode more beautiful.
(use-package org-beautify-theme
  :ensure t
  :if window-system)

;;; doom-modeline.el --- A minimal and modern mode-line
;; This package offers a fancy and fast mode-line inspired by minimalism design.
(use-package doom-modeline
  :ensure t
  :commands doom-modeline-mode
  :hook (after-init . doom-modeline-mode)
  :custom
  ;; If non-nil, cause imenu to see `doom-modeline' declarations.
  ;; This is done by adjusting `lisp-imenu-generic-expression' to
  ;; include support for finding `doom-modeline-def-*' forms.
  ;; Must be set before loading doom-modeline.
  (doom-modeline-support-imenu t)
  ;; Whether to use hud instead of default bar. It's only respected in GUI.
  (doom-modeline-hud t)
  ;; Determines the style used by `doom-modeline-buffer-file-name'.
  ;;
  ;; Given ~/Projects/FOSS/emacs/lisp/comint.el
  ;;   auto => emacs/l/comint.el (in a project) or comint.el
  ;;   truncate-upto-project => ~/P/F/emacs/lisp/comint.el
  ;;   truncate-from-project => ~/Projects/FOSS/emacs/l/comint.el
  ;;   truncate-with-project => emacs/l/comint.el
  ;;   truncate-except-project => ~/P/F/emacs/l/comint.el
  ;;   truncate-upto-root => ~/P/F/e/lisp/comint.el
  ;;   truncate-all => ~/P/F/e/l/comint.el
  ;;   truncate-nil => ~/Projects/FOSS/emacs/lisp/comint.el
  ;;   relative-from-project => emacs/lisp/comint.el
  ;;   relative-to-project => lisp/comint.el
  ;;   file-name => comint.el
  ;;   buffer-name => comint.el<2> (uniquify buffer name)
  ;;
  ;; If you are experiencing the laggy issue, especially while editing remote files
  ;; with tramp, please try `file-name' style.
  ;; Please refer to https://github.com/bbatsov/projectile/issues/657.
  (doom-modeline-buffer-file-name-style 'relative-from-project)
  ;; Whether to use unicode as a fallback (instead of ASCII) when not using icons.
  (doom-modeline-unicode-fallback t)
  ;; If non-nil, only display one number for checker information if applicable.
  (doom-modeline-checker-simple-format nil)
  ;; Whether display the GitHub notifications. It requires `ghub' package.
  (doom-modeline-github t)
  ;; Whether display the gnus notifications.
  ;; (doom-modeline-gnus t)
  ;; Whether gnus should automatically be updated and how often (set to 0 or smaller than 0 to disable)
  ;; (doom-modeline-gnus-timer 4)
  ;; Whether display the IRC notifications. It requires `circe' or `erc' package.
  (doom-modeline-irc nil)
  ;; Whether display the environment version.
  (doom-modeline-env-version t))

;;; pdf-view.el --- View PDF documents.
(use-package pdf-view
  :ensure pdf-tools
  :commands pdf-view-midnight-minor-mode)

;;; Modus Themes for GNU Emacs
;; This is a set of accessible themes for GNU Emacs. The contrast
;; ratio between foreground and background values should always be >=
;; 7:1, which conforms with the WCAG AAA accessibility standard. This
;; is the highest standard of its kind.
(use-package modus-themes
  :ensure t
  :preface
  (defun my@pdf@tools@midnight@mode@toggle ()
    (when (derived-mode-p 'pdf-view-mode)
      (if (eq (car custom-enabled-themes) 'modus-vivendi-tinted)
          (pdf-view-midnight-minor-mode 1)
	(pdf-view-midnight-minor-mode -1))))
  (defun my@pdf@tools@themes@toggle ()
    (mapc
     (lambda (buf)
       (with-current-buffer buf
	 (my@pdf@tools@midnight@mode@toggle)))
     (buffer-list)))
  :defines modus-themes-italic-constructs modus-themes-bold-constructs
  :defines modus-themes-mixed-fonts modus-themes-variable-pitch-ui
  :hook
  ((pdf-tools-enabled . my@pdf@tools@midnight@mode@toggle)
   (modus-themes-after-load-theme . my@pdf@tools@themes@toggle))
  :custom
  ;; Add all your customizations prior to loading the themes
  (modus-themes-italic-constructs t)
  (modus-themes-bold-constructs t)
  (modus-themes-variable-pitch-ui t))

;;; circadian.el --- Theme-switching for Emacs based on daytime Logo Circadian
;; Circadian tries to help reducing eye strain that may arise from
;; difference of your display brightness and the surrounding light.
(use-package circadian
  :ensure t
  :after solar
  :functions circadian-setup
  :preface
  (defun my@circardian@set@fonts ()
    (let ((fontsize (if my-iswork 115 135)))
      (set-face-attribute 'default nil
			  :family "JetBrains Mono"
			  :foundry "JB"
			  :slant 'normal
			  :weight 'normal
			  :height fontsize
			  :width 'normal))
    (set-face-attribute 'fixed-pitch nil :family (face-attribute 'default :family))
    (set-face-attribute 'variable-pitch nil :family "Source Sans Pro"))
  (defun my@circardian@setup (&rest args)
    (circadian-setup)
    (my@circardian@set@fonts))
  (defun my@circardian@before@load@theme (&rest args)
    (dolist (theme custom-enabled-themes)
      (disable-theme theme)))
  (defun my@circardian@after@load@theme (&rest args)
    (load-theme 'org-beautify t)
    (doom-modeline-mode)
    (my@circardian@set@fonts))
  :hook ((desktop-after-read . my@circardian@setup)
	 (circadian-after-load-theme . my@circardian@after@load@theme)
	 (circadian-before-load-theme . my@circardian@before@load@theme))
  :custom
  (circadian-themes
   '((:sunrise . modus-operandi-tinted)
     (:sunset . modus-vivendi-tinted))))

;; ;;; hl-line.el --- highlight the current line.
;; (use-package hl-line
;;   :hook (((dired-mode
;; 	   svn-status
;; 	   gnus-group-prepare
;; 	   gnus-summary-prepare) . hl-line-mode)
;; 	 (after-init . global-hl-line-mode))
;;   :init
;;   ;; Face with which to highlight the current line in Hl-Line mode.
;;   ;; (setq hl-line-face 'hl-line)
;;   ;; If `global-hl-line-sticky-flag' is non-nil, Global Hl-Line mode
;;   ;; highlights the line about the current buffer's point in all
;;   ;; windows.
;;   (setq global-hl-line-sticky-flag t))

;;; lin --- Make `hl-line-mode' more suitable for selection UIs
(use-package lin
  :ensure t
  :functions lin-global-mode
  :defines lin-mode-hooks
  :custom
  (lin-face 'lin-blue)
  :config
  (lin-global-mode 1))

;;; minimap --- Sidebar showing a "mini-map" of a buffer
;; This file is an implementation of a minimap sidebar, i.e., a
;; smaller display of the current buffer on the left side.  It
;; highlights the currently shown region and updates its position
;; automatically.  You can navigate in the minibar by dragging the
;; active region with the mouse, which will scroll the corresponding
;; edit buffer.  Additionally, you can overlay information from the
;; tags gathered by CEDET's semantic analyzer.
;;
;; Simply use M-x minimap-mode to toggle activation of the minimap.
;; Use 'M-x customize-group RET minimap RET' to adapt minimap to your
;; needs.
(use-package minimap :ensure t :defer t)

;;; electric-cursor.el --- Change cursor automatically
(use-package electric-cursor
  :ensure t
  :functions electric-cursor-mode
  :config
  (electric-cursor-mode))

(el-init-provide)



;;; init-theme.el ends here
