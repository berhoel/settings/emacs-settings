;;; init-make-mode.el --- Settings for `make-mode'.

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:28 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'init-autoinsert)
(require 'init-progmodes)

;;; make-mode.el --- makefile editing commands for Emacs
(use-package make-mode
  :mode ((rx (or (: bos (or "Makefile" "makefile" "GNUMakefile") eos)
                 (: ".mak" eos))) . makefile-mode)
  :custom
  ;; If non-nil, Makefile mode should install electric keybindings.
  (makefile-electric-keys t))

(defun my@insert@makefile@head ()
  "Insert yas head for Makefiles."
  (my-yas-expand-by-uuid 'makefile-mode "head"))
(define-auto-insert "\\(?:\\`\\(?:\\(?:\\(?:GNUM\\|[Mm]\\)akefile\\)\\)\\|\\.mak\\)\\'"
  #'my@insert@makefile@head)

;;; makefile-executor --- Emacs helpers to run things from makefiles
;; A set of tools aimed at working with Makefiles on a project level.
;;
;; Currently available:
;; - Interactively selecting a make target and running it.
;;   Bound to 'C-c C-e' when 'makefile-executor-mode' is enabled.
;; - Calculation of variables et.c.; $(BINARY) will show up as what it
;;   evaluates to.
;; - Execution from any buffer in a project.  If more than one is found,
;;   an interactive prompt for one is shown.  This is added to the
;;   `projectile-commander' on the 'm' key.
;;
;; To enable it, use the following snippet to add the hook into
;; 'makefile-mode':
(use-package makefile-executor
  :ensure t
  :hook (makefile-mode . makefile-executor-mode))

(el-init-provide)



;;; init-make-mode.el ends here
