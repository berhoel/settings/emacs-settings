;;; init-linux-scad.el --- Settings for OpenSCAD access from emacs.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:24 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; scad-mode --- A major mode for editing OpenSCAD code
;; This is a major-mode to implement the SCAD constructs and
;; font-locking for OpenSCAD
(use-package scad-mode
  :ensure t
  :mode ((rx ".scad" eos) . scad-mode))

;;; scad-preview --- Preview SCAD models in real-time within Emacs
;; Install `scad-mode' and load this script:
;;
;;   (require 'scad-preview)
;;
;; then call `scad-preview-mode' in a `scad-mode' buffer.
;;
;; You can rotate the preview image with following keybinds:
;;
;; - <right>   scad-preview-rotz+
;; - <left>    scad-preview-rotz-
;; - <up>      scad-preview-dist-
;; - <down>    scad-preview-dist+
;; - C-<left>  scad-preview-roty+
;; - C-<right> scad-preview-roty-
;; - C-<up>    scad-preview-rotx+
;; - C-<down>  scad-preview-rotx-
;; - M-<left>  scad-preview-trnsx+
;; - M-<right> scad-preview-trnsx-
;; - M-<up>    scad-preview-trnsz-
;; - M-<down>  scad-preview-trnsz+
;; - r         scad-preview-reset-camera-parameters
;;
;; You can also use "hjkl" or "npbf" instead of arrow keys.
(use-package scad-preview
  :ensure t
  :commands scad-preview-mode)

(el-init-provide)



;;; init-linux-scad.el ends here
