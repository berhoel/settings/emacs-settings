;;; init-cmake-mode.el --- Setup for CMake support.

;; Copyright © 2016, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:43 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'init-cc-mode)
(require 'init-eldoc)

;;; cmake-mode --- major-mode for editing CMake sources
(use-package cmake-mode :ensure t)

;;; cmake-font-lock --- Advanced, type aware, highlight support for
;;;                     CMake
(use-package cmake-font-lock
  :ensure t
  :hook (cmake-mode . cmake-font-lock-activate))

;;; cpputils-cmake --- Easy real time C++ syntax check and
;;;                    intellisense if you use CMake
(use-package cpputils-cmake
  :ensure t
  :disabled
  :preface
  (defun my@conditional@cppcm@reload@all (&rest args)
    "Execute `cppcm-reload-all' if in c[++]-mode derived mode."
    (if (derived-mode-p 'c-mode 'c++-mode)
        (cppcm-reload-all)))
  :hook ((c-mode-common . my@conditional@cppcm@reload@all)
         ;; OPTIONAL, somebody reported that they can use this package with
         ;; Fortran
         (f90-mode . cppcm-reload-all))
  :custom
  (cppcm-build-dirname "cbuild"))

;;; cmake-project.el --- Integrates CMake build process with Emacs
(use-package cmake-project
  :ensure t
  :commands cmake-project-mode)

;;; cmake-ide.el --- Calls CMake to find out include paths and other
;;;                  compiler flags
;; This package runs CMake and sets variables for IDE-like
;; functionality provided by other packages such as:
;; On the fly syntax checks with flycheck
;; auto-completion using auto-complete-clang or company-clang
;; Jump to definition and refactoring with rtags
(use-package cmake-ide
  :ensure t
  :disabled
  :preface
  (defun maybe-cmake-project-hook ()
    (if (file-exists-p "CMakeLists.txt") (cmake-project-mode)))
  :hook ((c-mode . maybe-cmake-project-hook)
         (c++-mode . maybe-cmake-project-hook))
  :config
  (cmake-ide-setup))

;;; helm-ctest --- Run ctest from within emacs
(use-package helm-ctest
  :ensure t
  :after (helm)
  :defer t)

;;; eldoc-cmake.el --- Eldoc support for CMake
;; CMake eldoc support, using a pre-generated set of docstrings from
;; CMake's documentation source.
(use-package eldoc-cmake
  :ensure t
  :hook (cmake-mode . eldoc-cmake-enable))


(el-init-provide)



;;; init-cmake-mode.el ends here
