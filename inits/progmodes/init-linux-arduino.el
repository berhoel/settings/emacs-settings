;;; init-linux-arduino.el --- Settings for editing arduino code from emacs.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:42 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'init-company)

;;; Arduino-mode.el --- Major mode for the Arduino language
;; Based on derived-mode-ex.el found here:
;;
;; <http://cc-mode.sourceforge.net/derived-mode-ex.el>.
(use-package arduino-mode
  :ensure t
  :mode (rx (or ".pde" ".ino") eos))

;; ;;; company-arduino --- company-mode for Arduino
;; ;; This package is a set of configuration to let you auto-completion
;; ;; by using `irony-mode', `company-irony' and `company-c-headers' on
;; ;; arduino-mode.
;; ;;
;; ;; Prerequisite:
;; ;; This package need irony-mode, company-irony and company-c-headers
;; ;; packages. Please install those packages first. But if you are using
;; ;; MELPA, package.el will install dependencies automatically when you install
;; ;; this package. Also, you need irony-mode's setting, so please follow
;; ;; the irony-mode's instruction (https://github.com/Sarcasm/irony-mode).
;; ;;
;; ;; Although you may prepared Arduino environment, you need it as well to
;; ;; refer to Arduino's header files. You can download from Arduino's web site
;; ;; (http://www.arduino.cc/en/Main/Software)
;; ;;
;; ;; Usage:
;; ;; Set $ARDUINO_HOME environment variable as Arduino IDE's installed directory.
;; ;; (i.e., export ARDUINO_HOME=$HOME/share/devkit/arduino-1.6.3 on zsh)
;; ;;
;; ;; Then put following configurations to your .emacs or somewhere.
;; (use-package company-arduino
;;   :ensure t
;;   :after company
;;   :commands company-arduino-append-include-dirs
;;   ;; Configuration for irony.el
;;   ;; Add arduino's include options to irony-mode's variable.
;;   :hook (irony-mode . company-arduino-turn-on)
;;   :preface
;;   ;; Configuration for company-c-headers.el
;;   ;; The `company-arduino-append-include-dirs' function appends
;;   ;; Arduino's include directories to the default directories
;;   ;; if `default-directory' is inside `company-arduino-home'. Otherwise just
;;   ;; returns the default directories.
;;   ;; Please change the default include directories accordingly.
;;   (defun my@company@c@headers@get@system@path (&rest args)
;;     "Return the system include path for the current buffer."
;;     (let ((default '("/usr/include/" "/usr/local/include/")))
;;       (company-arduino-append-include-dirs default t)))
;;   :custom
;;   (company-c-headers-path-system (my@company@c@headers@get@system@path))
;;   ;; If you are already using ‘company-irony’ and ‘company-c-headers’,
;;   ;; you might have same setting. That case, you can omit below setting.
;;   (company-backends
;;    (nconc company-backends '(company-irony company-c-headers))))

;;; arduino-cli-mode.el --- Arduino-CLI command wrapper
;; arduino-cli-mode is an Emacs minor mode for using the excellent new
;; arduino command line interface in an Emacs-native fashion. The mode
;; covers the full range of arduino-cli features in an Emacs native
;; fashion. It even leverages the infinite power the GNU to provide
;; fussy-finding of libraries and much improved support for handling
;; multiple boards. The commands that originally require multiple
;; steps (such as first searching for a library and then separately
;; installing it) have been folded into one.
(use-package arduino-cli-mode
  :ensure t
  :hook arduino-mode
  ;; :mode "\\.ino\\'"
  :custom
  (arduino-cli-warnings 'all)
  (arduino-cli-verify t))

(el-init-provide)



;;; init-linux-arduino.el ends here
