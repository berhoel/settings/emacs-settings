;;; init-cakecrumbs.el --- Configuration for cakecrumbs.el.

;; Copyright © 2018, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:31 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package)
  (load-library "sgml-mode"))

(require 'el-init)

;;; cakecrumbs --- Show parents on header for HTML/Jade/Sass/Stylus
;; Display parents’ chain on header for:
;;   HTML
;;   Jade / Pug
;;   SCSS / LESS
;;   Stylus / Sass
;; Life is painful, see screenshot on Github to know what on earth is this
;; doing::
;;   https://github.com/kuanyui/cakecrumbs.el
(use-package cakecrumbs
  :ensure t
  :after (:any
          html-mode web-mode nxml-mode sgml-mode yajade-mode jade-mode pug-mode
          scss-mode less-css-mode css-mode stylus-mode sass-mode)
  :config
  ;; Auto `add-hook' for above major-mode.  (Auto enable `cakecrumbs'
  ;; for the major-modes which have specify in above variables)
  ;; This automatically do this for you:
  ;; (add-hook 'MODE-HOOK 'cakecrumbs-enable-if-disabled)
  (cakecrumbs-auto-setup))

(el-init-provide)



;;; init-cakecrumbs.el ends here
