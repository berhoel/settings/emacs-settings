;;; init-misc-progmodes.el --- recognize additional programming modes

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-12-21 18:42:54 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(require 'init-autoinsert)
(require 'init-html)
(require 'init-path)
(require 'init-progmodes)

;;; mmm-auto.el --- loading and enabling MMM Mode automatically
;; MMM Mode is a minor mode for Emacs that allows Multiple Major Modes
;; to coexist in one buffer.
(use-package mmm-mode
  :ensure t
  :custom
  (mmm-global-mode 'maybe))
;; (mmm-add-classes
;;  '((python-rst
;;     :submode rst-mode
;;     :front "^ *[ru]?\"\"\"[^\"]*$"
;;     :back "^ *\"\"\""
;;     :include-front t
;;     :include-back t
;;     :end-not-begin t)))
;; (add-to-list 'mmm-mode-ext-classes-alist
;;              '(python-mode nil python-rst))

;; (use-package mmm-sample
;;   :after mmm-mode
;;   :config
;;   (add-to-list 'mmm-global-classes 'file-variables))

;;; generic-x.el --- A collection of generic modes
;; This file contains a collection of generic modes.
(use-package generic-x :defer t)

;;; csharp-mode.el --- C# mode derived mode
;; Major mode for editing C# code.
(use-package csharp-mode :ensure t :disabled t)

;;; tcl.el --- Tcl code editing commands for Emacs
(use-package tcl
  :disabled t
  :mode ((rx (or ".template" ".tcl" ".itcl") eos) . tcl-mode)
  :interpreter ((rx (or "tclsh" "wish" "tcsh")) . tcl-mode))

;;; meta-mode.el --- major mode for editing Metafont or MetaPost
;;;                  sources
(use-package meta-mode
  :mode (((rx ".mf" eos) . metafont-mode)
         ((rx ".mp" eos) . metapost-mode)))

;;; forth-mode.el --- major mode for editing (G)Forth sources
(use-package forth-mode :ensure t :disabled t)

;;; csv-mode.el --- Major mode for editing comma/char separated values
;; This package implements CSV mode, a major mode for editing records
;; in a generalized CSV (character-separated values) format.  It binds
;; finds with prefix ".csv" to `csv-mode' in `auto-mode-alist'.
(use-package csv-mode :ensure t :defer t)

;;; ssh-config-mode.el --- Mode for fontification of ~/.ssh/config
;; - Fontifys the ssh config keywords.
;; - keys for skipping from host section to host section.
;; - Add the following to your startup file.
(use-package ssh-config-mode :ensure t :defer t)

;;; js2-mode.el --- Improved JavaScript editing mode
;; This JavaScript editing mode supports:
;;
;;  - strict recognition of the Ecma-262 language standard
;;  - support for most Rhino and SpiderMonkey extensions from 1.5 and up
;;  - parsing support for ECMAScript for XML (E4X, ECMA-357)
;;  - accurate syntax highlighting using a recursive-descent parser
;;  - on-the-fly reporting of syntax errors and strict-mode warnings
;;  - undeclared-variable warnings using a configurable externs framework
;;  - "bouncing" line indentation to choose among alternate indentation points
;;  - smart line-wrapping within comments and strings
;;  - code folding:
;;    - show some or all function bodies as {...}
;;    - show some or all block comments as /*...*/
;;  - context-sensitive menu bar and popup menus
;;  - code browsing using the `imenu' package
;;  - many customization options
(use-package js2-mode
  :ensure t
  :mode ((rx (or ".js" ".jsx") eos))
  :interpreter "node")

;;; js-comint.el --- JavaScript interpreter in window.
(use-package js-comint :ensure t)

;;; eval-in-repl-javascript.el --- ESS-like eval for javascript
(use-package eval-in-repl-javascript
  :ensure eval-in-repl
  :after js2-mode
  :bind
  (:map js2-mode-map
	([C-return] . eir-eval-in-javascript)))

;;; ps-mode.el --- PostScript mode for GNU Emacs
;; PostScript-mode
(use-package ps-mode
  :disabled t
  :mode ((rx  (or ".ps" ".eps" ".epsf") eos)))

;;; ruby-mode.el --- Major mode for editing Ruby files
(use-package ruby-mode
  :disabled t
  :mode (rx ".rb" eos))

;;; gnuplot.el --- drive gnuplot from within emacs
(use-package gnuplot
  :ensure t
  :mode (((rx  (or ".gp" ".plt") eos) . gnuplot-mode)))

;;; lilypond-mode.el -- Major mode for editing GNU LilyPond music
;;;                     scores
(defvar my-@lilypond@mode@paths nil "Path to `lilypond-mode.el'.")
(eval-and-compile
  (setq my-@lilypond@mode@paths
	(dolist (dir '("/usr/share/emacs/site-lisp"))
	  (let ((path (concat (eval dir) "/lilypond-mode.el")))
	    (when (file-readable-p path)
	      (cl-return dir))))))
(use-package lilypond-mode
  :if my-@lilypond@mode@paths
  :load-path my-@lilypond@mode@paths
  :mode (((rx (or ".ly" ".ily") eos) . LilyPond-mode)))

;;; sql.el --- specialized comint.el for SQL interpreters
(use-package sql
  :mode ((rx (or ".sql" ".tbl") eos) . sql-mode))

;;; sql-indent --- indentation of SQL statements
(use-package sql-indent
  :ensure t
  :after sql
  :hook (sql-mode . sqlind-minor-mode))

;;; xrdb-mode.el --- mode for editing X resource database files
;; This file provides a major mode for editing X resource database
;; files.  It includes font-lock definitions and commands for
;; controlling indentation, re-indenting by subdivisions, and loading
;; and merging into the the resource database.
(use-package xrdb-mode
  :mode (rx (or (: bos (or ".Xdefaults" ".Xenvironment" ".Xresources")) ".ad") eos))

;;; ini-mode.el --- Major mode for Windows-style ini files.
(use-package ini-mode
  :ensure t
  :mode (rx (or ".ini" ".conf") eos))

;;; nastran-mode.el --- Nastran file editing major mode
(use-package nastran-mode
  :quelpa ((nastran-mode :fetcher github :repo "ounim/nastran-mode") :upgrade t)
  :mode ((rx (or ".dat" ".bdf" ".blk") eos) . nastran-mode))

;;; hgignore-mode.el --- a major mode for editing hgignore files
(use-package hgignore-mode
  :ensure t
  :disabled t
  :mode (rx ".hgignore" eos))

;;; sh-script.el --- shell-script editing commands for Emacs
(use-package sh-script
  :mode ((rx ".sh" eos) . sh-mode)
  :custom
  (sh-dynamic-complete-functions
   '(shell-dynamic-complete-environment-variable
     shell-dynamic-complete-command
     comint-dynamic-complete-filename)))

(defun my@insert@sh@head ()
  "Insert yas head for .sh files."
  (my-yas-expand-by-uuid 'sh-mode "sh-head"))
(define-auto-insert (rx ".sh" eos) #'my@insert@sh@head)

;;; eval-in-repl-shell.el --- ESS-like eval for shell
(use-package eval-in-repl-shell
  :ensure eval-in-repl
  :after sh-script
  :preface
  ;; Version with opposite behavior to eir-jump-after-eval configuration
  (defun eir-eval-in-shell2 ()
    "eval-in-repl for shell script (opposite behavior)

This version has the opposite behavior to the eir-jump-after-eval
configuration when invoked to evaluate a line."
    (interactive)
    (let ((eir-jump-after-eval (not eir-jump-after-eval)))
      (eir-eval-in-shell)))
  :bind
  (:map sh-mode-map
	([C-return] . eir-eval-in-shell)
	([C-M-return] . eir-eval-in-shell)))

;;; sed-mode --- Major mode to edit sed scripts
;; If you need this major mode, you might also want to consider
;; spending some time with `M-x doctor'.
(use-package sed-mode
  :ensure t
  :mode ((rx ".sed" eos)))

;;; strace-mode --- strace output syntax highlighting
;; This package provides a major mode with highlighting for output
;; from `strace'.
(use-package strace-mode :ensure t :defer t)

;;; magic-filetype --- Enhance filetype major mode
;; `magic-filetype' parse Vim-style file header.
;; For example, in executable JavaScript(node) file is...

;;   #!/usr/bin/env node
;;   // vim:set ft=javascript:
;;   (function(){
;;       "use strict";
;;        ....
(use-package magic-filetype
  :ensure t
  :functions magic-filetype-enable-vim-filetype
  :config
  (magic-filetype-enable-vim-filetype))

;;; blockdiag-mode --- Major mode for editing blockdiag files
;; Major mode for editing blockdiag files.
;; https://github.com/xcezx/blockdiag-mode
(use-package blockdiag-mode :ensure t :defer t)

;;; po-mode --- major mode for GNU gettext PO files
;; This package provides the tools meant to help editing PO files, as
;; documented in the GNU gettext user's manual. See this manual for
;; user documentation, which is not repeated here.
(use-package po-mode :ensure t :defer t)

;;; ereader --- Major mode for reading ebooks
(use-package ereader
  :ensure t
  :mode ((rx ".epub" eos) . ereader-mode))

;;; open-in-msvs --- Open current file:line:column in Microsoft Visual Studio
;; Opens current file:line:column within active instance of Visual Studio or
;; starts new one.
;; Bind the following command:
;; open-in-msvs
(use-package open-in-msvs
  :ensure t
  :disabled t
  :if my-iswin
  :commands open-in-msvs)

;;; lua-mode.el --- a major-mode for editing Lua scripts
;; lua-mode provides support for editing Lua, including automatical
;; indentation, syntactical font-locking, running interactive shell,
;; interacting with `hs-minor-mode' and online documentation lookup.
(use-package lua-mode :ensure t :defer t)

;;; puppet-mode.el --- Major mode for Puppet manifests
;; GNU Emacs 24 major mode for editing Puppet manifests.
;;
;; Provides syntax highlighting, indentation, alignment, movement, Imenu and
;; code checking.
(use-package puppet-mode :ensure t :defer t
  :disabled t)

(use-package org-src
  :ensure org
  :defer t
  :defines org-src-lang-modes)

;;; graphviz-dot-mode.el --- Mode for the dot-language used by graphviz (att).
(use-package graphviz-dot-mode
  :ensure t
  :after org-src
  :custom
  (setq graphviz-dot-indent-width 2)
  :config
  (add-to-list 'org-src-lang-modes '("dot" . graphviz-dot) t))

;;; ruler-mode.el --- display a ruler in the header line
(use-package ruler-mode :hook fortran-mode)

;;; hexl.el --- edit a file in a hex dump format using the hexl filter
(use-package hexl
  :custom
  (hexl-iso "-iso"))

;;; groovy-mode.el --- Major mode for Groovy source files
(use-package groovy-mode :ensure t :defer t)

;;; tokei --- Display codebase statistics
;; Tokei.el is a major-mode for Emacs, that displays codebase statistics with the help of Tokei.
(use-package tokei :ensure t :defer t)

;;; Colorization of Compilation Buffer

;;; ansi-color.el --- translate ANSI escape sequences into faces
;; This file provides a function that takes a string or a region
;; containing Select Graphic Rendition (SGR) control sequences (formerly
;; known as ANSI escape sequences) and tries to translate these into
;; faces.
(use-package ansi-color
  :hook ((compilation-filter . ansi-color-compilation-filter)))

;;; poke.el --- Emacs meets GNU poke!
;; This file implements an Emacs interface to GNU poke, the extensible
;; editor for structured binary data.
;;
;; It uses the poked (GNU poke daemon) in order to act as a set of
;; pokelets:
;;
;; poke-out
;;      connected to the poked output channel 1.
;; poke-cmd
;;      connected to the poked input channel 2.
;; poke-code
;;      connected to the poked input channel 3.
;; poke-vu
;;      connected to the poked output channel 2.
;; poke-complete
;;      connected to the poked output channel 5.
;; poke-elval
;;      connected to the poked output channel 100.
(use-package poke
  :ensure t
  :commands poke-out poke-cmd poke-code poke-vu poke-complete poke-elval)

;;; poke-mode.el --- Major mode for editing Poke programs  -*- lexical-binding: t; -*-
;; A major mode for editing Poke programs.
(use-package poke-mode
  :ensure t
  :defer t)

(el-init-provide)



;;; init-misc-modes.el ends here
