;;; init-mixal.el --- Settings for working with Donald Knuth's MIX language.

;; Copyright © 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:27 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(use-package mixvm
  :after mixal-mode
  :load-path "~/.local/share/mdk/")

(el-init-provide)



;;; init-mixal.el ends here
