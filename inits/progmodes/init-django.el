;;; init-django.el --- Settings for working with Django

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:42 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; django-manage --- Django minor mode for commanding manage.py
(use-package django-manage
  :ensure t
  :commands django-manage-command)

;;; django-mode --- Major mode for Django web framework.
(use-package django-mode
  :ensure t
  :after helm-make
  :commands django-mode)

(use-package django-html-mode
  :ensure django-mode
  :mode (rx ".djhtml" eos))

;;; django-snippets --- Yasnippets for django
(use-package django-snippets
  :ensure t
  :after (:all yasnippet django-mode)
  :functions django-snippets-initialize
  :config
  (django-snippets-initialize))

;;; django-commands --- Run django commands
(use-package django-commands
  :ensure t
  :defer)

(el-init-provide)



;;; init-django.el ends here
