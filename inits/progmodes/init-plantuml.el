;;; init-plantuml.el --- Settings for editing PlanUML files.

;; Copyright © 2020, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:26 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'el-init)

;;; plantuml-mode.el --- Major mode for PlantUML
(use-package plantuml-mode
  :ensure
  :mode (rx "." (or "plantuml" "pum" "plu") eos)
  :custom
  (plantuml-jar-path "/usr/share/plantuml/plantuml.jar")
  (plantuml-executable-path "/usr/bin/plantuml")
  (plantuml-default-exec-mode 'executable))

(use-package flycheck-plantuml
  :ensure t
  :after flycheck
  :config
  (flycheck-plantuml-setup))

(provide 'init-plantuml)



;;; init-plantuml.el ends here
