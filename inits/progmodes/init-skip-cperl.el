;;; init-cperl.el --- Settings for `cperl-mode'.

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 19:55:28 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(require 'init-misc)
(require 'init-progmodes)
(require 'init-spell)

;; cperl-mode is preferred to perl-mode
;; "Brevity is the soul of wit" <foo at acm.org>
(defalias 'perl-mode 'cperl-mode)

;;; cperl-mode.el --- Perl code editing commands for Emacs
(use-package cperl-mode
  :mode (rx (or ".pl" ".PL" ".pm") eos)
  :interpreter (("perl" . cperl-mode)
                ("perl5" . cperl-mode)
                ("miniperl" . cperl-mode))
  :preface
  (defvar ffap@perl@inc@dirs
    (apply 'append
           (mapcar (function ffap-all-subdirs)
                   (split-string
                    (shell-command-to-string
                     "perl -e 'pop @INC ; print join(q/ /,@INC) ;'")))))
  (defun my@cperl@ffap@locate(name)
    "Return cperl module for ffap"
    (let* ((r (replace-regexp-in-string ":" "/" (file-name-sans-extension name)))
           (e (replace-regexp-in-string "//" "/" r))
           (x (ffap-locate-file e '(".pm" ".pl" ".xs") ffap@perl@inc@dirs)))
      x))
  :bind (
         :map cperl-mode-map
         ("C-h f" . cperl-perldoc))
  :init
  (setq cperl-hairy t)
  (add-to-list 'ffap-alist '(cperl-mode . my@cperl@ffap@locate)))

(el-init-provide)



;;; init-cperl.el ends here
