;;; init-asm-mode.el --- Settings for `asm-mode'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:25 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'init-progmodes)

;;; asm-mode.el --- mode for editing assembler code
(use-package asm-mode
  :mode (rx ".asm" eos))

(el-init-provide)



;;; init-asm-mode.el ends here
