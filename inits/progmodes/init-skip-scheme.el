;;; init-scheme.el --- Settings for `scheme'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:23 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; scheme.el --- Scheme (and DSSSL) editing mode
(use-package scheme
  :mode ("\\.rkt\\'" . scheme-mode))

(el-init-provide)



;;; init-scheme.el ends here
