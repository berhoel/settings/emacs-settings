;;; init-maxima.el --- Settings for maxima access from emacs.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:23 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'init-path)
(require 'init-progmodes)

;;; imaxima.el --- Maxima mode with images
(use-package imaxima
  :if my-isux
  :commands imaxima
  :init
  ;; set imaxima font size
  (setq imaxima-fnt-size "Large")

  ;; Combined use with Maxima mode
  ;; Maxima.el provided in the Maxima source distribution enables you
  ;; more commands than imaxima.el. So, imaxima is designed to work
  ;; with maxima.el.
  ;; As for the preparation, you should put
  (setq imaxima-use-maxima-mode-flag t)
  ;; in .emacs file and restart your emacs.

  ;;; imath.el --- Imath minor mode
  (use-package imath
    :if my-isux
    :commands imath-mode))

;;; maxima.el --- Major modes for writing Maxima code
(use-package maxima
  :if my-isux
  :commands maxima
  :mode ((rx ".max" eos) . maxima-mode))

(el-init-provide)



;;; init-maxima.el ends here
