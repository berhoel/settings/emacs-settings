;;; init-html.el --- Setup for edition HTML files

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 19:55:30 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(require 'init-path)
(require 'init-web)
(require 'init-misc)

;;; html-check-frag --- Check html-fragments
;; Mismatches of html tags are highlighted with the face
;; html-check-frag-error-face. You can go to the next mismatch with
;; html-check-frag-next.
(use-package html-check-frag
  :ensure t
  :hook (;; If you want to start html-check-frag-mode together with html-mode
         ;; then also add:
         ((html-mode web-mode) . html-check-frag-mode)))

;;; css-mode.el --- Major mode to edit CSS files
(use-package css-mode
  ;;; A major mode for editing CSS.
  ;; Adds font locking, some rather primitive indentation handling and
  ;; some typing help.
  ;; To install, put this in your .emacs:
  :mode ("\\.css\\'"))

;;; less-css-mode.el --- Major mode for editing LESS CSS files
;;;                      (lesscss.org)
(use-package less-css-mode
  :ensure t
  :defines less-css-mode-map)

;;; helm-css-scss.el --- CSS/SCSS/LESS Selectors with helm interface
(use-package helm-css-scss
  :ensure t
  :after (helm)
  :defines helm-css-scss-map
  :bind (
         :map less-css-mode-map
         ("s-i" . helm-css-scss)
         ("s-I" . helm-css-scss-back-to-last-point)
         ([C-f8] . prettier-prettify)
         :map isearch-mode-map
         ("s-i" . helm-css-scss-from-isearch)
         :map helm-css-scss-map
         ("s-i" . helm-css-scss-multi-from-helm-css-scss)
         ([C-f8] . prettier-prettify))
  :custom
  ;; Allow comment inserting depth at each end of a brace
  (helm-css-scss-insert-close-comment-depth 2)
  ;; If this value is t, split window appears inside the current window
  (helm-css-scss-split-with-multiple-windows nil)
  ;; Split direction. 'split-window-vertically or 'split-window-horizontally
  (helm-css-scss-split-direction 'split-window-vertically))

;;; css-eldoc --- an eldoc-mode plugin for CSS source code
(use-package css-eldoc
  :ensure t
  :hook (css-mode . css-eldoc-enable))

(el-init-provide)



;;; init-html.el ends here
