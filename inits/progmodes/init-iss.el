;;; init-iss.el --- Settings for `iss-mode'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:29 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

;;; iss-mode.el --- Mode for InnoSetup install scripts
(use-package iss-mode
  :ensure t
  :mode (rx ".iss" eos)
  :defines iss-mode-map
  :bind (
         :map iss-mode-map
         ([f6] . iss-compile)
         ([M-f6] . iss-run-installer))
  :init
  (setq iss-compiler-path "C:/Program Files (x86)/Inno Setup 5/"))


(el-init-provide)



;;; init-iss.el ends here
