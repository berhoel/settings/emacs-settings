;;; init-eldoc.el --- Setup for `eldoc'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:30 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'init-progmodes)

;;; eldoc.el --- show function arglist or variable docstring in echo area
(use-package eldoc
  :hook ((ielm-mode . eldoc-mode)
         (emacs-lisp-mode . eldoc-mode)
         (lisp-interaction-mode . eldoc-mode)
         (eval-expression-minibuffer-setup . eldoc-mode)
         (racer-mode . eldoc-mode)))

;;; eldoc-overlay --- Display eldoc with contextual documentation overlay.
;; Eldoc displays the function signature of the closest function call
;; around point either in the minibuffer or in the modeline.

;; This package modifies Eldoc to display this documentation inline
;; using a buffer text overlay.

;;  `eldoc-overlay-mode' is a per-buffer minor mode.
;;    A call to `eldoc-overlay-enable' turns it on.
;;    A call to `eldoc-overlay-disable' turns it off

;;    {C-x C-h} interactively calls `eldoc-overlay-toggle' and tells
;;    you the mode's new state.

;;  `global-eldoc-overlay-mode' can be used to toggle this for all
;;  buffers.
(use-package eldoc-overlay
  :ensure t
  :after eldoc
  :init
  (eldoc-overlay-mode 1))

;;; eldoc-eval.el --- Enable eldoc support when minibuffer is in use.
(use-package eldoc-eval
  :ensure t
  :init
  (eldoc-in-minibuffer-mode 1))

(el-init-provide)



;;; init-eldoc.el ends here
