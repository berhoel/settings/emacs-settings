;; init-python.el --- Settings for `python' mode

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-19 16:17:26 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(require 'init-my-switches)

(require 'init-progmodes)

(require 'init-autoinsert)
(require 'init-cerbere)
(require 'init-flycheck)
(require 'init-fontlock)
(require 'init-projectile)
(require 'init-theme)
(require 'init-outline)
(require 'init-spell)
(require 'init-whitespace)
(require 'init-yasnippet)
(require 'init-company)

;;; cython-mode.el --- Major mode for editing Cython files
(use-package cython-mode
  :ensure t
  :mode
  (rx (or ".pxi" ".pxd" ".pyx") eos))

;;; flycheck-cython --- Support Cython in flycheck
(use-package flycheck-cython
  :ensure t
  :after cython-mode)

(defun my@insert@py@file@template(&rest args)
  "Insert yas file template for .py files. Ignore ARGS."
  (my-yas-expand-by-uuid 'python-mode "file_template"))
(define-auto-insert (rx ".py" eos) #'my@insert@py@file@template)

;;; python.el --- Python's flying circus support for Emacs
(use-package python
  :mode
  (((rx ".py" eos) . python-mode))
  :interpreter
  (("python" . python-mode))
  :bind
  (:map python-mode-map
        ("C-c C-n" . flycheck-tip-cycle)
        ;; Srefactor --- A refactoring tool based on Semantic parser
        ;;               framework
        ([M-return] . srefactor-refactor-at-point))
  :custom
  (python-shell-prompt-regexp ">>> \\|In \\[[0-9]+\\]: "))

;;; elpy.el --- Emacs Python Development Environment -*- lexical-binding: t -*-
;; The Emacs Lisp Python Environment in Emacs
;;
;; Elpy is an Emacs package to bring powerful Python editing to Emacs.
;; It combines a number of existing Emacs packages, both written in
;; Emacs Lisp as well as Python.
;;
;; For more information, read the Elpy manual:
;;
;; https://elpy.readthedocs.io/en/latest/index.html
(use-package elpy
  :ensure t
  :hook
  ;; Highlight syntax errors (Flycheck)
  ((elpy-mode . flycheck-mode))
  :custom
  ;; Auto formatter used by ‘elpy-format-code’.
  ;;
  ;; if nil, use the first formatter found amongst
  ;; ‘yapf’ , ‘autopep8’ and ‘black’.
  (elpy-formatter 'black)
  ;; Elpy can use a number of modules for additional features, which
  ;; can be inidividually enabled or disabled.
  (elpy-modules
   '(
     ;; Configure some sane defaults for Emacs
     elpy-module-sane-defaults
     ;; Inline code completion (company-mode)
     ;; elpy-module-company ; Use LSP instead of company
     ;; Show function signatures (ElDoc)
     ;; elpy-module-eldoc
     ;; Highlight syntax errors (Flymake)
     ;; elpy-module-flymake ; Use flycheck instead of flymake
     ;; Display indentation markers (highlight-indentation)
     elpy-module-highlight-indentation
     ;; Show the virtualenv in the mode line (pyvenv)
     elpy-module-pyvenv
     ;; Expand code snippets (YASnippet)
     elpy-module-yasnippet
     ;; Django configurations (Elpy-Django)
     elpy-module-django
     ;; Automatically update documentation (Autodoc).
     elpy-module-autodoc
     ;; Code folding
     elpy-module-folding))
  :config
  (elpy-enable))

;;; elpygen --- Generate a Python function/method using a symbol under
;;;             point
(use-package elpygen
  :ensure t
  :bind
  (:map python-mode-map
        ("C-c C-i" . elpygen-implement)))

;;; python-docstring.el --- Smart Python docstring formatting
(use-package python-docstring
  :ensure t
  :preface
  (defun my@python@docstring@mode (&rest args)
    "Ignore ARGS."
    (python-docstring-mode nil))
  :hook
  ((python-mode . my@python@docstring@mode)))

;;; pylint.el --- minor mode for running `pylint'
;; Specialized compile mode for pylint.
(use-package pylint
  :ensure t
  :after python
  :commands pylint-add-key-bindings
  :bind
  (:map python-mode-map
        ([f8] . pylint)
        ([M-C-f8] . pylint-insert-ignore-comment))
  :hook
  ((python-mode . pylint-add-menu-items)))

;;; py-isort.el --- Use isort to sort the imports in a Python buffer
;; Provides the `py-isort' command, which uses the external "isort"
;; tool to tidy up the imports in the current buffer.
;;
;; To automatically sort imports when saving a python file, use the
;; following code:
(use-package py-isort
  :ensure t
  :bind
  (:map python-mode-map
        ;; Uses the "isort" tool to reformat the current buffer.
        ([M-f8] . py-isort-buffer)))
;; :hook
;; ((before-save . py-isort-before-save))

;;; Poetry.el --- Poetry in Emacs.
;; From Poetry documentation: “Poetry is a tool for dependency
;; management and packaging in Python. It allows you to declare the
;; libraries your project depends on and it will manage
;; (install/update) them for you.”
;;
;; Poetry.el is a wrapper around Poetry, offering a simple an
;; intuitive interface in Emacs.
;; config suggested in <https://github.com/galaunay/poetry.el/issues/14>
(use-package poetry
  :ensure t
  :after python
  :hook
  ((python-mode . poetry-track-virtualenv))
  :custom
  (poetry-tracking-strategy 'switch-buffer)
  :config
  (poetry-tracking-mode))


;;; sphinx-doc.el --- Sphinx friendly docstrings for Python functions
;; Sphinx friendly docstring generation for Python code.
;; Inside a Python file, move the cursor to some function/method
;; definition and hit C-c M-d.
(use-package sphinx-doc
  :ensure t
  :hook
  ((python-mode . sphinx-doc-mode)))

;;; eval-in-repl-python.el --- ESS-like eval for python
;; eval-in-repl: Consistent ESS-like eval interface for various REPLs
;;
;; This package does what ESS does for R for various REPLs, including
;; ielm.
;;
;; Emacs Speaks Statistics (ESS) package has a nice function called
;; ess-eval-region-or-line-and-step, which is assigned to C-RET. This
;; function sends a line or a selected region to the corresponding
;; shell (R, Julia, Stata, etc) visibly. It also start up a shell (if )
;; there is none.
;;
;; This package implements similar work flow for various
;; read-eval-print-loops (REPLs) shown below.
;; python support
;; (require 'python) ; if not done elsewhere
(use-package eval-in-repl-python
  :ensure eval-in-repl
  :after python
  :if my-isux
  :bind
  (:map python-mode-map
        ([C-return] . eir-eval-in-python)))

;;; helm-pydoc.el --- pydoc with helm interface
(use-package helm-pydoc
  :ensure t
  :after (helm)
  :bind
  (:map python-mode-map
        ("C-c C-d" . helm-pydoc)))

;;; py-smart-operator.el --- smart-operator for python-mode
;; Python smart-operator mode aims to insert spaces around operators
;; when it is required. It was develop especially for python and
;; requires python-mode.
(use-package py-smart-operator
  :ensure t
  :hook
  ((python-mode . py-smart-operator-mode)))

;;; live-py-mode.el --- Live Coding in Python
(use-package live-py-mode
  :ensure t
  :commands live-py-mode)

;;; doctest-mode.el --- Major mode for editing Python doctest files
(use-package doctest-mode
  :commands doctest-register-mmm-classes
  :mode
  ((rx ".doctest" eos))
  :config
  (doctest-register-mmm-classes t t))

;;; pdb-capf.el --- Completion-at-point function for python debugger
;; `completion-at-point' function that provides completions for current
;; pdb session.
(use-package pdb-capf
  :ensure t
  :init
  (add-hook 'completion-at-point-functions 'pdb-capf nil t))

;;; lsp-pyre.el --- lsp-mode client for python using pyre
;; A simple LSP client for hooking up pyre to lsp-mode
;; (use-package lsp-pyre
;;   :ensure t
;;   :hook ((python-mode . lsp)))

;;; lsp-pyright.el --- Python LSP client using Pyright
;;  Pyright language server.
(use-package lsp-pyright
  :preface
  (defun my@lsp@python@hook (&rest args)
    "Ignore ARGS."
    (require 'lsp-pyright)
    (lsp))  ; or lsp-deferred
  :ensure t
  :hook (python-mode . my@lsp@python@hook))


;;; This repository contains a small collection of advanced Python snippets for the yasnippet package
;; This repository contains a small collection of advanced Python snippets for
;; the yasnippet package.
(use-package py-snippets
  :ensure t
  :after yasnippet
  :config
  (py-snippets-initialize))

(el-init-provide)



;;; init-python.el ends here
