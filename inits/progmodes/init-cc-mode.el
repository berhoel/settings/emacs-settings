;;; init-cc-mode.el --- Auto loaded settings for modes from module cc-mode.

;; Copyright © 2013, 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2024-01-08 19:45:38 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(require 'init-my-switches)
(when my-iswin ; set shell
  (require 'init-windows-base))
(require 'init-abbrev)
(require 'init-autoinsert)
;; (require 'init-company)
(require 'init-flycheck)
(require 'init-outline)
(require 'init-progmodes)
(require 'init-spell)
(require 'init-whitespace)

;;; cc-mode.el --- major mode for editing C and similar languages
(use-package cc-mode
  :mode (((rx (or ".lex" ".y" ".yacc" ".i" ".ic" ".xs" ".c" ".h") eos) . c-mode)
	 ((rx (or ".ii" ".C" ".CC" ".H" ".HH" ".cpp" ".hpp" ".cxx" ".hxx" ".c++"
		  ".h++" ".cc" ".hh") eos) . c++-mode)
	 ((rx ".m" eos) . objc-mode)
	 ((rx ".java" eos) . java-mode)
	 ((rx ".idl" eos) . idl-mode)
	 ((rx (or ".ulpc" ".lpc" ".pike" ".pmod" ".pmod.in") eos) . pike-mode)
	 ((rx ".awk" eos) . awk-mode))
  :preface
  ;; Once upon a time, I worked on xemacs which had a pretty nifty
  ;; feature. In one of the menus for C++, I could get all the
  ;; function names of a class. By clicking on it, I could then go to
  ;; that function directly....I do not have this feature in my
  ;; current emacs installation...
  ;;
  ;; M-x imenu RET lets you choose a function (with completion). You
  ;; can add a menu to the menu bar with M-x imenu-add-to-menubar RET.
  ;;
  ;; You can automate this with
  (defun my@c++@mode@hook (&rest args)
    "Ignore ARGS."
    (imenu-add-to-menubar "Imenu"))
  ;; Sometimes it is useful for files to supply local values for this variable.
  ;; You might also use mode hooks to specify it in certain modes, like this:
  (defun my@c-mode@compile-command-hook  (&rest args)
      (unless (or (file-exists-p "makefile")
		  (file-exists-p "Makefile"))
        (setq-local compile-command
		    (concat "make -k "
			    (if buffer-file-name
				(shell-quote-argument
				 (file-name-sans-extension buffer-file-name)))))))
  :hook ((c-mode . my@c-mode@compile-command-hook)
	 (c++-mode . my@c++@mode@hook))
  :bind (
	 :map c-mode-base-map
	 ;; Srefactor --- A refactoring tool based on Semantic parser
	 ;;               framework
	 ("C-c C-n" . flycheck-tip-cycle)
	 ;; ([M-return] . srefactor-refactor-at-point)
	 ;; ;; Setup cedet for cc-mode
	 ;; ([C-return] . semantic-ia-complete-symbol)
	 ;; ("C-c ?" . semantic-ia-complete-symbol-menu)
	 ;; ("C-c >" . semantic-complete-analyze-inline)
	 ;; ("C-c p" . semantic-analyze-proto-impl-toggle)
	 )
  :defines c-mode-base-map c-mode-map c++-mode-map
  :config
  (setq c-block-comment-prefix ""))

;; ;;; from the cc-mode info file
;; ;; Here's a sample .emacs file that might help you along the way.  Just
;; ;; copy this region and paste it into your .emacs file.  You may want to
;; ;; change some of the actual values.

;; (defconst my-c-style
;;   '((c-tab-always-indent        . t)
;;     (c-comment-only-line-offset . 4)
;;     (c-hanging-braces-alist     . ((substatement-open after)
;;                                    (brace-list-open)))
;;     (c-hanging-colons-alist     . ((member-init-intro before)
;;                                    (inher-intro)
;;                                    (case-label after)
;;                                    (label after)
;;                                    (access-label after)))
;;     (c-cleanup-list             . (scope-operator
;;                                    empty-defun-braces
;;                                    defun-close-semi))
;;     (c-offsets-alist            . ((arglist-close . c-lineup-armyist)
;;                                    (substatement-open . 0)
;;                                    (case-label        . 4)
;;                                    (block-open        . 0)
;;                                    (knr-argdecl-intro . -)))
;;     (c-echo-syntactic-information-p . t)
;;     )
;;   "My C Programming Style")

;; ;; offset customizations not in my-c-style
;; (setq c-offsets-alist '((member-init-intro . ++)))

;;; cc-vars.el --- user customization variables for CC Mode
(use-package cc-vars
  :after cc-mode
  :custom
  (c-default-style
   '((c-mode . "user")
     (c++-mode . "user")
     (java-mode . "java")
     (awk-mode . "awk")
     (other . "gnu")))
  (c-echo-syntactic-information-p t))

;;; cc-styles.el --- support for styles in CC Mode
(use-package cc-styles
  :functions c-add-style
  :config
  (c-add-style
   "user"
   '("ellemtel"
     (c-basic-offset . 2)
     (c-offsets-alist
      (access-label . 0)
      (arglist-intro . ++)
      (block-close . 0)
      (brace-entry-open . 0)
      (brace-list-close . 0)
      (brace-list-entry . 0)
      (brace-list-intro . ++)
      (case-label . 0)
      (class-close . 0)
      (cpp-define-intro . +)
      (cpp-macro-cont . ++)
      (defun-block-intro . +)
      (defun-close . 0)
      (inclass . +)
      (inline-close . 0)
      (innamespace . 0)
      (member-init-cont . -)
      (member-init-intro . 3)
      (namespace-close . 0)
      (statement . 0)
      (statement-block-intro . +)
      (statement-case-intro . +)
      (statement-cont . ++)
      (substatement . +)
      (topmost-intro . 0)
      (topmost-intro-cont . 0)
      (arglist-close . c-lineup-close-paren)
      (arglist-cont-nonempty . c-lineup-arglist)
      (c . c-lineup-C-comments)
      (comment-intro . c-lineup-comment)
      (cpp-macro . -1000)
      (inher-cont . c-lineup-multi-inher)
      (string . -1000))))
  (c-add-style
   "kumkeo" '("ellemtel")))

;; ;;; company-c-headers.el --- Company mode backend for C/C++ header files
;; ;; IMPORTANT: If you want to complete C++ header files, you have to
;; ;; add its paths since by default company-c-headers only includes
;; ;; these two system include paths: /usr/include/ and
;; ;; /usr/local/include/. To enable C++ header completion for standard
;; ;; libraries, you have to add its path.
;; (use-package company-c-headers
;;   :ensure t
;;   :disabled
;;   :after (:all cc-mode company)
;;   :functions company-c-headers
;;   :config
;;   (let ((cxx-base-dir "/usr/include/c++"))
;;     (when (file-exists-p cxx-base-dir)
;;       (let ((last-cpp-ver
;; 	     (last
;; 	      (directory-files cxx-base-dir nil "[0-9]+\\(\\.[0-9]+\\)+"))))
;; 	(add-to-list 'company-c-headers-path-system
;; 		     (concat (file-name-as-directory cxx-base-dir)
;; 			     (car last-cpp-ver))))))
;;   ;; To initialize it, just add it to `company-backends':
;;   (add-to-list 'company-backends 'company-c-headers))

;;; demangle-mode.el --- Automatically demangle C++ symbols
(use-package demangle-mode
  :ensure t
  :if my-isux
  :hook c-mode-common)

;;; never-comment.el --- Never blocks are comment
;; Often C programmers type blocks of code between =#if 0= and
;; =#endif=. This code will never be compiler. Therefore some IDE's
;; display this code as comment. With this module, emacs will also
;; do this.
(use-package never-comment
  :ensure t
  :after cc-mode)

;;; function-args.el --- C++ completion for GNU Emacs
;; GNU Emacs package for showing an inline arguments hint for the
;; C/C++ function at point
(use-package function-args
  :ensure t
  :disabled t
  :after cc-mode
  :functions fa-config-default
  :config
  (let ((map function-args-mode-map))
    ;; (define-key map (kbd "M-o") nil)
    (define-key map (kbd "C-2") nil)
    ;; (define-key map (kbd "M-n") nil)
    ;; (define-key map (kbd "M-h") nil)
    (define-key map (kbd "M-u") nil)
    ;; (define-key map (kbd "M-j") nil)
    ;; (define-key map (kbd "C-M-j") nil)
    ;; (define-key map (kbd "C-M-k") nil)
    )
  (fa-config-default))

;;; msvc --- Microsoft Visual C/C++ mode
(use-package msvc
  :ensure t
  :if (boundp 'w32-pipe-read-delay)
  :commands msvc-mode-on msvc-initialize
  :hook (c-mode-common . msvc-mode-on)
  :config
  (setq w32-pipe-read-delay 0))

;;; msvc-flags.el --- MSVC's CFLAGS extractor and database
(use-package msvc-flags
  :if (boundp 'w32-pipe-read-delay)
  :after msvc
  :functions msvc-flags-load-db
  :config
  (msvc-flags-load-db :parsing-buffer-delete-p t))

;;; cff --- Search of the C/C++ file header by the source and vice versa
;; This is a replacement for the ff-find-other-file. If the helm is
;; loaded, uses it to provide possible multiple choices; otherwise
;; just picks the first one.
(use-package cff
  :ensure t
  :after (:all helm cc-mode)
  :bind (
	 :map c-mode-base-map
	 ("M-o" . cff-find-other-file)))

;;; xcscope.el --- cscope interface for (X)Emacs
;; This is a cscope interface for (X)Emacs.
;; It currently runs under Unix only.
(use-package xcscope
  :ensure t
  :disabled t
  :after cc-mode
  :functions cscope-setup
  :config
  (cscope-setup))

;;; helm-cscope.el --- Helm interface for xcscope.el.
(use-package helm-cscope
  :ensure t
  :disabled t
  :after (helm)
  :hook (c-mode-common . helm-cscope-mode)
  :bind (:map helm-cscope-mode-map
	      ("M-." . helm-cscope-find-global-definition)
	      ("M-@" . helm-cscope-find-calling-this-function)
	      ("M-s" . helm-cscope-find-this-symbol)
	      ("M-," . helm-cscope-pop-mark)))

;;; modern-cpp-font-lock --- Font-locking for "Modern C++"
;; Syntax coloring support for "Modern C++" - until C++17 and TS
;; (Technical Specification). It is recommended to use it in addition
;; with the `c++-mode` major-mode.
;;
;; `modern-c++-font-lock-mode` will be activated for buffers using the
;; `c++-mode` major-mode.
;;
;; For the current buffer, the minor-mode can be turned on/off via the
;; command:
;;    M-x modern-c++-font-lock-mode [RET]
(use-package modern-cpp-font-lock
  :ensure t
  :hook (c++-mode . modern-c++-font-lock-mode))

;;; c-eldoc --- helpful description of the arguments to C functions
(use-package c-eldoc
  :ensure t
  :hook ((c-mode c++-mode) . c-turn-on-eldoc-mode))

;;; qt-pro-mode --- Qt Pro/Pri major mode
;; Provides a major-mode for editing Qt build-system files.
(use-package qt-pro-mode
  :ensure t
  :mode ("\\.pro\\'" "\\.pri\\'"))

;;; clang-capf.el --- Completion-at-point backend for c/c++ using clang
;; clang-capf.el (formerly cpp-capf.el)
;;
;; This package provides a completion-at-point function to complete
;; C/C++ code using clang, offering context-base suggestions for
;; functions, variables and types without having to save the buffer.
;; While clang-capf doesn't require anything to be installed besides
;; clang as an external component, a better looking completion
;; front-end in Emacs, such as ivy might be worth recommending.
(setq clang-capf-clang "clang")
(use-package clang-capf
  :after cc-mode
  :preface
  (defun my@clang@capf@hook@fun (&rest args)
    "Add `clang-capf' to `completion-at-point-functions'.
Ignore ARGS."
    (add-hook 'completion-at-point-functions #'clang-capf nil t))
  ;; :init
  ;; (setq clang-capf-clang "clang")
  :hook (((c-mode c++-mode) . my@clang@capf@hook@fun)))

;; autoinsert settings
(defun my@insert@c@head (&rest args)
  "Insert yas head for .c files. Ignore ARGS."
  (my-yas-expand-by-uuid 'c-mode "C new"))
(define-auto-insert (rx ".c" eos) #'my@insert@c@head)

(defun my@insert@h@head (&rest args)
  "Insert yas head for .c files. Ignore ARGS."
  (my-yas-expand-by-uuid 'c-mode "h new"))
(define-auto-insert  (rx ".h" eos) #'my@insert@h@head)

(defun my@insert@cc@head (&rest args)
  "Insert yas head for .c++ files. Ignore ARGS."
  (my-yas-expand-by-uuid 'c++-mode "C++ new"))
(define-auto-insert (rx (or ".C" ".CC" ".cc" ".cpp" ".cxx" ".c++") eos)
  #'my@insert@cc@head)

(defun my@insert@hh@head (&rest args)
  "Insert yas head for .c++ files. Ignore ARGS."
  (my-yas-expand-by-uuid 'c++-mode "C++ h new"))
(define-auto-insert (rx (or ".H" ".HH" ".hh" ".hpp" ".hxx" ".h++") eos)
  #'my@insert@hh@head)

(el-init-provide)



;;; init-cc-mode.el ends here
