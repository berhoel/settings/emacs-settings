;;; init-rust.el --- Setup for editing rust files.

;; Copyright © 2018, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 19:55:28 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(require 'init-progmodes)
(require 'init-yasnippet)

(require 'init-autoinsert)

;;; rust-mode.el --- A major emacs mode for editing Rust source code
(use-package rust-mode
  :ensure t
  :mode (rx ".rs" eos)
  :bind
  (:map rust-mode-map ( "C-c C-c" . rust-run))

  :config
  ;; Formatting with rustfmt
  ;; The rust-format-buffer function will format your code with
  ;; rustfmt if installed. By default, this is bound to C-c C-f.
  (setq rust-format-on-save t))

;;; cargo.el --- Emacs Minor Mode for Cargo, Rust's Package Manager.
(use-package cargo
  :ensure t
  :hook (rust-mode . cargo-minor-mode))

;;; racer --- code completion, goto-definition and docs browsing for
;;;           Rust via racer
(use-package racer
  :ensure t
  :hook (rust-mode . racer-mode))

;;; rust-auto-use --- Utility to automatically insert Rust use statements
(use-package rust-auto-use
  :ensure t
  :defines rust-auto-use)

;;; flycheck-rust --- Flycheck: Rust additions and Cargo support
(use-package flycheck-rust
  :ensure t
  :hook (rust-mode . flycheck-rust-setup))

(el-init-provide)



;;; init-rust.el ends here
