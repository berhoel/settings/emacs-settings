<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#orgheadline1">1. My personal emacs settings</a></li>
</ul>
</div>
</div>

# My personal emacs settings<a id="orgheadline1"></a>

These are my quite elaborate emacs settings for all kinds of file
types. The settings make use of `el-init` and `use-package` to
organize things.

The whole setup is still quite messy and is changed all the time to
add new features, remove old ones, and further clean the settings.
