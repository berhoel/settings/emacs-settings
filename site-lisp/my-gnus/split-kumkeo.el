;;; package --- Define settings for gnus splitting of incoming mails,

;;; Copyright (C) 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Task      gnus split setting for pc130799

;; Code:

(provide 'my-gnus/split-kumkeo)

;;; split-kumkeo.el ends here



;; Local Variables:
;; mode: emacs-lisp
;; coding: utf-8
;; End:
