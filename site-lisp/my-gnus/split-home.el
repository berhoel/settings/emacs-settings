;;; package --- Define settings for gnus splitting of incoming mails,

;; Copyright (C) 2013, 2016 - 2022 by Berthold Höllmann

;; Author    Berthold Höllmann <berhoel@gmail.com>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'use-package))

(use-package nnml)
(use-package spam)
(use-package rx)

;; Whether the nnmail splitting functionality should MIME decode headers.
(setq nnmail-mail-splitting-decodes t)

(setq
 nnmail-split-fancy
 `(|
   (: nnmail-split-fancy-with-parent)
   ("subject" ,(rx (* nonl) "[despammed]" (* nonl)) "spam.despammed")
   (to ,(rx "berthold-ebay@xn--hllmanns-n4a.de")
       (|
        ("subject"
         ,(rx (or
               (: (+ nonl) (+ num) "_" (| "N" "n") "eu" (? "e") ":_" (+ nonl))
               (: (+ num) blank (| "N" "n") "eu" (? "e") ":" blank (+ nonl))
               (: " " (+ nonl) ": " (+ num) blank "NEU" (? "E") "!" eol)
               "Neue Artikel, die zu Ihrer Suche passen"
               (: "Neue" (| " " "_") "Artikel" (| " " "_") "passend" (| " " "_") "zu:" (+ nonl))
               (: (+ nonl) "neue Ergebnisse heute!")
               (: (+ nonl) "neues Ergebnis heute!")))
         "eBay.Angebote")
        ("subject" "Neue_Angebote_Ihrer_bevorzugten" "eBay.Bevorzugt")
        (from ,(rx (or
                    "frida@news.ebay-kleinanzeigen.de"
                    "noreply@ebay-kleinanzeigen.de"
                    "info@dialog.ebay-kleinanzeigen.de"))
              "eBay.Kleinanzeigen")
        "eBay"))
   (from ,(rx "tns@thenewstack.io" "TheNewStack"))
   (from ,(rx (: (+ nonl) "@ebay." (or "de" "com"))) "eBay")
   (to ,(rx "berthold-thalia@xn--hllmanns-n4a.de") "Thalia")
   (to ,(rx "berthold-amazon@xn--hllmanns-n4a.de") "Amazon")
   (to ,(rx "berthold-buchhandel-de@xn--hllmanns-n4a.de") "Buchhandel.de")
   (to ,(rx "berthold-dictum@xn--hllmanns-n4a.de") "Dictum")
   (to ,(rx "berthold-heise@xn--hllmanns-n4a.de") "Heise")
   (to ,(rx "berthold-atomic@xn--hllmanns-n4a.de") "Atomic")
   (to ,(rx "berthold-booklooker@xn--hllmanns-n4a.de") "booklooker")
   (to ,(rx "berthold-discogs@xn--hllmanns-n4a.de") "Discogs")
   (to ,(rx "berthold-firefox@xn--hllmanns-n4a.de") "Firefox")
   (to ,(rx "berthold-framagit@xn--hllmanns-n4a.de") "Framagit")
   (to ,(rx "berthold-freenode@xn--hllmanns-n4a.de") "freenode")
   (to ,(rx "berthold-gitlab@xn--hllmanns-n4a.de") "GitLab")
   (to ,(rx "berthold-gocomics@xn--hllmanns-n4a.de") "GoComics")
   (to ,(rx "berthold-haspa@xn--hllmanns-n4a.de") "HASPA")
   (to ,(rx "berthold-imdb@xn--hllmanns-n4a.de") "IMDB")
   (to ,(rx "berthold-microsoft@xn--hllmanns-n4a.de") "MicroSoft")
   (to ,(rx "berthold-neckermann@xn--hllmanns-n4a.de") "Neckermann")
   (to ,(rx "berthold-netflix@xn--hllmanns-n4a.de") "Netflix")
   (to ,(rx "berthold-raindop@xn--hllmanns-n4a.de") "raindrop")
   (to ,(rx "berthold-rebuy@xn--hllmanns-n4a.de") "reBuy")
   (to ,(rx "berthold-sat1@xn--hllmanns-n4a.de") "Pro7SAT1")
   (to ,(rx "berthold-shop4@xn--hllmanns-n4a.de") "Shop4")
   (to ,(rx "camelcamelcamel@xn--hllmanns-n4a.de") "camelcamelcamel")
   (to ,(rx "disney+@xn--hllmanns-n4a.de") "Disney+")
   (to ,(rx "keepass@xn--hllmanns-n4a.de") "admin.keepass")
   (from ,(rx "ct-register@ct.de") "Heise-Register")
   ("subject" ,(rx (: "Heise-Register-Updates - c") (+ nonl)) "Heise-Register")
   (from ,(rx "ct-club@newsletter.heise.de") "Heise.ct-Club")
   (from ,(rx "event-mailings@infoservice.heise.de") "Heise.Events")
   (from ,(rx "newsletter@newsletter.heise.de") "Heise.TechStage")
   (to ,(rx "berthold-saturn@xn--hllmanns-n4a.de") "Saturn")
   (to ,(rx (or "berhoel+mediamarkt@gmail.com" "berthold-mediamarkt@xn--hllmanns-n4a.de")) "MediaMarkt")
   ("Reply-To" ,(rx "rekall-announce@rekallrevealed.org") "Rekall.announce")
   ("List-Id" ,(rx "pgsql-announce.postgresql.org") "postgreSQL.announce")
   (to ,(rx "pgsql-announce@lists.postgresql.org") "postgreSQL.announce")
   (from ,(rx "netdata@xn--hllmanns-n4a.de") "netdata")
   (from ,(rx "hello@blynk.cc") "Blynk")
   (from ,(rx (or "info@e-mail.xing.com"
                  "news@xing.com"
                  "news@mail.xing.com"
                  "mailrobot@mail.xing.com"
                  "news@e-mail.xing.com")) "XING")
   (from ,(rx (or "linkedin@e.linkedin.com"
                  "notifications-noreply@linkedin.com"
                  "security-noreply@linkedin.com"
                  "messaging-digest-noreply@linkedin.com"
                  "messages-noreply@linkedin.com")) "LinkedIn")
   (from ,(rx "invitations@linkedin.com") "LinkedIn.invitations")
   (from ,(rx "jobalerts-noreply@linkedin.com") "LinkedIn.jobalerts")
   (from ,(rx "news@newsletter.conrad.de") "Conrad")
   (from ,(rx (or "service@paypal.de" "announcements-account@paypal.com")) "PayPal")
   (from ,(rx "newsletter@mysql.com") "mySQL.Newsletter")
   (from ,(rx "newsletter@gentlewarenews.com") "software.PoseidonUML")
   (from ,(rx "info@dooyoo.com") "dooyoo")
   (from ,(rx (or"info@abebooks.de" "newsletter@abebooks.de")) "AbeBooks")
   (from ,(rx "errors@counter.li.org") "Linux-Counter")
   (from ,(rx "quadmail@mailbox.cps.intel.com") "software.INTEL")
   (from ,(rx "SoftwareTools@plan.intel.com") "software.INTEL")
   (from ,(rx "info@isource.ibm.com") "software.IBM.DB2")
   (from ,(rx "noreply@getpocket.com") "Pocket")
   (from ,(rx "noreply@discogs.com") "Discogs")
   (from ,(rx "newsletter@news.tchibo.de") "Tchibo")
   (from ,(rx "newsletter-de@hornbach.com") "HORNBACH")
   ("subject" "Downloadstand Berthold" "MRH.Netzzugang.Downloadstand")
   (from ,(rx "wordpress@mrh-netz.de")
         (|
          ("subject" ,(rx "Sucuri Alert, www.mrh-netz.de, ") "MRH.Web.WordPress.Sucuri")
          "MRH.Web.WordPress"))
   (from ,(rx "fragenanznad@wohnheime.studentenwerk-hamburg.de") "MRH.Netzzugang")
   (to ,(rx (or
             "protektoren@studierendenwerk-hamburg.de"
             "protektor-mrh@wohnheime.studierendenwerk-hamburg.de"
             "protektor-psh@wohnheime.studierendenwerk-hamburg.de"))
       "MRH.Protektoren")
   (from ,(rx "Gmane Autoauthorizer <auth" (? "-") (+ nonl) "@" (? "auth.") "gmane.org") "Gmane.Autoauthorizer")
   ("subject" ,(rx "SMART error (" (* nonl) ") detected on host: " (group (+ nonl)))
    ,(rx "Admin.SMART." (backref 1)))
   ("subject" ,(rx (or (: "Cron" (+ nonl)) "pchoel Daily Usenet report")) "Admin")
   (to ,(rx (or "root@localhost" "root@xn--hllmanns-n4a")) "Admin")
   ("subject" ,(rx "SuSEconfig") "SuSE")
   ("subject" ,(rx "Local " (+ word) " Security for pc" (+ nonl) ": C" (+ word)) "Admin.Security")
   (from ,(rx (or "neu@mailings.web.de"
                  "keineantwortadresse@web.de"
                  "noreply@web.de"
                  "millionenklick@web.de"
                  "noreply@millionenklick.web.de"))
         (|
          ("subject" "Tippbestätigung [[:digit:]]\\{4,\\}" "web.de.millionenklick")
          ("subject" "Tippbest.+" "web.de.millionenklick")
          ("subject" "Millionenklick Ziehungsergebnisse" "web.de.millionenklick.ZI")
          "web.de"))
   ("subject" ,(rx "JazzEcho Newsletter vom " (= 2 digit) "|" (= 2 digit) "|" (= 2 digit)) "JazzEcho")
   ("subject" ,(rx (+ nonl) "memberships" (or " reminder" "_re")) "mail.admin")
   ("subject" ,(rx "Rejected freedb submission") "freedb")
   (from ,(rx (or
               "mailman-owner@wohnheime.studierendenwerk-hamburg.de"
               "mailman-owner@wohnheime.studentenwerk-hamburg.de"
               "mailman-owner@lists.hamburg.gruene.de"))
         "mail.admin")
   ("subject" ,(rx "jpc Newsletter:" (+ nonl)) "jpc")
   ("subject" ,(rx "Logwatch for " (group (+ nonl)) "(Linux") ,(rx "Logwatch." (backref 1)))
   (to ,(rx "MRH@wohnheime.studierendenwerk-hamburg.de") "MRH")
   (to ,(rx "alumni-request@" (or "mrh" "psh") "-netz.de") "MRH.Alumni")
   ("List-Id" ,(rx "python-announce-list.python.org") "python.announce")
   ("List-Id" ,(rx "pypapers.substack.com") "PythonPapers")
   ("List-Id" ,(rx "bambooweekly.substack.com") "BambooWeekly")
   (from ,(rx "cs@importpython.com>") "ImportPython")
   ("subject" ,(rx "hamburg-pythoneers - Google Groups: Ausstehende Nachricht") "python.pythoneers.hh.admin.pending")
   (to ,(rx "devpi-dev@python.org") "python.devpi.dev")
   (from ,(rx "info@realpython.com") "python.RealPython")
   (from ,(rx "globenews@" (? "no-") "reply.globetrotter.de") "Globetrotter")
   ("subject" ,(rx "FRITZ!Box-Info: Nutzungs") "FRITZ!Box.Info")
   (from ,(rx (or "\"FRITZ!Box 7490\" <bhoel@web.de>"
                  "\"FRITZ!Box 3490\" <bhoel@web.de>"
                  "noreply@myfritz.net"))
         "O2.FRITZ!Box")
   (from ,(rx "reply@oreilly.com" "Community@make.co") "OReilly")
   (from ,(rx (or "make@email.makermedia.com" "Community@make.co")) "OReilly.MAKE")
   (from ,(rx (or "reply@news.merkheft.de" "merkmail@merkheft.eu" "newsletter@reply.zweitausendeins.de")) "zweitausendeins")
   (from ,(rx "info@linux-magazine.com") "LinuxProMagazine")
   (from ,(rx (or "service@vth-newsletter.de"
                  "newsletter@vth.de"
                  "service@vth.de"))
         "VTH")
   (from ,(rx "momox@momox.de") "momox")
   (from ,(rx "do-not-reply@stackoverflow.email") "StackOverflow")
   ;; yocto mailinglists
   (from ,(rx "yocto-bounces@yoctoproject.org") "yocto")
   (to ,(rx (or "yocto@yoctoproject.org" "yocto@lists.yoctoproject.org")) "yocto")
   (from ,(rx "poky-bounces@yoctoproject.org") "yocto.poky")
   (to ,(rx "poky@lists.yoctoproject.org") "yocto.poky")
   (to ,(rx "berthold-yocto@xn--hllmanns-n4a.de") "yocto.suspicious")
   (from ,(rx (or "kundenservice@congstar.de" "noreply@news.congstar.de")) "congstar")
   (from ,(rx "info@maritimes-cluster.de") "MaritimesClusterNorddeutschland")
   (to ,(rx "berthold-aerofred@xn--hllmanns-n4a") "AeroFred")
   (from ,(rx "reuven@lerner.co.il") "Lerner")
   (from ,(rx (or "noreply@adafruit.com" "newsletter@adafruitdaily.com")) "Adafruit")
   (from ,(rx (or "buero.kellner@newsletter.gruene.de"
                  "presse@hamburg.gruene.de"
                  "kreisverband@gruene-eimsbuettel.de"
                  "umfrage@info.gruene.de"))
         "GAL")
   (to ,(rx "lv-greennewsletter@lists.hamburg.gruene.de") "GAL")
   (from ,(rx "admin@pycoders.com") "python.PyCodersWeekly")
   (from ,(rx "rahul@pythonweekly.com") "python.weekly")
   (from ,(rx "noreply@medium.com") "Medium")
   (from ,(rx (or "noreply@dhl.de" "noreply.packstation@dhl.de")) "DHL.Packstation")
   (to ,(rx "matplotlib-announce@python.org") "python.Matplotlib.announce")
   (to ,(rx (or
             "berthold-tumbleweed@xn--hllmanns-n4a.de"
             "opensuse-factory@opensuse.org"
             "factory@lists.opensuse.org"))
       "SuSE.Factory")
   (from ,(rx "noreply@github.com") "GitHub")
   (to ,(rx (any " <") (group (+ nonl)) "@noreply.github.com") ,(rx "GitHub." (backref 1)))
   (from ,(rx "noreply@mycloud.com") "MyCloud")
   (from ,(rx "designspark@comms.rs-online.com") "DesignSpark")
   (from ,(rx (or "updates@academia-mail.com"
                  "premium@academia-mail.com"))
         "Academia-Mail")
   (from ,(rx "ikea@hej.news.email.ikea.de") "IKEA")
   (from ,(rx "info@az-delivery.com") "AZ-Delivery")
   (from ,(rx "digiabo@taz.de") "taz.digiabo")
   (from ,(rx "info@meetup.com") "Meetup")
   (from ,(rx "newsletter@reichelt.com") "Reichelt.Newsletter")
   (from ,(rx "info@newsletter.bueromarkt-ag.de") "BueromarktBoettcher")
   (from ,(rx "info@mailer.netflix.com") "Netflix")
   (from ,(rx "info@admin-magazine.com") "LinuxProMagazine")
   (to ,(rx "info-gnu@gnu.org") "info.gnu")
   (to ,(rx "gcc-announce@gcc.gnu.org") "software.gnu.gcc")
   (to ,(rx "info-gnu-emacs@gnu.org") "info.gnu.emacs")
   (from ,(rx "info@fsf.org") "info.fsf")
   (to ,(rx "dante-ev@dante.de") "LaTeX.DANTE")
   (from ,(rx "no-reply@news.panasonic.eu") "Panasonic")
   (from ,(rx "noreply@kununu.com") "kununu")
   (from ,(rx "newsletter@kununu.com") "kununu.newsletter")
   (from ,(rx "newsletter@softmaker.com") "SoftMaker")
   (from ,(rx "no-reply@m1.email.samsung.com") "hardware.SAMSUNG")
   (from ,(rx "microcenter@microcenterinsider.com") "MicroCenter")
   (from ,(rx "team@hired.com") "Jobsuche.Hired")
   (from ,(rx "job-alert@hays.de") "Jobsuche.Hays")
   (from ,(rx "redaktion@news.ingenieur.de") "Jobsuche.ingenieur_de")
   (from ,(rx "do-not-reply@imdb.com") "IMDB")
   (to ,(rx "bhoel@gmx.de")
       (|
        (from ,(rx "noreply_lgs@uim.de")  "GMX.Gewinnspiel")
        "GMX"))
   (from ,(rx (or "mailings@sicher.gmx.net"
                  "mailings@mailings.gmx.net"))
         (| (subject ,(rx (* nonl) "Ihr_Gl" (+ nonl) "ckscode_lautet:" (* nonl)) "GMX.Gewinnspiel")
            "GMX"))
   (from ,(rx (or "rezepte@mailing.rewe.de" "newsletter@mailing.rewe.de")) "REWE")
   (from ,(rx "service@news.moia.io") "MOIA")
   (from ,(rx "akademie@zeitakademie.zeit.de") "Zeit.Akademie")
   (from ,(rx (: (+ nonl) "@matched.io")) "Jobsuche.matched.io")
   (from ,(rx (: (+ nonl) "@workwise.io")) "Jobsuche.workwise.io")
   (from ,(rx "JobNews@vesterling.com") "Jobsuche.Vesterling")
   (from ,(rx "service@stayfriends.de") "StayFriends")
   (from ,(rx "noreply-maps-timeline@google.com") "Google.Maps")
   (from ,(rx "googledev-noreply@google.com") "Google.Developer")
   (from ,(rx "no-reply@rechnung.o2online.de") "O2.Rechnung")
   (from ,(rx "website@shmf.de") "Schleswig-Holstein-Musik-Festival")
   (from ,(rx "noreply@ec.consorsfinanz.de") "ConsorsFinanz")
   (from ,(rx "tns@thenewstack.io") "TheNewStack")
   (from ,(rx "noreply@youtube.com") "YouTube")
   (from ,(rx "Germany@email.DaenischesBettenlager.de") "DaenischesBettenlager")
   (from ,(rx "mail@comms.canon-europe.com") "hardware.Canon")
   (from ,(rx "news@tonermacher.de") "tonermacher")
   (from ,(rx "portal@elster.de") "Elster")
   (to ,(rx "r-announce@r-project.org") "R")
   (from ,(rx "newsletter@kiekeberg-museum.de") "FreilichtmuseumKiekeberg")
   (from ,(rx "noreply@haveibeenpwned.com") "HaveIBeenPwned")
   (from ,(rx "noreply@notifications.simplr.de") "Versicherungen.simplr")
   (from ,(rx (or "recommendations@" (or "discover" "inspire") ".pinterest.com")) "Pinterest")
   (from ,(rx "abfuhrkalender@stadtreinigung.hamburg") "Abfuhrkalender")
   (: gnus-group-split-fancy nil 1)
   ("X-Spam-Flag" "YES" "spam")
   ;; (: spam-split 'spam-use-regex-headers)
   (: spam-split)
   ;; default mailbox
   "mail.misc"))

(provide 'my-gnus/split-home)
;;; split-home.el ends here



;; Local Variables:
;; mode: emacs-lisp
;; coding: utf-8
;; End:
