;;; system-home.el --- Define system settings for `gnus' especially
;;;                    for my home setup.

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author    Berthold Höllmann <berhoel@gmail.com>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'gnus)
  (require 'gnus-msg)
  (require 'epg)
  (require 'nnmail)
  (require 'nnfolder)
  (require 'smime))

(eval-when-compile (require 'use-package))

(setq
 gnus-parameters
 '(("^nnml:.*"
    (gcc-self . t)
    (gnus-use-scoring nil))

   ("^gmane\\..*"
    (spam-contents gnus-group-spam-classification-ham)
    (spam-process ((spam spam-use-gmane)
                   (spam spam-use-bogofilter)
                   (ham spam-use-bogofilter))))

   ("^\\<\\(gnu\\|gnus\\|de\\|comp\\|alt\\|ger\\|linux\\|misc\\|pgsql\\|public\\|rec\\|schule\\|sci\\|spline\\|uk\\)\\..*\\>"
    (gnus-article-sort-functions '(gnus-article-sort-by-number))
    (gnus-thread-sort-functions '(gnus-thread-sort-by-number))
    (spam-process ((spam spam-use-bogofilter)
                   (ham spam-use-bogofilter))))

   ("^nnml:spam.*"
    (gnus-article-sort-functions '(gnus-article-sort-by-subject))
    (gnus-thread-sort-functions '(gnus-thread-sort-by-subject))
    (spam-contents gnus-group-spam-classification-spam)
    (spam-process ((spam spam-use-bogofilter)))
    (gnus-use-scoring nil))))
;;      ("^.*"
;;       (spam-contents gnus-group-spam-classification-ham)
;;       (spam-process
;;        ((spam spam-use-bogofilter)
;;         (ham spam-use-bogofilter))))))

(setq nnfolder-directory "~/Mail/nnfolder")

(setq gnus-select-method
      '(nntp "localhost"))

(setq gnus-secondary-select-methods
      '((nnml ""
              (nnmail-split-methods nnmail-split-fancy))))

;; EasyPg benutzen (ab Gnus 5.10)
(setq epg-user-id "bhoel@web.de")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Nützliche Funktionen
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; mml-sec.el --- A package with security functions for MML documents
(use-package mml-sec
  :hook (;; automatisch signieren
         (gnus-message-setup . mml-secure-sign-pgpmime)))

(setq mail-sources
      '((file :path "/var/spool/mail/hoel")))

;; # Postingstyles
;; Hier stellen wir Mailadressen und anderes ein.
(setq gnus-posting-styles
      '(
	(".*"
	 ;; In diesem Abschnitt ('".*"') können wir allgemeingültige
	 ;; Einstellungen vornehmen, wie Name etc.
	 ;;
	 ;; Hier für siehe Personalausweis deinen Namen eintragen.
	 (name "Berthold Höllmann"))
	((message-news-p)
	 ;; Deine E-Mail-Adresse für Postings
	 (address "bhoel@despammed.com")
	 ;; Die Reply-To-Adresse, wenn du keine haben möchtest, lösch
	 ;; die Zeile einfach.
	 (reply-to "berthold@xn--hllmanns-n4a.de"))
	((message-mail-p)
	 ;; Deine E-Mail-Adresse für Mails.
	 (reply-to "berthold@xn--hllmanns-n4a.de")
	 (address "berthold@xn--hllmanns-n4a.de"))
	("nnml:MRH"
	 (signature-file "~/.signature-MRH")
	 (reply-to"berhoel@gmail.com")
	 (address "berhoel@gmail.com"))
	("nnml:MRH\\..*"
	 (signature-file "~/.signature-MRH")
	 (reply-to "protektor-mrh@wohnheime.studierendenwerk-hamburg.de")
	 (address "protektor-mrh@wohnheime.studierendenwerk-hamburg.de"))
	("nnml:Linux-Counter\\.*"
	 (signature-file "/home/hoel/.signature_linuxcounter"))
	("nnml:software\\.Xfoil.*"
	 (reply-to "bhoel@web.de")
	 (address "bhoel@web.de"))
	("nnml:yocto.*"
	 (reply-to "berthold-yocto@xn--hllmanns-n4a.de")
	 (address "berthold-yocto@xn--hllmanns-n4a.de"))
	("nnml:python\\.pythoneers\\.hh"
	 (reply-to "berhoel@gmail.com")
	 (address "berhoel@gmail.com"))
	("nnml:SuSE\\.Factory"
	 (reply-to "berthold-tumbleweed@xn--hllmanns-n4a.de")
	 (address "berthold-tumbleweed@xn--hllmanns-n4a.de"))
	("nnml:Disney\\+"
	 (reply-to "disney+@xn--hllmanns-n4a.de")
	 (address "disney+@xn--hllmanns-n4a.de"))
	("nnml:Amazon.*"
	 (reply-to "berthold-amazon@xn--hllmanns-n4a.de")
	 (address "berthold-amazon@xn--hllmanns-n4a.de"))
	("nnml:gsrc\\.bug"
	 (reply-to "berhoel@gmail.com")
	 (address "berhoel@gmail.com"))))
;;  ("nnml:.*"
;;   (From (save-excursion
;;        (set-buffer gnus-article-buffer)
;;        (message-fetch-field "to"))))))

(use-package gnus-search
  :config
  (setq gnus-search-default-engines
	'((nnimap . gnus-search-imap)
          (nnmaildir . gnus-search-notmuch)
          (nnfolder . gnus-search-notmuch)
          (nnml . gnus-search-notmuch))))

;; gnus-message-archive-group
(setq gnus-message-archive-method
      '(nnml "archive"
             (nnml-inhibit-expiry t)
             (nnml-active-file "~/News/sent-mail/active")
             (nnml-directory "~/News/sent-mail/")))
;; Documentation:
;; *Name of the group in which to save the messages you've written.
;; This can either be a string; a list of strings; or an alist
;; of regexps/functions/forms to be evaluated to return a string (or a list
;; of strings).  The functions are called with the name of the current
;; group (or nil) as a parameter.
;;
;; If you want to save your mail in one group and the news articles you
;; write in another group, you could say something like:
(setq gnus-message-archive-group
      '((if (message-news-p)
            "nnml:misc-news"
          "nnml:misc-mail")))

;;; gnus-select-account --- Select an account before writing a mail in gnus
;; gnus-select-account let user select an account before write a email
;; in gnus.
;;
;; ** Configure
;; 1. Gnus-select-account configure
;;    #+BEGIN_EXAMPLE
;;    (require 'gnus-select-account)
;;    (gnus-select-account-enable)
;;    #+END_EXAMPLE
;; 2. Add account information to file: "~/.authinfo.gpg" or "~/.authinfo", for example:
;;    #+BEGIN_EXAMPLE
;;    machine smtp.163.com login xxxr@163.com port 465 password PASSWORD user-full-name "XXX" user-mail-address xxx@163.com
;;    machine smtp.qq.com  login xxx@qq.com   port 465 password PASSWORD user-full-name "XXX" user-mail-address xxx@qq.com
;;    #+END_EXAMPLE
(use-package gnus-select-account :ensure
  :if nil
  :init
  (gnus-select-account-enable))

(provide 'my-gnus/system-home)



;; Local Variables:
;; mode: emacs-lisp
;; coding: utf-8
;; End:

;;; system-home.el ends here
